package com.atlassian.plugin.osgi.factory;

import com.atlassian.plugin.IllegalPluginStateException;
import com.atlassian.plugin.PluginArtifact;
import com.atlassian.plugin.module.ContainerAccessor;
import com.atlassian.plugin.osgi.container.OsgiContainerException;
import com.atlassian.plugin.osgi.container.OsgiContainerManager;
import com.atlassian.plugin.osgi.util.OsgiHeaderUtil;
import org.osgi.framework.Bundle;
import org.osgi.util.tracker.ServiceTracker;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.InputStream;
import java.net.URL;
import java.util.Set;

import static com.atlassian.plugin.osgi.factory.transform.JarUtils.getManifest;
import static com.atlassian.plugin.osgi.factory.transform.JarUtils.hasManifestEntry;
import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Helper class that implements the methods assuming the OSGi plugin has not been installed
 *
 * @since 2.2.0
 */
final class OsgiPluginUninstalledHelper implements OsgiPluginHelper
{
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    private final String key;
    private final OsgiContainerManager osgiContainerManager;
    private final PluginArtifact pluginArtifact;

    public OsgiPluginUninstalledHelper(String key, final OsgiContainerManager mgr, final PluginArtifact artifact)
    {
        this.key = checkNotNull(key);
        this.pluginArtifact = checkNotNull(artifact);
        this.osgiContainerManager = checkNotNull(mgr);
    }

    public Bundle getBundle()
    {
        throw new IllegalPluginStateException(getNotInstalledMessage());
    }

    public <T> Class<T> loadClass(String clazz, Class<?> callingClass) throws ClassNotFoundException
    {
        throw new IllegalPluginStateException(getNotInstalledMessage() + " This is probably because the module " +
                "descriptor is trying to load classes in its init() method.  Move all classloading into the " +
                "enabled() method, and be sure to properly drop class and instance references in disabled().");
    }

    public URL getResource(String name)
    {
        throw new IllegalPluginStateException(getNotInstalledMessage());
    }

    public InputStream getResourceAsStream(String name)
    {
        throw new IllegalPluginStateException(getNotInstalledMessage());
    }

    public ClassLoader getClassLoader()
    {
        throw new IllegalPluginStateException(getNotInstalledMessage());
    }

    public Bundle install()
    {
        final File osgiPlugin = pluginArtifact.toFile();
        logger.debug("Installing OSGi plugin '{}'", osgiPlugin);
        final Bundle bundle = osgiContainerManager.installBundle(osgiPlugin);
        if (!OsgiHeaderUtil.getPluginKey(bundle).equals(key))
        {
            throw new IllegalArgumentException("The plugin key '" + key + "' must either match the OSGi bundle symbolic " +
                    "name (Bundle-SymbolicName) or be specified in the Atlassian-Plugin-Key manifest header");
        }
        return bundle;
    }

    public void onEnable(ServiceTracker... serviceTrackers) throws OsgiContainerException
    {
        throw new IllegalPluginStateException(getNotInstalledMessage());
    }

    public void onDisable() throws OsgiContainerException
    {
        throw new IllegalPluginStateException(getNotInstalledMessage());
    }

    public void onUninstall() throws OsgiContainerException
    {
        throw new IllegalPluginStateException(getNotInstalledMessage());
    }

    public Set<String> getRequiredPlugins()
    {
        throw new IllegalPluginStateException(getNotInstalledMessage());
    }

    public void setPluginContainer(Object container)
    {
        throw new IllegalPluginStateException(getNotInstalledMessage());
    }

    public ContainerAccessor getContainerAccessor()
    {
        throw new IllegalPluginStateException(getNotInstalledMessage());
    }

    public ContainerAccessor getRequiredContainerAccessor()
    {
        throw new IllegalPluginStateException(getNotInstalledMessage());
    }

    private String getNotInstalledMessage()
    {
        return "This operation requires the plugin '" + key + "' to be installed";
    }

    @Override
    public boolean isRemotePlugin()
    {
        return hasManifestEntry(getManifest(pluginArtifact.toFile()), OsgiPlugin.REMOTE_PLUGIN_KEY);
    }
}
