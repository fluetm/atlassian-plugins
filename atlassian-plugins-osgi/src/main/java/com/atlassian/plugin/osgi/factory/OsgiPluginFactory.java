package com.atlassian.plugin.osgi.factory;

import com.atlassian.plugin.Application;
import com.atlassian.plugin.JarPluginArtifact;
import com.atlassian.plugin.ModuleDescriptorFactory;
import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginArtifact;
import com.atlassian.plugin.PluginInformation;
import com.atlassian.plugin.PluginParseException;
import com.atlassian.plugin.PluginPermission;
import com.atlassian.plugin.event.PluginEventManager;
import com.atlassian.plugin.factories.AbstractPluginFactory;
import com.atlassian.plugin.impl.UnloadablePlugin;
import com.atlassian.plugin.osgi.container.OsgiContainerManager;
import com.atlassian.plugin.osgi.container.OsgiPersistentCache;
import com.atlassian.plugin.osgi.factory.transform.DefaultPluginTransformer;
import com.atlassian.plugin.osgi.factory.transform.PluginTransformationException;
import com.atlassian.plugin.osgi.factory.transform.PluginTransformer;
import com.atlassian.plugin.osgi.factory.transform.model.SystemExports;
import com.atlassian.plugin.parsers.DescriptorParser;
import com.google.common.base.Predicate;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Ranges;
import org.apache.commons.io.IOUtils;
import org.osgi.framework.Constants;
import org.osgi.util.tracker.ServiceTracker;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.Set;
import java.util.jar.Manifest;

import static com.atlassian.plugin.osgi.util.OsgiHeaderUtil.getAttributeWithoutValidation;
import static com.atlassian.plugin.osgi.util.OsgiHeaderUtil.getNonEmptyAttribute;
import static com.atlassian.plugin.osgi.util.OsgiHeaderUtil.getValidatedAttribute;
import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Plugin loader that starts an OSGi container and loads plugins into it, wrapped as OSGi bundles.  Supports
 * <ul>
 *  <li>Dynamic loading of module descriptors via OSGi services</li>
 *  <li>Delayed enabling until the plugin container is active</li>
 *  <li>XML or Jar manifest configuration</li>
 * </ul>
 */
public final class OsgiPluginFactory extends AbstractPluginFactory
{
    private static final Logger log = LoggerFactory.getLogger(OsgiPluginFactory.class);

    public interface PluginTransformerFactory
    {
        PluginTransformer newPluginTransformer(OsgiPersistentCache cache, SystemExports systemExports, Set<Application> applicationKeys, String pluginDescriptorPath, OsgiContainerManager osgi);
    }

    public static class DefaultPluginTransformerFactory implements PluginTransformerFactory
    {
        public PluginTransformer newPluginTransformer(OsgiPersistentCache cache, SystemExports systemExports, Set<Application> applicationKeys, String pluginDescriptorPath, OsgiContainerManager osgi)
        {
            return new DefaultPluginTransformer(cache, systemExports, applicationKeys, pluginDescriptorPath, osgi);
        }
    }

    private final OsgiContainerManager osgi;
    private final String pluginDescriptorFileName;
    private final PluginEventManager pluginEventManager;
    private final Set<Application> applications;
    private final OsgiPersistentCache persistentCache;
    private final PluginTransformerFactory pluginTransformerFactory;

    private volatile PluginTransformer pluginTransformer;

    private final OsgiChainedModuleDescriptorFactoryCreator osgiChainedModuleDescriptorFactoryCreator;

    /**
     * Default constructor
     */
    public OsgiPluginFactory(String pluginDescriptorFileName, Set<Application> applications, OsgiPersistentCache persistentCache, final OsgiContainerManager osgi, PluginEventManager pluginEventManager)
    {
        this(pluginDescriptorFileName, applications, persistentCache, osgi, pluginEventManager, new DefaultPluginTransformerFactory());
    }

    /**
     * Constructor for implementations that want to override the DefaultPluginTransformer with a custom implementation
     */
    public OsgiPluginFactory(String pluginDescriptorFileName, Set<Application> applications, OsgiPersistentCache persistentCache, final OsgiContainerManager osgi, PluginEventManager pluginEventManager, PluginTransformerFactory pluginTransformerFactory)
    {
        super(new OsgiPluginXmlDescriptorParserFactory(), applications);
        this.pluginDescriptorFileName = checkNotNull(pluginDescriptorFileName, "Plugin descriptor is required");
        this.osgi = checkNotNull(osgi, "The OSGi container is required");
        this.applications = checkNotNull(applications, "Applications is required!");
        this.persistentCache = checkNotNull(persistentCache, "The osgi persistent cache is required");
        this.pluginEventManager = checkNotNull(pluginEventManager, "The plugin event manager is required");
        this.pluginTransformerFactory = checkNotNull(pluginTransformerFactory, "The plugin transformer factory is required");
        this.osgiChainedModuleDescriptorFactoryCreator = new OsgiChainedModuleDescriptorFactoryCreator(new OsgiChainedModuleDescriptorFactoryCreator.ServiceTrackerFactory()
        {
            public ServiceTracker create(String className)
            {
                return osgi.getServiceTracker(className);
            }
        });
    }

    private PluginTransformer getPluginTransformer()
    {
        if (pluginTransformer == null)
        {
            String exportString = (String) osgi.getBundles()[0].getHeaders()
                    .get(Constants.EXPORT_PACKAGE);
            SystemExports exports = new SystemExports(exportString);
            pluginTransformer = pluginTransformerFactory.newPluginTransformer(persistentCache, exports, applications, pluginDescriptorFileName, osgi);
        }
        return pluginTransformer;
    }

    public String canCreate(PluginArtifact pluginArtifact) throws PluginParseException
    {
        if (hasDescriptor(checkNotNull(pluginArtifact)))
        {
            return getPluginKeyFromDescriptor(checkNotNull(pluginArtifact));
        }
        else // no descriptor, pure OSGi bundle
        {
            return getPluginKeyFromManifest(pluginArtifact);
        }
    }

    @Override
    protected InputStream getDescriptorInputStream(PluginArtifact pluginArtifact)
    {
        return pluginArtifact.getResourceAsStream(pluginDescriptorFileName);
    }

    @Override
    protected Predicate<Integer> isValidPluginsVersion()
    {
        return Ranges.singleton(Plugin.VERSION_2);
    }

    /**
     * @param pluginArtifact The plugin artifact
     * @return The plugin key if a manifest is present and contains {@link OsgiPlugin#ATLASSIAN_PLUGIN_KEY} and
     * {@link Constants#BUNDLE_VERSION}
     */
    private String getPluginKeyFromManifest(PluginArtifact pluginArtifact)
    {
        final Manifest mf = getManifest(pluginArtifact);
        if (mf != null)
        {
            final String key = mf.getMainAttributes().getValue(OsgiPlugin.ATLASSIAN_PLUGIN_KEY);
            final String version = mf.getMainAttributes().getValue(Constants.BUNDLE_VERSION);
            if (key != null)
            {
                if (version != null)
                {
                    return key;
                }
                else
                {
                    log.warn("Found plugin key '" + key + "' in the manifest but no bundle version, so it can't be loaded as an OsgiPlugin");
                }
            }
        }
        return null;
    }

    private Manifest getManifest(PluginArtifact pluginArtifact)
    {
        InputStream descriptorClassStream = pluginArtifact.getResourceAsStream("META-INF/MANIFEST.MF");
        if (descriptorClassStream != null)
        {
            try
            {
                return new Manifest(descriptorClassStream);
            }
            catch (IOException e)
            {
                log.error("Cannot read manifest from plugin artifact " + pluginArtifact.getName(), e);
            }
            finally
            {
                IOUtils.closeQuietly(descriptorClassStream);
            }
        }
        return null;
    }

    /**
     * Deploys the plugin artifact.  The artifact will only undergo transformation if a plugin descriptor can be found
     * and the "Atlassian-Plugin-Key" value is not already defined in the manifest.
     *
     * @param pluginArtifact          the plugin artifact to deploy
     * @param moduleDescriptorFactory The factory for plugin modules
     * @return The instantiated and populated plugin
     * @throws PluginParseException If the descriptor cannot be parsed
     * @throws IllegalArgumentException If the plugin descriptor isn't found, and the plugin key and bundle version aren't
     * specified in the manifest
     * @since 2.2.0
     */
    public Plugin create(PluginArtifact pluginArtifact, ModuleDescriptorFactory moduleDescriptorFactory) throws PluginParseException
    {
        checkNotNull(pluginArtifact, "The plugin deployment unit is required");
        checkNotNull(moduleDescriptorFactory, "The module descriptor factory is required");

        Plugin plugin = null;
        InputStream pluginDescriptor = null;
        try
        {
            pluginDescriptor = pluginArtifact.getResourceAsStream(pluginDescriptorFileName);
            if (pluginDescriptor != null)
            {
                ModuleDescriptorFactory combinedFactory = getChainedModuleDescriptorFactory(moduleDescriptorFactory, pluginArtifact);
                DescriptorParser parser = descriptorParserFactory.getInstance(pluginDescriptor, applications);

                final PluginArtifact artifactToInstall;

                // only transform the artifact if no plugin key is found in the manifest
                final String pluginKeyFromManifest = getPluginKeyFromManifest(pluginArtifact);
                if (pluginKeyFromManifest == null)
                {
                    log.debug("Plugin key NOT found in manifest at entry {}, undergoing transformation", OsgiPlugin.ATLASSIAN_PLUGIN_KEY);
                    artifactToInstall = createOsgiPluginJar(pluginArtifact);
                }
                else
                {
                    log.debug("Plugin key found in manifest at entry {}, skipping transformation for '{}'", OsgiPlugin.ATLASSIAN_PLUGIN_KEY, pluginKeyFromManifest);
                    artifactToInstall = pluginArtifact;
                }

                final Plugin osgiPlugin = new OsgiPlugin(parser.getKey(), osgi, artifactToInstall, pluginArtifact, pluginEventManager);

                // Temporarily configure plugin until it can be properly installed
                plugin = parser.configurePlugin(combinedFactory, osgiPlugin);
            }
            else
            {
                Manifest mf = getManifest(pluginArtifact);
                plugin = extractOsgiPlugin(pluginArtifact, mf, osgi, pluginEventManager);
                plugin.setPluginInformation(extractOsgiPluginInformation(mf));
            }
        }
        catch (PluginTransformationException ex)
        {
            return reportUnloadablePlugin(pluginArtifact.toFile(), ex);
        }
        finally
        {
            IOUtils.closeQuietly(pluginDescriptor);
        }
        return plugin;
    }

    private static Plugin extractOsgiPlugin(PluginArtifact pluginArtifact, Manifest mf, final OsgiContainerManager osgi, final PluginEventManager pluginEventManager)
    {
        Plugin plugin;
        String pluginKey = getNonEmptyAttribute(mf, OsgiPlugin.ATLASSIAN_PLUGIN_KEY);
        String bundleName = getAttributeWithoutValidation(mf, Constants.BUNDLE_NAME);

        plugin = new OsgiPlugin(pluginKey, osgi, pluginArtifact, pluginArtifact, pluginEventManager);
        plugin.setKey(pluginKey);
        plugin.setPluginsVersion(2);
        plugin.setName(bundleName);
        return plugin;
    }

    private PluginInformation extractOsgiPluginInformation(final Manifest manifest) {
        String pluginVersion = getValidatedAttribute(manifest, Constants.BUNDLE_VERSION);
        String vendorName = getAttributeWithoutValidation(manifest, Constants.BUNDLE_VENDOR);
        String bundleDescription = getAttributeWithoutValidation(manifest, Constants.BUNDLE_DESCRIPTION);
        PluginInformation info = new PluginInformation();
        info.setVersion(pluginVersion);
        info.setDescription(bundleDescription);
        info.setVendorName(vendorName);
        info.setPermissions(ImmutableSet.of(PluginPermission.EXECUTE_JAVA)); // OSGi plugins require execute Java.
        return info;
    }


    /**
     * Get a chained module descriptor factory that includes any dynamically available descriptor factories
     *
     * @param originalFactory The factory provided by the host application
     * @param pluginArtifact
     * @return The composite factory
     */
    private ModuleDescriptorFactory getChainedModuleDescriptorFactory(ModuleDescriptorFactory originalFactory, final PluginArtifact pluginArtifact)
    {
        return osgiChainedModuleDescriptorFactoryCreator.create(new OsgiChainedModuleDescriptorFactoryCreator.ResourceLocator()
        {
            public boolean doesResourceExist(String name)
            {
                return pluginArtifact.doesResourceExist(name);
            }
        }, originalFactory);
    }

    private PluginArtifact createOsgiPluginJar(PluginArtifact pluginArtifact)
    {
        File transformedFile = getPluginTransformer().transform(pluginArtifact, osgi.getHostComponentRegistrations());
        return new JarPluginArtifact(transformedFile);
    }

    private Plugin reportUnloadablePlugin(File file, Exception e)
    {
        log.error("Unable to load plugin: " + file, e);

        UnloadablePlugin plugin = new UnloadablePlugin();
        plugin.setErrorText("Unable to load plugin: " + e.getMessage());
        return plugin;
    }
}
