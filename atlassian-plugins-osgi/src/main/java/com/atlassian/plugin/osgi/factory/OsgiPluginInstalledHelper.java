package com.atlassian.plugin.osgi.factory;

import com.atlassian.plugin.IllegalPluginStateException;
import com.atlassian.plugin.module.ContainerAccessor;
import com.atlassian.plugin.osgi.container.OsgiContainerException;
import com.atlassian.plugin.osgi.spring.DefaultSpringContainerAccessor;
import com.atlassian.plugin.osgi.util.BundleClassLoaderAccessor;
import com.atlassian.plugin.osgi.util.OsgiHeaderUtil;
import com.atlassian.plugin.util.resource.AlternativeDirectoryResourceLoader;
import com.google.common.base.Function;
import org.osgi.framework.Bundle;
import org.osgi.framework.Constants;
import org.osgi.service.packageadmin.ExportedPackage;
import org.osgi.service.packageadmin.PackageAdmin;
import org.osgi.util.tracker.ServiceTracker;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.InputStream;
import java.net.URL;
import java.util.Dictionary;
import java.util.HashSet;
import java.util.Set;

import static com.atlassian.fugue.Option.option;
import static com.atlassian.fugue.Suppliers.alwaysFalse;
import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Helper class that implements the methods assuming the OSGi plugin has been installed
 *
 * @since 2.2.0
 */
final class OsgiPluginInstalledHelper implements OsgiPluginHelper
{
    private static final Logger logger = LoggerFactory.getLogger(OsgiPluginInstalledHelper.class);
    
    private final ClassLoader bundleClassLoader;
    private final Bundle bundle;
    private final PackageAdmin packageAdmin;

    private volatile ContainerAccessor containerAccessor;
    private volatile ServiceTracker[] serviceTrackers;

    /**
     * @param bundle The bundle
     * @param packageAdmin The package admin
     */
    public OsgiPluginInstalledHelper(final Bundle bundle, final PackageAdmin packageAdmin)
    {
        this.bundle = checkNotNull(bundle);
        this.packageAdmin = checkNotNull(packageAdmin);
        bundleClassLoader = BundleClassLoaderAccessor.getClassLoader(bundle, new AlternativeDirectoryResourceLoader());
    }

    public Bundle getBundle()
    {
        return bundle;
    }

    public <T> Class<T> loadClass(final String clazz, final Class<?> callingClass) throws ClassNotFoundException
    {
        return BundleClassLoaderAccessor.loadClass(getBundle(), clazz);
    }

    public URL getResource(final String name)
    {
        return bundleClassLoader.getResource(name);
    }

    public InputStream getResourceAsStream(final String name)
    {
        return bundleClassLoader.getResourceAsStream(name);
    }

    public ClassLoader getClassLoader()
    {
        return bundleClassLoader;
    }

    public Bundle install()
    {
        logger.debug("Not installing OSGi plugin '{}' since it's already installed.", bundle.getSymbolicName());
        throw new IllegalPluginStateException("Plugin '" + bundle.getSymbolicName() + "' has already been installed");
    }

    public void onEnable(final ServiceTracker... serviceTrackers) throws OsgiContainerException
    {
        for (final ServiceTracker svc : checkNotNull(serviceTrackers))
        {
            svc.open();
        }
        
        this.serviceTrackers = serviceTrackers;
    }

    public void onDisable() throws OsgiContainerException
    {
        final ServiceTracker[] serviceTrackers = this.serviceTrackers; // cache a copy locally for multi-threaded goodness
        if(serviceTrackers != null)
        {
            for (final ServiceTracker svc : serviceTrackers)
            {
                svc.close();
            }
            this.serviceTrackers = null;
        }
        setPluginContainer(null);
    }

    public void onUninstall() throws OsgiContainerException
    {
    }

    public Set<String> getRequiredPlugins()
    {
        /* A bundle must move from INSTALLED to RESOLVED before we can get its import */
        if (bundle.getState() == Bundle.INSTALLED)
        {
            logger.debug("Bundle is in INSTALLED for {}", bundle.getSymbolicName());
            packageAdmin.resolveBundles(new Bundle[]{bundle});
            logger.debug("Bundle state is now {}", bundle.getState());
        }
        
        final Set<String> keys = new HashSet<String>();
        getRequiredPluginsFromExports(keys);

        // we can't get required plugins from services, since services could have different cardinalities and you can't
        // detect that from looking at the service reference.
        return keys;
    }

    private void getRequiredPluginsFromExports(Set<String> keys)
    {
        // Get a set of all packages that this plugin imports
        final Set<String> imports = new HashSet<String>(OsgiHeaderUtil.parseHeader((String) getBundle().getHeaders().get(Constants.IMPORT_PACKAGE)).keySet());

        // Add in all explicit (non-wildcard) dynamic package imports
        imports.addAll(OsgiHeaderUtil.parseHeader((String) getBundle().getHeaders().get(Constants.DYNAMICIMPORT_PACKAGE)).keySet());

        // For each import, determine what bundle provides the package
        for (final String imp : imports)
        {
            // Get a list of package exports for this package
            final ExportedPackage[] exports = packageAdmin.getExportedPackages(imp);
            if (exports != null)
            {
                // For each exported package, determine if we are a consumer
                for (final ExportedPackage export : exports)
                {
                    // Get a list of bundles that consume that package
                    final Bundle[] importingBundles = export.getImportingBundles();
                    if (importingBundles != null)
                    {
                        // For each importing bundle, determine if it is us
                        for (final Bundle importingBundle : importingBundles)
                        {
                            // If we are the bundle consumer, or importer, then add the exporter as a required plugin
                            if (getBundle() == importingBundle)
                            {
                                keys.add(OsgiHeaderUtil.getPluginKey(export.getExportingBundle()));
                                break;
                            }
                        }
                    }
                }
            }
        }
    }

    public void setPluginContainer(final Object container)
    {
        if (container == null)
        {
            containerAccessor = null;
        }
        else if (container instanceof ContainerAccessor)
        {
            containerAccessor = (ContainerAccessor) container;
        }
        else
        {
            containerAccessor = new DefaultSpringContainerAccessor(container);
        }
    }

    public ContainerAccessor getContainerAccessor()
    {
        return containerAccessor;
    }

    /**
     * @throws IllegalPluginStateException if the plugin container is not initialized
     */
    public ContainerAccessor getRequiredContainerAccessor() throws IllegalPluginStateException
    {
        if (containerAccessor == null)
        {
            throw new IllegalStateException("Cannot create object because the plugin container is unavailable.");
        }
        return containerAccessor;
    }

    @Override
    public boolean isRemotePlugin()
    {
        return option(getBundle().getHeaders()).fold(alwaysFalse(), new Function<Dictionary, Boolean>()
        {
            @Override
            public Boolean apply(Dictionary headers)
            {
                return headers.get(OsgiPlugin.REMOTE_PLUGIN_KEY) != null;
            }
        });
    }
}