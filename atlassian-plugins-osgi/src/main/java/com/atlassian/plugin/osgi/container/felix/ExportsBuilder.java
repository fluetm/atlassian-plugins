package com.atlassian.plugin.osgi.container.felix;

import com.atlassian.plugin.osgi.container.PackageScannerConfiguration;
import com.atlassian.plugin.osgi.hostcomponents.HostComponentRegistration;
import com.atlassian.plugin.osgi.util.OsgiHeaderUtil;
import com.atlassian.plugin.util.PluginFrameworkUtils;
import com.google.common.annotations.VisibleForTesting;
import com.google.common.base.Predicate;
import com.google.common.collect.Sets;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.twdata.pkgscanner.DefaultOsgiVersionConverter;
import org.twdata.pkgscanner.ExportPackage;
import org.twdata.pkgscanner.PackageScanner;

import static com.atlassian.plugin.osgi.container.felix.ExportBuilderUtils.parseExportFile;
import static com.atlassian.plugin.osgi.container.felix.ExportBuilderUtils.copyUnlessExist;
import static com.google.common.collect.Iterables.any;
import static com.google.common.collect.Lists.newArrayList;
import static org.twdata.pkgscanner.PackageScanner.exclude;
import static org.twdata.pkgscanner.PackageScanner.include;
import static org.twdata.pkgscanner.PackageScanner.jars;
import static org.twdata.pkgscanner.PackageScanner.packages;

import javax.annotation.Nullable;
import javax.servlet.ServletContext;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.*;

/**
 * Builds the OSGi package exports string.  Uses a file to cache the scanned results, keyed by the application version.
 */
class ExportsBuilder
{
    static final String JDK_6 = "1.6";
    static final String JDK_7 = "1.7";

    static final String OSGI_PACKAGES_PATH = "osgi-packages.txt";
    static final String JDK_PACKAGES_PATH = "jdk-packages.txt";

    private static Logger log = LoggerFactory.getLogger(ExportsBuilder.class);
    private static String exportStringCache;

    @VisibleForTesting
    static final Predicate<String> UNDER_PLUGIN_FRAMEWORK = new Predicate<String>()
    {
        private Iterable<String> packagesNotInPlugins = newArrayList(
                "com.atlassian.plugin.remotable",
                // webfragments and webresources moved out in PLUG-942 and PLUG-943
                "com.atlassian.plugin.cache.filecache",
                "com.atlassian.plugin.webresource",
                "com.atlassian.plugin.web"
        );

        public boolean apply(final String pkg)
        {
            Predicate<String> underPackage = new Predicate<String>() {
                @Override
                public boolean apply(@Nullable String input) {
                    return pkg.equals(input) || pkg.startsWith(input + ".");
                }
            };
            return pkg.startsWith("com.atlassian.plugin.") && !any(packagesNotInPlugins, underPackage);
        }
    };

    public static interface CachedExportPackageLoader
    {
        Collection<ExportPackage> load();
    }

    private final CachedExportPackageLoader cachedExportPackageLoader;

    public ExportsBuilder()
    {
        this(new PackageScannerExportsFileLoader("package-scanner-exports.xml"));
    }

    public ExportsBuilder(CachedExportPackageLoader loader)
    {
        this.cachedExportPackageLoader = loader;
    }
    /**
     * Gets the framework exports taking into account host components and package scanner configuration.
     * <p>
     * Often, this information will not change without a system restart, so we determine this once and then cache the value.
     * The cache is only useful if the plugin system is thrown away and re-initialised. This is done thousands of times
     * during JIRA functional testing, and the cache was added to speed this up.
     *
     * If needed, call {@link #clearExportCache()} to clear the cache.
     *
     * @param regs The list of host component registrations
     * @param packageScannerConfig The configuration for the package scanning
     * @return A list of exports, in a format compatible with OSGi headers
     */
    public String getExports(List<HostComponentRegistration> regs, PackageScannerConfiguration packageScannerConfig)
    {
        if (exportStringCache == null)
        {
            exportStringCache = determineExports(regs, packageScannerConfig);
        }
        return exportStringCache;
    }

    /**
     * Clears the export string cache. This results in {@link #getExports(java.util.List, com.atlassian.plugin.osgi.container.PackageScannerConfiguration)}
     * having to recalculate the export string next time which can significantly slow down the start up time of plugin framework.
     * @since 2.9.0
     */
    public void clearExportCache()
    {
        exportStringCache = null;
    }

    /**
     * Determines framework exports taking into account host components and package scanner configuration.
     *
     * @param regs The list of host component registrations
     * @param packageScannerConfig The configuration for the package scanning
     * @param cacheDir No longer used. (method deprecated).
     * @return A list of exports, in a format compatible with OSGi headers
     * @deprecated Please use {@link #getExports}. Deprecated since 2.3.6
     */
    @SuppressWarnings ({ "UnusedDeclaration" })
    public String determineExports(List<HostComponentRegistration> regs, PackageScannerConfiguration packageScannerConfig, File cacheDir)
    {
        return determineExports(regs, packageScannerConfig);
    }

    /**
     * Determines framework exports taking into account host components and package scanner configuration.
     *
     * @param regs The list of host component registrations
     * @param packageScannerConfig The configuration for the package scanning
     * @return A list of exports, in a format compatible with OSGi headers
     */
    String determineExports(List<HostComponentRegistration> regs, PackageScannerConfiguration packageScannerConfig)
    {
        Map<String, String> exportPackages = new HashMap<String, String>();

        // The first part is osgi related packages.
        copyUnlessExist(exportPackages, parseExportFile(OSGI_PACKAGES_PATH));

        // The second part is JDK packages.
        copyUnlessExist(exportPackages, parseExportFile(JDK_PACKAGES_PATH));

        // Third part by scanning packages available via classloader. The versions are determined by jar names.
        Collection<ExportPackage> scannedPackages = generateExports(packageScannerConfig);
        copyUnlessExist(exportPackages, ExportBuilderUtils.toMap(scannedPackages));

        // Fourth part by scanning host components since all the classes referred to by them must be available to consumers.
        try
        {
            Map<String,String> referredPackages = OsgiHeaderUtil.findReferredPackageVersions(regs, packageScannerConfig.getPackageVersions());
            copyUnlessExist(exportPackages, referredPackages);
        }
        catch (IOException ex)
        {
            log.error("Unable to calculate necessary exports based on host components", ex);
        }

        // All the packages under plugin framework namespace must be exported as the plugin framework's version.
        enforceFrameworkVersion(exportPackages);

        // Generate the actual export string in OSGi spec.
        final String exports = OsgiHeaderUtil.generatePackageVersionString(exportPackages);

        if (log.isDebugEnabled())
        {
            log.debug("Exports:\n"+exports.replaceAll(",", "\r\n"));
        }

        return exports;
    }

    private void enforceFrameworkVersion(Map<String, String> exportPackages)
    {
        final String frameworkVersion = PluginFrameworkUtils.getPluginFrameworkVersion();

        // convert the version to OSGi format.
        DefaultOsgiVersionConverter converter = new DefaultOsgiVersionConverter();
        final String frameworkVersionOsgi = converter.getVersion(frameworkVersion);

        for(String pkg: Sets.filter(exportPackages.keySet(), UNDER_PLUGIN_FRAMEWORK))
        {
            exportPackages.put(pkg, frameworkVersionOsgi);
        }
    }

    Collection<ExportPackage> generateExports(PackageScannerConfiguration packageScannerConfig)
    {
        String[] arrType = new String[0];

        Map<String,String> pkgVersions = new HashMap<String,String>(packageScannerConfig.getPackageVersions());
        if (packageScannerConfig.getServletContext() != null)
        {
            String ver = packageScannerConfig.getServletContext().getMajorVersion() + "." + packageScannerConfig.getServletContext().getMinorVersion();
            pkgVersions.put("javax.servlet*", ver);
        }

        PackageScanner scanner = new PackageScanner()
           .select(
               jars(
                       include(packageScannerConfig.getJarIncludes().toArray(arrType)),
                       exclude(packageScannerConfig.getJarExcludes().toArray(arrType))),
               packages(
                       include(packageScannerConfig.getPackageIncludes().toArray(arrType)),
                       exclude(packageScannerConfig.getPackageExcludes().toArray(arrType)))
           )
           .withMappings(pkgVersions);

        if (log.isDebugEnabled())
        {
            scanner.enableDebug();
        }

        Collection<ExportPackage> exports = cachedExportPackageLoader.load();
        if (exports == null)
        {
            exports = scanner.scan();
        }
        log.info("Package scan completed. Found " + exports.size() + " packages to export.");

        if (!isPackageScanSuccessful(exports) && packageScannerConfig.getServletContext() != null)
        {
            log.warn("Unable to find expected packages via classloader scanning.  Trying ServletContext scanning...");
            ServletContext ctx = packageScannerConfig.getServletContext();
            try
            {
                exports = scanner.scan(ctx.getResource("/WEB-INF/lib"), ctx.getResource("/WEB-INF/classes"));
            }
            catch (MalformedURLException e)
            {
                log.warn("Unable to scan webapp for packages", e);
            }
        }

        if (!isPackageScanSuccessful(exports))
        {
            throw new IllegalStateException("Unable to find required packages via classloader or servlet context"
                    + " scanning, most likely due to an application server bug.");
        }
        return exports;
    }

    /**
     * Tests to see if a scan of packages to export was successful, using the presence of slf4j as the criteria.
     *
     * @param exports The exports found so far
     * @return True if slf4j is present, false otherwise
     */
    private static boolean isPackageScanSuccessful(Collection<ExportPackage> exports)
    {
        boolean slf4jFound = false;
        for (ExportPackage export : exports)
        {
            if (export.getPackageName().equals("org.slf4j"))
            {
                slf4jFound = true;
                break;
            }
        }
        return slf4jFound;
    }

    static class PackageScannerExportsFileLoader implements CachedExportPackageLoader
    {
        private final String path;

        public PackageScannerExportsFileLoader(String path)
        {
            this.path = path;
        }

        @Override
        public Collection<ExportPackage> load()
        {
            URL exportsUrl = getClass().getClassLoader().getResource(path);
            if (exportsUrl != null)
            {
                log.debug("Precalculated exports found, loading...");
                List<ExportPackage> result = newArrayList();
                try
                {
                    Document doc = new SAXReader().read(exportsUrl);
                    for (Element export : ((List<Element>)doc.getRootElement().elements()))
                    {
                        String packageName = export.attributeValue("package");
                        String version = export.attributeValue("version");
                        String location = export.attributeValue("location");

                        if (packageName == null || location == null)
                        {
                            log.warn("Invalid configuration: package({}) and location({}) are required, " +
                                    "aborting precalculated exports and reverting to normal scanning",
                                    packageName, location);
                            return Collections.emptyList();
                        }
                        result.add(new ExportPackage(packageName, version, new File(location)));
                    }
                    log.debug("Loaded {} precalculated exports", result.size());

                    return result;
                }
                catch (DocumentException e)
                {
                    log.warn("Unable to load exports from " + path + " due to malformed XML", e);
                }
            }
            log.debug("No precalculated exports found");
            return null;
        }
    }
}
