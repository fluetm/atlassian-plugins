package com.atlassian.plugin.osgi.factory;

import com.atlassian.plugin.Application;
import com.atlassian.plugin.ModuleDescriptorFactory;
import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.PluginArtifact;
import com.atlassian.plugin.PluginParseException;
import com.atlassian.plugin.event.PluginEventManager;
import com.atlassian.plugin.factories.AbstractPluginFactory;
import com.atlassian.plugin.impl.UnloadablePlugin;
import com.atlassian.plugin.osgi.container.OsgiContainerException;
import com.atlassian.plugin.osgi.container.OsgiContainerManager;
import com.atlassian.plugin.osgi.util.OsgiHeaderUtil;
import com.google.common.base.Predicate;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Ranges;
import org.apache.commons.io.IOUtils;
import org.osgi.framework.Bundle;
import org.osgi.framework.Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.jar.Manifest;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Plugin deployer that deploys OSGi bundles that don't contain XML descriptor files
 */
public final class OsgiBundleFactory extends AbstractPluginFactory
{
    private static final Logger log = LoggerFactory.getLogger(OsgiBundleFactory.class);

    private final OsgiContainerManager osgi;
    private final PluginEventManager pluginEventManager;
    private final String pluginDescriptorFileName;

    public OsgiBundleFactory(OsgiContainerManager osgi, PluginEventManager pluginEventManager)
    {
        this(PluginAccessor.Descriptor.FILENAME, osgi, pluginEventManager);
    }

    public OsgiBundleFactory(String pluginDescriptorFileName, OsgiContainerManager osgi, PluginEventManager pluginEventManager)
    {
        super(new OsgiPluginXmlDescriptorParserFactory(), ImmutableSet.<Application>of());
        this.pluginDescriptorFileName = checkNotNull(pluginDescriptorFileName);
        this.osgi = checkNotNull(osgi, "The osgi container is required");
        this.pluginEventManager = checkNotNull(pluginEventManager, "The plugin event manager is required");
    }

    @Override
    protected InputStream getDescriptorInputStream(PluginArtifact pluginArtifact)
    {
        return pluginArtifact.getResourceAsStream(pluginDescriptorFileName);
    }

    @Override
    protected Predicate<Integer> isValidPluginsVersion()
    {
        return Ranges.atLeast(Plugin.VERSION_2);
    }

    public String canCreate(PluginArtifact pluginArtifact) throws PluginParseException
    {
        checkNotNull(pluginArtifact, "The plugin artifact is required");

        InputStream manifestStream = null;
        InputStream descriptorStream = null;

        try
        {
            descriptorStream = pluginArtifact.getResourceAsStream(pluginDescriptorFileName);
            manifestStream = pluginArtifact.getResourceAsStream("META-INF/MANIFEST.MF");

            final String pluginKey;
            if (descriptorStream == null && manifestStream != null)
            {
                final Manifest mf = loadManifest(manifestStream);
                final String bundleSymbolicName = mf.getMainAttributes().getValue(Constants.BUNDLE_SYMBOLICNAME);
                pluginKey = bundleSymbolicName == null ? null : OsgiHeaderUtil.getPluginKey(mf);
            }
            else // it's not a bundle
            {
                pluginKey = null;
            }
            return pluginKey;
        }
        finally
        {
            IOUtils.closeQuietly(manifestStream);
            IOUtils.closeQuietly(descriptorStream);
        }
    }

    private Manifest loadManifest(InputStream manifestStream)
    {
        try
        {
            return new Manifest(manifestStream);
        }
        catch (IOException e)
        {
            throw new PluginParseException("Unable to parse manifest", e);
        }
    }

    /**
     * Deploys the plugin artifact
     * @param pluginArtifact the plugin artifact to deploy
     * @param moduleDescriptorFactory The factory for plugin modules
     * @return The instantiated and populated plugin
     * @throws PluginParseException If the descriptor cannot be parsed
     * @since 2.2.0
     */
    public Plugin create(PluginArtifact pluginArtifact, ModuleDescriptorFactory moduleDescriptorFactory) throws PluginParseException
    {
        checkNotNull(pluginArtifact, "The plugin artifact is required");
        checkNotNull(moduleDescriptorFactory, "The module descriptor factory is required");

        File file = pluginArtifact.toFile();
        Bundle bundle;
        try
        {
            bundle = osgi.installBundle(file);
        } catch (OsgiContainerException ex)
        {
            return reportUnloadablePlugin(file, ex);
        }
        String key = OsgiHeaderUtil.getPluginKey(bundle);
        return new OsgiBundlePlugin(bundle, key, pluginArtifact);
    }

    private Plugin reportUnloadablePlugin(File file, Exception e)
    {
        log.error("Unable to load plugin: "+file, e);

        UnloadablePlugin plugin = new UnloadablePlugin();
        plugin.setErrorText("Unable to load plugin: "+e.getMessage());
        return plugin;
    }
}