package com.atlassian.plugin.osgi.factory;

import com.atlassian.plugin.PluginState;
import com.atlassian.plugin.descriptors.AbstractModuleDescriptor;
import com.atlassian.plugin.descriptors.RequiresRestart;
import com.atlassian.plugin.event.PluginEventManager;
import com.atlassian.plugin.event.events.PluginContainerRefreshedEvent;
import com.atlassian.plugin.module.ModuleFactory;
import com.atlassian.plugin.osgi.container.OsgiContainerException;
import com.atlassian.plugin.osgi.util.OsgiSystemBundleUtil;
import junit.framework.TestCase;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.osgi.framework.Bundle;
import org.osgi.framework.BundleContext;
import org.osgi.framework.BundleException;
import org.osgi.framework.Constants;

import java.util.Dictionary;
import java.util.Hashtable;

import static org.mockito.Mockito.*;

public class TestOsgiPlugin extends TestCase
{
    private Bundle bundle;
    private OsgiPlugin plugin;
    private BundleContext bundleContext;
    private Bundle systemBundle;
    private BundleContext systemBundleContext;
    private Dictionary<String, String> dict;
    private OsgiPluginHelper helper;

    @Override
    public void setUp()
    {
        bundle = mock(Bundle.class);
        dict = new Hashtable<String, String>();
        dict.put(Constants.BUNDLE_DESCRIPTION, "desc");
        dict.put(Constants.BUNDLE_VERSION, "1.0");
        when(bundle.getHeaders()).thenReturn(dict);
        bundleContext = mock(BundleContext.class);
        when(bundle.getBundleContext()).thenReturn(bundleContext);

        systemBundle = mock(Bundle.class);
        systemBundleContext = mock(BundleContext.class);
        when(bundleContext.getBundle(OsgiSystemBundleUtil.SYSTEM_BUNDLE_ID)).thenReturn(systemBundle);
        when(systemBundle.getBundleContext()).thenReturn(systemBundleContext);

        helper = mock(OsgiPluginHelper.class);
        when(helper.getBundle()).thenReturn(bundle);

        plugin = new OsgiPlugin(mock(PluginEventManager.class), helper);
    }

    @Override
    public void tearDown()
    {
        bundle = null;
        plugin = null;
        bundleContext = null;
    }

    public void testEnabled() throws BundleException
    {
        _enablePlugin();
        verify(bundle).start();
    }

    private void _enablePlugin() {
        when(bundle.getState()).thenReturn(Bundle.RESOLVED);
        plugin.enable();
    }

    public void testDisabled() throws BundleException
    {
        when(bundle.getState()).thenReturn(Bundle.ACTIVE);
        plugin.disable();
        verify(bundle).stop();
    }

    public void testDisabledOnNonDynamicPlugin() throws BundleException
    {
        plugin.addModuleDescriptor(new StaticModuleDescriptor());
        plugin.onPluginFrameworkStartedEvent(null);
        when(bundle.getState()).thenReturn(Bundle.ACTIVE);
        plugin.disable();
        verify(bundle, never()).stop();
    }

    public void testUninstall() throws BundleException
    {
        _enablePlugin();
        when(bundle.getState()).thenReturn(Bundle.ACTIVE);
        _assertPluginUninstalled();
    }

    public void testUninstallingUninstalledBundle() throws BundleException
    {
        _enablePlugin();
        when(bundle.getState()).thenReturn(Bundle.ACTIVE).thenReturn(Bundle.UNINSTALLED);
        _assertPluginUninstalled();
        _assertPluginUninstalled();
    }

    private void _assertPluginUninstalled() {
        plugin.uninstallInternal();
        assertEquals(plugin.getPluginState(), PluginState.UNINSTALLED);
    }

    public void testOnPluginContainerRefresh()
    {
        plugin.setKey("plugin-key");
        _enablePlugin();
        PluginContainerRefreshedEvent event = new PluginContainerRefreshedEvent(new Object(), "plugin-key");
        plugin.onPluginContainerRefresh(event);
        assertEquals(PluginState.ENABLED, plugin.getPluginState());
    }

    public void testQuickOnPluginContainerRefresh() throws BundleException, InterruptedException
    {
        plugin.setKey("plugin-key");
        when(bundle.getState()).thenReturn(Bundle.RESOLVED);

        final ConcurrentStateEngine states = new ConcurrentStateEngine("bundle-starting", "container-created", "bundle-started", "mid-start", "end");
        when(bundle.getBundleContext()).thenAnswer(new Answer<Object>()
        {
            public Object answer(InvocationOnMock invocation) throws Throwable
            {
                states.tryNextState("bundle-started", "mid-start");
                final BundleContext context = mock(BundleContext.class);
                when(context.getBundle(OsgiSystemBundleUtil.SYSTEM_BUNDLE_ID)).thenReturn(systemBundle);

                return context;
            }
        });

        doAnswer(new Answer<Object>()
        {
            public Object answer(InvocationOnMock invocation) throws Throwable
            {
                states.state("bundle-starting");
                Thread t = new Thread()
                {
                    public void run()
                    {
                        PluginContainerRefreshedEvent event = new PluginContainerRefreshedEvent(new Object(), "plugin-key");
                        states.tryNextState("bundle-starting", "container-created");
                        plugin.onPluginContainerRefresh(event);
                    }
                };
                t.start();
                states.tryNextState("container-created", "bundle-started");
                return null;
            }
        }).when(bundle).start();

        plugin.enable();


        states.tryNextState("mid-start", "end");

        assertEquals(PluginState.ENABLED, plugin.getPluginState());
    }

    public void testOnPluginContainerRefreshNotEnabling()
    {
        plugin.setKey("plugin-key");
        PluginContainerRefreshedEvent event = new PluginContainerRefreshedEvent(new Object(), "plugin-key");
        when(bundle.getState()).thenReturn(Bundle.ACTIVE);
        plugin.disable();
        plugin.onPluginContainerRefresh(event);
        assertEquals(PluginState.DISABLED, plugin.getPluginState());
    }

    public void testUninstallRetryLogic() throws BundleException
    {
        try {
            when(bundle.getState()).thenReturn(Bundle.ACTIVE);
            plugin.enable();
            when(bundle.getSymbolicName()).thenReturn("Mock Bundle");
            doThrow(new BundleException("Mock Bundle Exception")).when(bundle).uninstall();
            plugin.uninstallInternal();
        } catch (Exception e) {
            assertTrue("Should throw an OsgiContainerException", e instanceof OsgiContainerException);
        } finally {
            assertEquals(PluginState.ENABLED, plugin.getPluginState());
            verify(bundle, times(3)).uninstall();
        }
    }

    @RequiresRestart
    public static class StaticModuleDescriptor extends AbstractModuleDescriptor<Object>
    {
        public StaticModuleDescriptor()
        {
            super(ModuleFactory.LEGACY_MODULE_FACTORY);
        }

        public Object getModule()
        {
            return null;
        }
    }
}
