package com.atlassian.plugin.spring;

import com.atlassian.plugin.osgi.hostcomponents.ContextClassLoaderStrategy;

import java.util.Collections;
import java.util.Map;
import java.util.Set;

/**
 * Offers configurations for SpringHostComponentProvider.
 */
public class SpringHostComponentProviderConfig
{
    /**
     * A set of bean names to make available to plugins
     */
    private Set<String> beanNames = Collections.emptySet();

    /**
     * Mapping of beanNames to the interfaces it should be exposed as. Note that if a bean name is present an no interface
     * is defined then all its interfaces should be 'exposed'.
     */
    private Map<String, Class[]> beanInterfaces = Collections.emptyMap();

    /**
     * Mapping of beanNames with their {@link com.atlassian.plugin.osgi.hostcomponents.ContextClassLoaderStrategy}.
     * Default value is {@link com.atlassian.plugin.osgi.hostcomponents.ContextClassLoaderStrategy#USE_HOST}.
     */
    private Map<String, ContextClassLoaderStrategy> beanContextClassLoaderStrategies = Collections.emptyMap();

    /**
     * Whether or not to scan for {@link com.atlassian.plugin.spring.AvailableToPlugins} annotations on beans defined in the bean factory, defaults to {@code false}.
     */
    private boolean useAnnotation = false;


    public Set<String> getBeanNames() {
        return beanNames;
    }

    public void setBeanNames(Set<String> beanNames) {
        this.beanNames = beanNames;
    }

    public Map<String, Class[]> getBeanInterfaces() {
        return beanInterfaces;
    }

    public void setBeanInterfaces(Map<String, Class[]> beanInterfaces) {
        this.beanInterfaces = beanInterfaces;
    }

    public Map<String, ContextClassLoaderStrategy> getBeanContextClassLoaderStrategies() {
        return beanContextClassLoaderStrategies;
    }

    public void setBeanContextClassLoaderStrategies(Map<String, ContextClassLoaderStrategy> beanContextClassLoaderStrategies) {
        this.beanContextClassLoaderStrategies = beanContextClassLoaderStrategies;
    }

    public void setUseAnnotation(boolean useAnnotation)
    {
        this.useAnnotation = useAnnotation;
    }

    public boolean isUseAnnotation()
    {
        return useAnnotation;
    }
}
