# Atlassian Plugins

## Description

The plugin framework supports several types of plugins, including OSGi-based plugins. [OSGi](http://www.osgi.org/) is a dynamic module system for Java that the framework uses to enable plugins to depend on each other and more easily share services. Because the plugin framework supports multiple versions of plugins simultaneously, legacy plugins or those built into the application can exist side-by-side with newer dynamic plugins that leverage the plugin-sharing benefits of OSGi.

## Atlassian Developer?

### Committing Guidelines

Please see [the following guide](https://extranet.atlassian.com/x/Uouvdg) for committing to this module.

### Builds

The Bamboo builds for this project are on [BEAC](https://bamboo.extranet.atlassian.com/browse/PLUG)

## External User?

### Issues

Please raise any issues you find with this module in [JIRA](https://ecosystem.atlassian.net/browse/PLUG)

### Documentation

[Plugin Framework Documentation](https://developer.atlassian.com/display/PLUGINFRAMEWORK)