package com.atlassian.plugin.schema.spi;

/**
 * Creates schema instances
 */
public interface SchemaFactory
{
    Schema getSchema();
}
