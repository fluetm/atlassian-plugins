package com.atlassian.plugin.manager;

import com.atlassian.plugin.DefaultModuleDescriptorFactory;
import com.atlassian.plugin.JarPluginArtifact;
import com.atlassian.plugin.MockApplication;
import com.atlassian.plugin.MockModuleDescriptor;
import com.atlassian.plugin.ModuleDescriptor;
import com.atlassian.plugin.ModuleDescriptorFactory;
import com.atlassian.plugin.Permissions;
import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.PluginArtifact;
import com.atlassian.plugin.PluginException;
import com.atlassian.plugin.PluginInformation;
import com.atlassian.plugin.PluginInstaller;
import com.atlassian.plugin.PluginParseException;
import com.atlassian.plugin.PluginRestartState;
import com.atlassian.plugin.PluginState;
import com.atlassian.plugin.descriptors.AbstractModuleDescriptor;
import com.atlassian.plugin.descriptors.MockUnusedModuleDescriptor;
import com.atlassian.plugin.descriptors.RequiresRestart;
import com.atlassian.plugin.event.PluginEventListener;
import com.atlassian.plugin.event.PluginEventManager;
import com.atlassian.plugin.event.events.PluginContainerUnavailableEvent;
import com.atlassian.plugin.event.events.PluginDisabledEvent;
import com.atlassian.plugin.event.events.PluginEnabledEvent;
import com.atlassian.plugin.event.events.PluginFrameworkShutdownEvent;
import com.atlassian.plugin.event.events.PluginModuleAvailableEvent;
import com.atlassian.plugin.event.events.PluginModuleDisabledEvent;
import com.atlassian.plugin.event.events.PluginModuleEnabledEvent;
import com.atlassian.plugin.event.events.PluginModuleUnavailableEvent;
import com.atlassian.plugin.event.impl.DefaultPluginEventManager;
import com.atlassian.plugin.event.listeners.FailListener;
import com.atlassian.plugin.event.listeners.PassListener;
import com.atlassian.plugin.factories.LegacyDynamicPluginFactory;
import com.atlassian.plugin.factories.PluginFactory;
import com.atlassian.plugin.factories.XmlDynamicPluginFactory;
import com.atlassian.plugin.hostcontainer.DefaultHostContainer;
import com.atlassian.plugin.impl.AbstractDelegatingPlugin;
import com.atlassian.plugin.impl.StaticPlugin;
import com.atlassian.plugin.impl.UnloadablePlugin;
import com.atlassian.plugin.loaders.DirectoryPluginLoader;
import com.atlassian.plugin.loaders.DynamicPluginLoader;
import com.atlassian.plugin.loaders.PluginLoader;
import com.atlassian.plugin.loaders.SinglePluginLoader;
import com.atlassian.plugin.loaders.classloading.DirectoryPluginLoaderUtils;
import com.atlassian.plugin.manager.store.MemoryPluginPersistentStateStore;
import com.atlassian.plugin.metadata.ClasspathFilePluginMetadata;
import com.atlassian.plugin.mock.MockAnimal;
import com.atlassian.plugin.mock.MockAnimalModuleDescriptor;
import com.atlassian.plugin.mock.MockBear;
import com.atlassian.plugin.mock.MockMineral;
import com.atlassian.plugin.mock.MockMineralModuleDescriptor;
import com.atlassian.plugin.mock.MockThing;
import com.atlassian.plugin.mock.MockVegetableModuleDescriptor;
import com.atlassian.plugin.mock.MockVegetableSubclassModuleDescriptor;
import com.atlassian.plugin.module.ModuleFactory;
import com.atlassian.plugin.parsers.DescriptorParser;
import com.atlassian.plugin.parsers.DescriptorParserFactory;
import com.atlassian.plugin.predicate.ModuleDescriptorPredicate;
import com.atlassian.plugin.predicate.PluginPredicate;
import com.atlassian.plugin.repositories.FilePluginInstaller;
import com.atlassian.plugin.test.PluginJarBuilder;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Iterables;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

import static com.atlassian.plugin.loaders.classloading.DirectoryPluginLoaderUtils.PADDINGTON_JAR;
import static com.google.common.collect.ImmutableList.copyOf;
import static java.util.Arrays.asList;
import static java.util.Collections.singleton;
import static org.junit.Assert.*;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.isA;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.hamcrest.Matchers.hasSize;

public class TestDefaultPluginManager
{
    /**
     * the object being tested
     */
    protected DefaultPluginManager manager;

    private PluginPersistentStateStore pluginStateStore;
    private DefaultModuleDescriptorFactory moduleDescriptorFactory; // we should be able to use the interface here?

    private DirectoryPluginLoader directoryPluginLoader;
    protected PluginEventManager pluginEventManager;
    private File pluginsDirectory;
    private File pluginsTestDir;

    private void createFillAndCleanTempPluginDirectory() throws IOException
    {
        final DirectoryPluginLoaderUtils.ScannerDirectories directories = DirectoryPluginLoaderUtils.createFillAndCleanTempPluginDirectory();
        pluginsDirectory = directories.pluginsDirectory;
        pluginsTestDir = directories.pluginsTestDir;
    }

    @Before
    public void setUp() throws Exception
    {
        pluginEventManager = new DefaultPluginEventManager();

        pluginStateStore = new MemoryPluginPersistentStateStore();
        moduleDescriptorFactory = new DefaultModuleDescriptorFactory(new DefaultHostContainer());
    }

    @After
    public void tearDown() throws Exception
    {
        manager = null;
        moduleDescriptorFactory = null;
        pluginStateStore = null;

        if (directoryPluginLoader != null)
        {
            directoryPluginLoader = null;
        }
    }

    protected DefaultPluginManager newDefaultPluginManager(PluginLoader... pluginLoaders)
    {
        manager = new DefaultPluginManager(pluginStateStore, copyOf(pluginLoaders), moduleDescriptorFactory, pluginEventManager, true);
        return manager;
    }

    protected PluginAccessor getPluginAccessor()
    {
        return manager;
    }

    @Test
    public void testRetrievePlugins() throws PluginParseException
    {
        manager = newDefaultPluginManager(
                new SinglePluginLoader("test-atlassian-plugin.xml"),
                new SinglePluginLoader("test-disabled-plugin.xml"));

        moduleDescriptorFactory.addModuleDescriptor("animal", MockAnimalModuleDescriptor.class);
        moduleDescriptorFactory.addModuleDescriptor("mineral", MockMineralModuleDescriptor.class);
        manager.init();

        assertThat(manager.getPlugins(), hasSize(2));
        assertThat(manager.getEnabledPlugins(), hasSize(1));
        manager.enablePlugin("test.disabled.plugin");
        assertThat(manager.getEnabledPlugins(), hasSize(2));
    }

    @Test
    public void testEnableModuleFailed() throws PluginParseException
    {
        final PluginLoader mockPluginLoader = mock(PluginLoader.class);
        final ModuleDescriptor<Object> badModuleDescriptor = new AbstractModuleDescriptor<Object>(ModuleFactory.LEGACY_MODULE_FACTORY)
        {
            @Override
            public String getKey()
            {
                return "bar";
            }

            @Override
            public String getCompleteKey()
            {
                return "foo:bar";
            }

            @Override
            public void enabled()
            {
                throw new IllegalArgumentException("Cannot enable");
            }

            @Override
            public Object getModule()
            {
                return null;
            }
        };

        final AbstractModuleDescriptor goodModuleDescriptor = mock(AbstractModuleDescriptor.class);
        when(goodModuleDescriptor.getKey()).thenReturn("baz");
        when(goodModuleDescriptor.getCompleteKey()).thenReturn("foo:baz");

        Plugin plugin = new StaticPlugin()
        {
            @Override
            public Collection<ModuleDescriptor<?>> getModuleDescriptors()
            {
                return Arrays.<ModuleDescriptor<?>>asList(goodModuleDescriptor, badModuleDescriptor);
            }

            @Override
            public ModuleDescriptor<Object> getModuleDescriptor(final String key)
            {
                return badModuleDescriptor;
            }
        };
        plugin.setKey("foo");
        plugin.setEnabledByDefault(true);
        plugin.setPluginInformation(new PluginInformation());

        when(mockPluginLoader.loadAllPlugins(isA(ModuleDescriptorFactory.class))).thenReturn(Collections.singletonList(plugin));

        pluginEventManager.register(new FailListener(PluginEnabledEvent.class));

        MyModuleDisabledListener listener = new MyModuleDisabledListener(goodModuleDescriptor);
        pluginEventManager.register(listener);

        manager = newDefaultPluginManager(mockPluginLoader);
        manager.init();

        assertThat(getPluginAccessor().getPlugins(), hasSize(1));
        assertThat(getPluginAccessor().getEnabledPlugins(), hasSize(0));
        plugin = getPluginAccessor().getPlugin("foo");
        assertFalse(plugin.getPluginState() == PluginState.ENABLED);
        assertTrue(plugin instanceof UnloadablePlugin);
        assertTrue(listener.isCalled());
    }

    public static class MyModuleDisabledListener
    {
        private final ModuleDescriptor goodModuleDescriptor;
        private volatile boolean disableCalled = false;

        public MyModuleDisabledListener(ModuleDescriptor goodModuleDescriptor)
        {
            this.goodModuleDescriptor = goodModuleDescriptor;
        }

        @PluginEventListener
        public void onDisable(PluginModuleDisabledEvent evt)
        {
            if (evt.getModule().equals(goodModuleDescriptor))
            {
                disableCalled = true;
            }
        }

        public boolean isCalled()
        {
            return disableCalled;
        }
    }

    @Test
    public void testEnabledModuleOutOfSyncWithPlugin() throws PluginParseException
    {
        final PluginLoader mockPluginLoader = mock(PluginLoader.class);
        Plugin plugin = new StaticPlugin();
        plugin.setKey("foo");
        plugin.setEnabledByDefault(true);
        plugin.setPluginInformation(new PluginInformation());

        when(mockPluginLoader.loadAllPlugins(isA(ModuleDescriptorFactory.class))).thenReturn(Collections.singletonList(plugin));

        manager = newDefaultPluginManager(mockPluginLoader);
        manager.init();

        assertThat(getPluginAccessor().getPlugins(), hasSize(1));
        assertThat(getPluginAccessor().getEnabledPlugins(), hasSize(1));
        plugin = getPluginAccessor().getPlugin("foo");
        assertTrue(plugin.getPluginState() == PluginState.ENABLED);
        assertTrue(getPluginAccessor().isPluginEnabled("foo"));
        plugin.disable();
        assertFalse(plugin.getPluginState() == PluginState.ENABLED);
        assertFalse(getPluginAccessor().isPluginEnabled("foo"));
    }

    @Test
    public void testDisablePluginModuleWithCannotDisableAnnotation()
    {
        moduleDescriptorFactory.addModuleDescriptor("animal", MockAnimalModuleDescriptor.class);
        moduleDescriptorFactory.addModuleDescriptor("mineral", MockMineralModuleDescriptor.class);
        moduleDescriptorFactory.addModuleDescriptor("bullshit", MockUnusedModuleDescriptor.class);
        moduleDescriptorFactory.addModuleDescriptor("vegetable", MockVegetableModuleDescriptor.class);

        manager = newDefaultPluginManager(new SinglePluginLoader("test-atlassian-plugin.xml"));
        manager.init();

        final String pluginKey = "test.atlassian.plugin";
        final String disablableModuleKey = pluginKey + ":bear";
        final String moduleKey = pluginKey + ":veg";

        // First, make sure we can disable the bear module
        manager.disablePluginModule(disablableModuleKey);
        assertNull(getPluginAccessor().getEnabledPluginModule(disablableModuleKey));

        // Now, make sure we can't disable the veg module
        manager.disablePluginModule(moduleKey);
        assertNotNull(getPluginAccessor().getEnabledPluginModule(moduleKey));
    }

    @Test
    public void testDisablePluginModuleWithCannotDisableAnnotationInSuperclass()
    {
        moduleDescriptorFactory.addModuleDescriptor("animal", MockAnimalModuleDescriptor.class);
        moduleDescriptorFactory.addModuleDescriptor("mineral", MockMineralModuleDescriptor.class);
        moduleDescriptorFactory.addModuleDescriptor("bullshit", MockUnusedModuleDescriptor.class);
        moduleDescriptorFactory.addModuleDescriptor("vegetable", MockVegetableModuleDescriptor.class);
        moduleDescriptorFactory.addModuleDescriptor("vegetableSubclass", MockVegetableSubclassModuleDescriptor.class);

        manager = newDefaultPluginManager(new SinglePluginLoader("test-atlassian-plugin.xml"));
        manager.init();

        final String pluginKey = "test.atlassian.plugin";
        final String disablableModuleKey = pluginKey + ":bear";
        final String moduleKey = pluginKey + ":vegSubclass";

        // First, make sure we can disable the bear module
        manager.disablePluginModule(disablableModuleKey);
        assertNull(getPluginAccessor().getEnabledPluginModule(disablableModuleKey));

        // Now, make sure we can't disable the vegSubclass module
        manager.disablePluginModule(moduleKey);
        assertNotNull(getPluginAccessor().getEnabledPluginModule(moduleKey));
    }

    @Test
    public void testEnabledDisabledRetrieval() throws PluginParseException
    {
        moduleDescriptorFactory.addModuleDescriptor("animal", MockAnimalModuleDescriptor.class);
        moduleDescriptorFactory.addModuleDescriptor("mineral", MockMineralModuleDescriptor.class);
        moduleDescriptorFactory.addModuleDescriptor("bullshit", MockUnusedModuleDescriptor.class);
        moduleDescriptorFactory.addModuleDescriptor("vegetable", MockVegetableModuleDescriptor.class);

        final PassListener enabledListener = new PassListener(PluginEnabledEvent.class);
        final PassListener disabledListener = new PassListener(PluginDisabledEvent.class);
        pluginEventManager.register(enabledListener);
        pluginEventManager.register(disabledListener);

        manager = newDefaultPluginManager(new SinglePluginLoader("test-atlassian-plugin.xml"));
        manager.init();

        // check non existent plugins don't show
        assertNull(getPluginAccessor().getPlugin("bull:shit"));
        assertNull(getPluginAccessor().getEnabledPlugin("bull:shit"));
        assertNull(getPluginAccessor().getPluginModule("bull:shit"));
        assertNull(getPluginAccessor().getEnabledPluginModule("bull:shit"));
        assertTrue(getPluginAccessor().getEnabledModuleDescriptorsByClass(NothingModuleDescriptor.class).isEmpty());
        assertTrue(getPluginAccessor().getEnabledModuleDescriptorsByType("bullshit").isEmpty());

        final String pluginKey = "test.atlassian.plugin";
        final String moduleKey = pluginKey + ":bear";

        // retrieve everything when enabled
        assertNotNull(getPluginAccessor().getPlugin(pluginKey));
        assertNotNull(getPluginAccessor().getEnabledPlugin(pluginKey));
        assertNotNull(getPluginAccessor().getPluginModule(moduleKey));
        assertNotNull(getPluginAccessor().getEnabledPluginModule(moduleKey));
        assertNull(getPluginAccessor().getEnabledPluginModule(pluginKey + ":shit"));
        assertFalse(getPluginAccessor().getEnabledModuleDescriptorsByClass(MockAnimalModuleDescriptor.class).isEmpty());
        assertFalse(getPluginAccessor().getEnabledModuleDescriptorsByType("animal").isEmpty());
        assertFalse(getPluginAccessor().getEnabledModulesByClass(MockBear.class).isEmpty());
        assertEquals(new MockBear(), getPluginAccessor().getEnabledModulesByClass(MockBear.class).get(0));
        enabledListener.assertCalled();

        // now only retrieve via always retrieve methods
        manager.disablePlugin(pluginKey);
        assertNotNull(getPluginAccessor().getPlugin(pluginKey));
        assertNull(getPluginAccessor().getEnabledPlugin(pluginKey));
        assertNotNull(getPluginAccessor().getPluginModule(moduleKey));
        assertNull(getPluginAccessor().getEnabledPluginModule(moduleKey));
        assertTrue(getPluginAccessor().getEnabledModulesByClass(com.atlassian.plugin.mock.MockBear.class).isEmpty());
        assertTrue(getPluginAccessor().getEnabledModuleDescriptorsByClass(MockAnimalModuleDescriptor.class).isEmpty());
        assertTrue(getPluginAccessor().getEnabledModuleDescriptorsByType("animal").isEmpty());
        disabledListener.assertCalled();

        // now enable again and check back to start
        manager.enablePlugin(pluginKey);
        assertNotNull(getPluginAccessor().getPlugin(pluginKey));
        assertNotNull(getPluginAccessor().getEnabledPlugin(pluginKey));
        assertNotNull(getPluginAccessor().getPluginModule(moduleKey));
        assertNotNull(getPluginAccessor().getEnabledPluginModule(moduleKey));
        assertFalse(getPluginAccessor().getEnabledModulesByClass(com.atlassian.plugin.mock.MockBear.class).isEmpty());
        assertFalse(getPluginAccessor().getEnabledModuleDescriptorsByClass(MockAnimalModuleDescriptor.class).isEmpty());
        assertFalse(getPluginAccessor().getEnabledModuleDescriptorsByType("animal").isEmpty());
        enabledListener.assertCalled();

        // now let's disable the module, but not the plugin
        pluginEventManager.register(new FailListener(PluginEnabledEvent.class));
        manager.disablePluginModule(moduleKey);
        assertNotNull(getPluginAccessor().getPlugin(pluginKey));
        assertNotNull(getPluginAccessor().getEnabledPlugin(pluginKey));
        assertNotNull(getPluginAccessor().getPluginModule(moduleKey));
        assertNull(getPluginAccessor().getEnabledPluginModule(moduleKey));
        assertTrue(getPluginAccessor().getEnabledModulesByClass(com.atlassian.plugin.mock.MockBear.class).isEmpty());
        assertTrue(getPluginAccessor().getEnabledModuleDescriptorsByClass(MockAnimalModuleDescriptor.class).isEmpty());
        assertTrue(getPluginAccessor().getEnabledModuleDescriptorsByType("animal").isEmpty());

        // now enable the module again
        pluginEventManager.register(new FailListener(PluginDisabledEvent.class));
        manager.enablePluginModule(moduleKey);
        assertNotNull(getPluginAccessor().getPlugin(pluginKey));
        assertNotNull(getPluginAccessor().getEnabledPlugin(pluginKey));
        assertNotNull(getPluginAccessor().getPluginModule(moduleKey));
        assertNotNull(getPluginAccessor().getEnabledPluginModule(moduleKey));
        assertFalse(getPluginAccessor().getEnabledModulesByClass(com.atlassian.plugin.mock.MockBear.class).isEmpty());
        assertFalse(getPluginAccessor().getEnabledModuleDescriptorsByClass(MockAnimalModuleDescriptor.class).isEmpty());
        assertFalse(getPluginAccessor().getEnabledModuleDescriptorsByType("animal").isEmpty());
    }

    @Test
    public void testDuplicatePluginKeysAreIgnored() throws PluginParseException
    {
        moduleDescriptorFactory.addModuleDescriptor("mineral", MockMineralModuleDescriptor.class);

        manager = newDefaultPluginManager(new SinglePluginLoader("test-atlassian-plugin.xml"), new SinglePluginLoader("test-atlassian-plugin.xml"));
        manager.init();
        assertThat(getPluginAccessor().getEnabledPlugins(), hasSize(1));
    }

    @Test
    public void testDuplicateSnapshotVersionsAreNotLoaded() throws PluginParseException
    {
        moduleDescriptorFactory.addModuleDescriptor("mineral", MockMineralModuleDescriptor.class);

        manager = newDefaultPluginManager(new SinglePluginLoader("test-atlassian-snapshot-plugin.xml"), new SinglePluginLoader("test-atlassian-snapshot-plugin.xml"));
        manager.init();
        assertThat(getPluginAccessor().getEnabledPlugins(), hasSize(1));
    }

    @Test
    public void testChangedSnapshotVersionIsLoaded() throws PluginParseException
    {
        moduleDescriptorFactory.addModuleDescriptor("mineral", MockMineralModuleDescriptor.class);

        manager = newDefaultPluginManager(new SinglePluginLoader("test-atlassian-snapshot-plugin.xml"), new SinglePluginLoader("test-atlassian-snapshot-plugin-changed-same-version.xml"));
        manager.init();
        assertThat(getPluginAccessor().getEnabledPlugins(), hasSize(1));
        final Plugin plugin = getPluginAccessor().getPlugin("test.atlassian.plugin");
        assertEquals("1.1-SNAPSHOT", plugin.getPluginInformation().getVersion());
        assertEquals("This plugin descriptor has been changed!", plugin.getPluginInformation().getDescription());
    }

    @Test
    public void testLoadOlderDuplicatePlugin()
    {
        moduleDescriptorFactory.addModuleDescriptor("mineral", MockMineralModuleDescriptor.class);
        moduleDescriptorFactory.addModuleDescriptor("animal", MockAnimalModuleDescriptor.class);

        manager = newDefaultPluginManager(new MultiplePluginLoader("test-atlassian-plugin-newer.xml"), new MultiplePluginLoader("test-atlassian-plugin.xml", "test-another-plugin.xml"));
        manager.init();
        assertThat(getPluginAccessor().getEnabledPlugins(), hasSize(2));
    }

    @Test
    public void testLoadOlderDuplicatePluginDoesNotTryToEnableIt()
    {
        moduleDescriptorFactory.addModuleDescriptor("mineral", MockMineralModuleDescriptor.class);
        moduleDescriptorFactory.addModuleDescriptor("animal", MockAnimalModuleDescriptor.class);

        final Plugin plugin = new StaticPlugin()
        {
            @Override
            protected PluginState enableInternal()
            {
                fail("enable() must never be called on a earlier version of plugin when later version is installed");
                return null;
            }

            @Override
            public void disableInternal()
            {
                fail("disable() must never be called on a earlier version of plugin when later version is installed");
            }
        };
        plugin.setKey("test.atlassian.plugin");
        plugin.getPluginInformation().setVersion("1.0");

        manager = newDefaultPluginManager(new MultiplePluginLoader("test-atlassian-plugin-newer.xml"));
        manager.init();
        manager.addPlugins(null, Collections.singletonList(plugin));
    }

    @Test
    public void testLoadNewerDuplicatePlugin()
    {
        moduleDescriptorFactory.addModuleDescriptor("mineral", MockMineralModuleDescriptor.class);
        moduleDescriptorFactory.addModuleDescriptor("animal", MockAnimalModuleDescriptor.class);

        manager = newDefaultPluginManager(
                new SinglePluginLoader("test-atlassian-plugin.xml"),
                new SinglePluginLoader("test-atlassian-plugin-newer.xml"));
        manager.init();
        assertThat(getPluginAccessor().getEnabledPlugins(), hasSize(1));
        final Plugin plugin = getPluginAccessor().getPlugin("test.atlassian.plugin");
        assertEquals("1.1", plugin.getPluginInformation().getVersion());
    }

    @Test
    public void testLoadNewerDuplicateDynamicPluginPreservesPluginState() throws PluginParseException
    {
        moduleDescriptorFactory.addModuleDescriptor("mineral", MockMineralModuleDescriptor.class);
        moduleDescriptorFactory.addModuleDescriptor("animal", MockAnimalModuleDescriptor.class);

        manager = newDefaultPluginManager(new SinglePluginLoaderWithRemoval("test-atlassian-plugin.xml"));
        manager.init();

        pluginStateStore.save(PluginPersistentState.Builder.create(pluginStateStore.load()).setEnabled(manager.getPlugin("test.atlassian.plugin"),
                false).toState());

        assertFalse(getPluginAccessor().isPluginEnabled("test.atlassian.plugin"));
        manager.shutdown();

        manager = newDefaultPluginManager(new SinglePluginLoaderWithRemoval("test-atlassian-plugin-newer.xml"));
        manager.init();

        final Plugin plugin = getPluginAccessor().getPlugin("test.atlassian.plugin");
        assertEquals("1.1", plugin.getPluginInformation().getVersion());
        assertFalse(getPluginAccessor().isPluginEnabled("test.atlassian.plugin"));
    }

    @Test
    public void testLoadNewerDuplicateDynamicPluginPreservesModuleState() throws PluginParseException
    {
        moduleDescriptorFactory.addModuleDescriptor("mineral", MockMineralModuleDescriptor.class);
        moduleDescriptorFactory.addModuleDescriptor("animal", MockAnimalModuleDescriptor.class);

        manager = newDefaultPluginManager(new SinglePluginLoaderWithRemoval("test-atlassian-plugin.xml"));
        manager.init();

        pluginStateStore.save(PluginPersistentState.Builder.create(pluginStateStore.load()).setEnabled(
                getPluginAccessor().getPluginModule("test.atlassian.plugin:bear"), false).toState());

        manager.shutdown();

        manager = newDefaultPluginManager(new SinglePluginLoaderWithRemoval("test-atlassian-plugin-newer.xml"));
        manager.init();

        final Plugin plugin = getPluginAccessor().getPlugin("test.atlassian.plugin");
        assertEquals("1.1", plugin.getPluginInformation().getVersion());
        assertFalse(getPluginAccessor().isPluginModuleEnabled("test.atlassian.plugin:bear"));
        assertTrue(getPluginAccessor().isPluginModuleEnabled("test.atlassian.plugin:gold"));
    }

    @Test
    public void testLoadChangedDynamicPluginWithSameVersionNumberDoesNotReplaceExisting() throws PluginParseException
    {
        moduleDescriptorFactory.addModuleDescriptor("mineral", MockMineralModuleDescriptor.class);
        moduleDescriptorFactory.addModuleDescriptor("animal", MockAnimalModuleDescriptor.class);

        manager = newDefaultPluginManager(
                new SinglePluginLoaderWithRemoval("test-atlassian-plugin.xml"),
                new SinglePluginLoaderWithRemoval("test-atlassian-plugin-changed-same-version.xml"));
        manager.init();

        final Plugin plugin = getPluginAccessor().getPlugin("test.atlassian.plugin");
        assertEquals("Test Plugin", plugin.getName());
    }

    @Test
    public void testGetPluginsWithPluginMatchingPluginPredicate() throws Exception
    {
        final Plugin plugin = mockTestPlugin(Collections.emptyList());

        final PluginPredicate mockPluginPredicate = mock(PluginPredicate.class);
        when(mockPluginPredicate.matches(plugin)).thenReturn(true);

        manager = newDefaultPluginManager();
        manager.addPlugins(null, Collections.singletonList(plugin));
        final Collection<Plugin> plugins = getPluginAccessor().getPlugins(mockPluginPredicate);

        assertThat(plugins, hasSize(1));
        assertTrue(plugins.contains(plugin));
        verify(mockPluginPredicate).matches(any(Plugin.class));
    }

    @Test
    public void testGetPluginsWithPluginNotMatchingPluginPredicate() throws Exception
    {
        final Plugin plugin = mockTestPlugin(Collections.emptyList());

        final PluginPredicate mockPluginPredicate = mock(PluginPredicate.class);

        manager = newDefaultPluginManager();
        manager.addPlugins(null, Collections.singletonList(plugin));
        final Collection<Plugin> plugins = getPluginAccessor().getPlugins(mockPluginPredicate);

        assertThat(plugins, hasSize(0));
    }

    @Test
    public void testGetPluginModulesWithModuleMatchingPredicate() throws Exception
    {
        final MockThing module = new MockThing()
        {
        };
        @SuppressWarnings("unchecked")
        final ModuleDescriptor<MockThing> moduleDescriptor = mock(ModuleDescriptor.class);
        when(moduleDescriptor.getModule()).thenReturn(module);
        when(moduleDescriptor.getCompleteKey()).thenReturn("some-plugin-key:module");
        when(moduleDescriptor.isEnabledByDefault()).thenReturn(true);

        final Plugin plugin = mockTestPlugin(Collections.singleton(moduleDescriptor));
        when(plugin.getModuleDescriptor("module")).thenReturn((ModuleDescriptor) moduleDescriptor);

        final ModuleDescriptorPredicate mockModulePredicate = mock(ModuleDescriptorPredicate.class);
        when(mockModulePredicate.matches(moduleDescriptor)).thenReturn(true);

        manager = newDefaultPluginManager();
        manager.addPlugins(null, Collections.singletonList(plugin));
        @SuppressWarnings("unchecked")
        final ModuleDescriptorPredicate<MockThing> predicate = (ModuleDescriptorPredicate<MockThing>) mockModulePredicate;
        final Collection<MockThing> modules = getPluginAccessor().getModules(predicate);

        assertThat(modules, hasSize(1));
        assertTrue(modules.contains(module));

        verify(mockModulePredicate).matches(moduleDescriptor);
    }

    @Test
    public void testGetPluginModulesWithGetModuleThrowingException() throws Exception
    {
        final Plugin badPlugin = new StaticPlugin();
        badPlugin.setKey("bad");
        final MockModuleDescriptor<Object> badDescriptor = new MockModuleDescriptor<Object>(badPlugin, "bad", new Object())
        {
            @Override
            public Object getModule()
            {
                throw new RuntimeException();
            }
        };
        badPlugin.addModuleDescriptor(badDescriptor);

        final Plugin goodPlugin = new StaticPlugin();
        goodPlugin.setKey("good");
        final MockModuleDescriptor<Object> goodDescriptor = new MockModuleDescriptor<Object>(goodPlugin, "good", new Object());
        goodPlugin.addModuleDescriptor(goodDescriptor);

        manager = newDefaultPluginManager();
        manager.addPlugins(null, Arrays.asList(goodPlugin, badPlugin));
        manager.enablePlugin("bad");
        manager.enablePlugin("good");

        assertTrue(getPluginAccessor().isPluginEnabled("bad"));
        assertTrue(getPluginAccessor().isPluginEnabled("good"));
        final Collection<Object> modules = getPluginAccessor().getEnabledModulesByClass(Object.class);

        assertThat(modules, hasSize(1));
        assertFalse(getPluginAccessor().isPluginEnabled("bad"));
        assertTrue(getPluginAccessor().isPluginEnabled("good"));
    }

    @Test
    public void testGetPluginModulesWith2GetModulesThrowingExceptionOnlyNotifiesOnce() throws Exception
    {
        final Plugin badPlugin = new StaticPlugin();
        badPlugin.setKey("bad");
        final MockModuleDescriptor<Object> badDescriptor = new MockModuleDescriptor<Object>(badPlugin, "bad", new Object())
        {
            @Override
            public Object getModule()
            {
                throw new RuntimeException();
            }
        };
        badPlugin.addModuleDescriptor(badDescriptor);
        final MockModuleDescriptor<Object> badDescriptor2 = new MockModuleDescriptor<Object>(badPlugin, "bad2", new Object())
        {
            @Override
            public Object getModule()
            {
                throw new RuntimeException();
            }
        };
        badPlugin.addModuleDescriptor(badDescriptor2);

        final Plugin goodPlugin = new StaticPlugin();
        goodPlugin.setKey("good");
        final MockModuleDescriptor<Object> goodDescriptor = new MockModuleDescriptor<Object>(goodPlugin, "good", new Object());
        goodPlugin.addModuleDescriptor(goodDescriptor);
        DisabledPluginCounter counter = new DisabledPluginCounter();
        pluginEventManager.register(counter);

        manager = newDefaultPluginManager();
        manager.addPlugins(null, Arrays.asList(goodPlugin, badPlugin));
        manager.enablePlugin("bad");
        manager.enablePlugin("good");

        assertTrue(getPluginAccessor().isPluginEnabled("bad"));
        assertTrue(getPluginAccessor().isPluginEnabled("good"));
        final Collection<Object> modules = getPluginAccessor().getEnabledModulesByClass(Object.class);

        assertThat(modules, hasSize(1));
        assertFalse(getPluginAccessor().isPluginEnabled("bad"));
        assertTrue(getPluginAccessor().isPluginEnabled("good"));
        assertEquals(1, counter.disableCount);
    }

    public static class DisabledPluginCounter
    {
        int disableCount = 0;
        @PluginEventListener
        public void consume(PluginDisabledEvent element)
        {
            disableCount++;
        }
    }

    @Test
    public void testGetPluginModulesWithModuleNotMatchingPredicate() throws Exception
    {
        @SuppressWarnings("unchecked")
        final ModuleDescriptor<MockThing> moduleDescriptor = mock(ModuleDescriptor.class);
        when(moduleDescriptor.getCompleteKey()).thenReturn("some-plugin-key:module");
        when(moduleDescriptor.isEnabledByDefault()).thenReturn(true);

        final Plugin plugin = mockTestPlugin(Collections.singleton(moduleDescriptor));
        when(plugin.getModuleDescriptor("module")).thenReturn((ModuleDescriptor) moduleDescriptor);

        @SuppressWarnings("unchecked")
        final ModuleDescriptorPredicate<MockThing> predicate = mock(ModuleDescriptorPredicate.class);

        manager = newDefaultPluginManager();
        manager.addPlugins(null, Collections.singletonList(plugin));
        final Collection<MockThing> modules = getPluginAccessor().getModules(predicate);

        assertThat(modules, hasSize(0));

        verify(predicate).matches(moduleDescriptor);
    }

    @Test
    public void testGetPluginModuleDescriptorWithModuleMatchingPredicate() throws Exception
    {
        @SuppressWarnings("unchecked")
        final ModuleDescriptor<MockThing> moduleDescriptor = mock(ModuleDescriptor.class);
        when(moduleDescriptor.getCompleteKey()).thenReturn("some-plugin-key:module");
        when(moduleDescriptor.isEnabledByDefault()).thenReturn(true);

        final Plugin plugin = mockTestPlugin(Collections.singleton(moduleDescriptor));
        when(plugin.getModuleDescriptor("module")).thenReturn((ModuleDescriptor) moduleDescriptor);

        @SuppressWarnings("unchecked")
        final ModuleDescriptorPredicate<MockThing> predicate = mock(ModuleDescriptorPredicate.class);
        when(predicate.matches(moduleDescriptor)).thenReturn(true);

        manager = newDefaultPluginManager();
        manager.addPlugins(null, Collections.singletonList(plugin));
        final Collection<ModuleDescriptor<MockThing>> modules = getPluginAccessor().getModuleDescriptors(predicate);

        assertThat(modules, hasSize(1));
        assertTrue(modules.contains(moduleDescriptor));

        verify(predicate).matches(moduleDescriptor);
    }

    @Test
    public void testGetPluginModuleDescriptorsWithModuleNotMatchingPredicate() throws Exception
    {
        @SuppressWarnings("unchecked")
        final ModuleDescriptor<MockThing> moduleDescriptor = mock(ModuleDescriptor.class);
        when(moduleDescriptor.getCompleteKey()).thenReturn("some-plugin-key:module");
        when(moduleDescriptor.isEnabledByDefault()).thenReturn(true);

        final Plugin plugin = mockTestPlugin(Collections.singleton(moduleDescriptor));
        when(plugin.getModuleDescriptor("module")).thenReturn((ModuleDescriptor) moduleDescriptor);

        @SuppressWarnings("unchecked")
        final ModuleDescriptorPredicate<MockThing> predicate = mock(ModuleDescriptorPredicate.class);

        manager = newDefaultPluginManager();
        manager.addPlugins(null, Collections.singletonList(plugin));
        final Collection<MockThing> modules = getPluginAccessor().getModules(predicate);

        assertThat(modules, hasSize(0));

        verify(predicate).matches(moduleDescriptor);
    }

    private Plugin mockTestPlugin(Collection moduleDescriptors)
    {
        final Plugin mockPlugin = mock(Plugin.class);
        when(mockPlugin.getKey()).thenReturn("some-plugin-key");
        when(mockPlugin.isEnabledByDefault()).thenReturn(true);
        when(mockPlugin.isEnabled()).thenReturn(true);
        when(mockPlugin.getPluginState()).thenReturn(PluginState.ENABLED);
        when(mockPlugin.getModuleDescriptors()).thenReturn(moduleDescriptors);
        return mockPlugin;
    }

    @Test
    public void testGetPluginAndModules() throws PluginParseException
    {
        moduleDescriptorFactory.addModuleDescriptor("animal", MockAnimalModuleDescriptor.class);
        moduleDescriptorFactory.addModuleDescriptor("mineral", MockMineralModuleDescriptor.class);

        manager = newDefaultPluginManager(new SinglePluginLoader("test-atlassian-plugin.xml"));
        manager.init();

        final Plugin plugin = manager.getPlugin("test.atlassian.plugin");
        assertNotNull(plugin);
        assertEquals("Test Plugin", plugin.getName());

        final ModuleDescriptor<?> bear = plugin.getModuleDescriptor("bear");
        assertEquals(bear, getPluginAccessor().getPluginModule("test.atlassian.plugin:bear"));
    }

    @Test
    public void testGetModuleByModuleClassOneFound() throws PluginParseException
    {
        moduleDescriptorFactory.addModuleDescriptor("animal", MockAnimalModuleDescriptor.class);
        moduleDescriptorFactory.addModuleDescriptor("mineral", MockMineralModuleDescriptor.class);

        manager = newDefaultPluginManager(new SinglePluginLoader("test-atlassian-plugin.xml"));
        manager.init();

        final List<MockAnimalModuleDescriptor> animalDescriptors = getPluginAccessor().getEnabledModuleDescriptorsByClass(MockAnimalModuleDescriptor.class);
        assertNotNull(animalDescriptors);
        assertThat(animalDescriptors, hasSize(1));
        final ModuleDescriptor<MockAnimal> moduleDescriptor = animalDescriptors.iterator().next();
        assertEquals("Bear Animal", moduleDescriptor.getName());

        final List<MockMineralModuleDescriptor> mineralDescriptors = getPluginAccessor().getEnabledModuleDescriptorsByClass(MockMineralModuleDescriptor.class);
        assertNotNull(mineralDescriptors);
        assertThat(mineralDescriptors, hasSize(1));
        final ModuleDescriptor<MockMineral> mineralDescriptor = mineralDescriptors.iterator().next();
        assertEquals("Bar", mineralDescriptor.getName());
    }

    @Test
    public void testGetModuleByModuleClassAndDescriptor() throws PluginParseException
    {
        moduleDescriptorFactory.addModuleDescriptor("animal", MockAnimalModuleDescriptor.class);
        moduleDescriptorFactory.addModuleDescriptor("mineral", MockMineralModuleDescriptor.class);

        manager = newDefaultPluginManager(new SinglePluginLoader("test-atlassian-plugin.xml"));
        manager.init();

        final Collection<MockBear> bearModules = getPluginAccessor().getEnabledModulesByClassAndDescriptor(
                new Class[]{MockAnimalModuleDescriptor.class, MockMineralModuleDescriptor.class}, MockBear.class);
        assertNotNull(bearModules);
        assertThat(bearModules, hasSize(1));
        assertTrue(bearModules.iterator().next() instanceof MockBear);

        final Collection<MockBear> noModules = getPluginAccessor().getEnabledModulesByClassAndDescriptor(new Class[]{}, MockBear.class);
        assertNotNull(noModules);
        assertThat(noModules, hasSize(0));

        final Collection<MockThing> mockThings = getPluginAccessor().getEnabledModulesByClassAndDescriptor(
                new Class[]{MockAnimalModuleDescriptor.class, MockMineralModuleDescriptor.class}, MockThing.class);
        assertNotNull(mockThings);
        assertThat(mockThings, hasSize(2));
        assertTrue(mockThings.iterator().next() instanceof MockThing);
        assertTrue(mockThings.iterator().next() instanceof MockThing);

        final Collection<MockThing> mockThingsFromMineral = getPluginAccessor().getEnabledModulesByClassAndDescriptor(
                new Class[]{MockMineralModuleDescriptor.class}, MockThing.class);
        assertNotNull(mockThingsFromMineral);
        assertThat(mockThingsFromMineral, hasSize(1));
        final Object o = mockThingsFromMineral.iterator().next();
        assertTrue(o instanceof MockMineral);
    }

    @Test
    public void testGetModuleByModuleClassNoneFound() throws PluginParseException
    {
        moduleDescriptorFactory.addModuleDescriptor("animal", MockAnimalModuleDescriptor.class);
        moduleDescriptorFactory.addModuleDescriptor("mineral", MockMineralModuleDescriptor.class);

        manager = newDefaultPluginManager(new SinglePluginLoader("test-atlassian-plugin.xml"));
        manager.init();

        class MockSilver implements MockMineral
        {
            public int getWeight()
            {
                return 3;
            }
        }

        final Collection<MockSilver> descriptors = getPluginAccessor().getEnabledModulesByClass(MockSilver.class);
        assertNotNull(descriptors);
        assertTrue(descriptors.isEmpty());
    }

    @Test
    public void testGetModuleDescriptorsByType() throws PluginParseException
    {
        moduleDescriptorFactory.addModuleDescriptor("animal", MockAnimalModuleDescriptor.class);
        moduleDescriptorFactory.addModuleDescriptor("mineral", MockMineralModuleDescriptor.class);

        manager = newDefaultPluginManager(new SinglePluginLoader("test-atlassian-plugin.xml"));
        manager.init();

        Collection<ModuleDescriptor<MockThing>> descriptors = getPluginAccessor().getEnabledModuleDescriptorsByType("animal");
        assertNotNull(descriptors);
        assertThat(descriptors, hasSize(1));
        ModuleDescriptor<MockThing> moduleDescriptor = descriptors.iterator().next();
        assertEquals("Bear Animal", moduleDescriptor.getName());

        descriptors = getPluginAccessor().getEnabledModuleDescriptorsByType("mineral");
        assertNotNull(descriptors);
        assertThat(descriptors, hasSize(1));
        moduleDescriptor = descriptors.iterator().next();
        assertEquals("Bar", moduleDescriptor.getName());

        try
        {
            getPluginAccessor().getEnabledModuleDescriptorsByType("foobar");
        }
        catch (final IllegalArgumentException e)
        {
            fail("Shouldn't have thrown exception.");
        }
    }

    @Test
    public void testRetrievingDynamicResources() throws PluginParseException, IOException
    {
        createFillAndCleanTempPluginDirectory();

        final DefaultPluginManager manager = makeClassLoadingPluginManager();

        final InputStream is = manager.getPluginResourceAsStream("test.atlassian.plugin.classloaded", "atlassian-plugin.xml");
        assertNotNull(is);
        IOUtils.closeQuietly(is);
    }

    @Test
    public void testGetDynamicPluginClass() throws IOException, PluginParseException
    {
        createFillAndCleanTempPluginDirectory();

        final DefaultPluginManager manager = makeClassLoadingPluginManager();
        try
        {
            manager.getDynamicPluginClass("com.atlassian.plugin.mock.MockPooh");
        }
        catch (final ClassNotFoundException e)
        {
            fail(e.getMessage());
        }
    }

    @Test
    public void testGetEnabledPluginsDoesNotReturnEnablingPlugins() throws Exception
    {
        final PluginLoader mockPluginLoader = mock(PluginLoader.class);

        final Plugin firstPlugin = new StaticPlugin();
        firstPlugin.setKey("first");
        firstPlugin.setEnabledByDefault(false);
        firstPlugin.setPluginInformation(new PluginInformation());

        manager = newDefaultPluginManager();
        manager.enablePluginState(firstPlugin, pluginStateStore);

        final Plugin secondPlugin = new StaticPlugin()
        {
            public PluginState enableInternal()
            {
                try
                {
                    // Assert here when the first plugin has been started but this plugin still has not been loaded
                    assertThat(manager.getPlugins(), hasSize(2));
                    assertThat("First plugin should not be enabled", manager.getEnabledPlugins(), hasSize(0));
                }
                catch (Exception e)
                {
                    throw new RuntimeException(e);
                }
                return PluginState.ENABLED;
            }

            public void disableInternal()
            {
                // do nothing
            }
        };
        secondPlugin.setKey("second");
        secondPlugin.setEnabledByDefault(false);
        secondPlugin.setPluginInformation(new PluginInformation());
        manager.enablePluginState(secondPlugin, pluginStateStore);

        when(mockPluginLoader.loadAllPlugins(any(ModuleDescriptorFactory.class))).thenReturn(Arrays.asList(firstPlugin, secondPlugin));

        manager = newDefaultPluginManager(mockPluginLoader);
        manager.init();
    }

    @Test
    public void testFindingNewPlugins() throws PluginParseException, IOException
    {
        createFillAndCleanTempPluginDirectory();

        //delete paddington for the timebeing
        final File paddington = new File(pluginsTestDir, PADDINGTON_JAR);
        paddington.delete();

        final DefaultPluginManager manager = makeClassLoadingPluginManager();

        assertThat(manager.getPlugins(), hasSize(1));
        assertNotNull(manager.getPlugin("test.atlassian.plugin.classloaded2"));

        //restore paddington to test plugins dir
        FileUtils.copyDirectory(pluginsDirectory, pluginsTestDir);

        manager.scanForNewPlugins();
        assertThat(manager.getPlugins(), hasSize(2));
        assertNotNull(manager.getPlugin("test.atlassian.plugin.classloaded2"));
        assertNotNull(manager.getPlugin("test.atlassian.plugin.classloaded"));

        manager.scanForNewPlugins();
        assertThat(manager.getPlugins(), hasSize(2));
        assertNotNull(manager.getPlugin("test.atlassian.plugin.classloaded2"));
        assertNotNull(manager.getPlugin("test.atlassian.plugin.classloaded"));
    }

    @Test
    public void testFindingNewPluginsNotLoadingRestartRequiredDescriptors() throws PluginParseException, IOException
    {
        createFillAndCleanTempPluginDirectory();

        final DynamicSinglePluginLoader dynamicSinglePluginLoader = new DynamicSinglePluginLoader("test.atlassian.plugin", "test-requiresRestart-plugin.xml");
        manager = makeClassLoadingPluginManager(dynamicSinglePluginLoader);

        moduleDescriptorFactory.addModuleDescriptor("requiresRestart", RequiresRestartModuleDescriptor.class);

        assertThat(manager.getPlugins(), hasSize(2));
        assertNotNull(manager.getPlugin("test.atlassian.plugin.classloaded2"));

        // enable the dynamic plugin loader
        dynamicSinglePluginLoader.canLoad.set(true);

        manager.scanForNewPlugins();
        assertThat(manager.getPlugins(), hasSize(3));
        assertNotNull(manager.getPlugin("test.atlassian.plugin.classloaded2"));
        assertNotNull(manager.getPlugin("test.atlassian.plugin"));

        final Plugin plugin = manager.getPlugin("test.atlassian.plugin");
        assertTrue(plugin instanceof UnloadablePlugin);
        assertTrue(((UnloadablePlugin)plugin).getErrorText().contains("foo"));

        assertEquals(PluginRestartState.INSTALL, manager.getPluginRestartState("test.atlassian.plugin"));
    }

    /**
     * Tests upgrade of plugin where the old version didn't have any restart required module descriptors, but the new one does
     */
    @Test
    public void testFindingUpgradePluginsNotLoadingRestartRequiredDescriptors() throws PluginParseException, IOException
    {
        createFillAndCleanTempPluginDirectory();

        moduleDescriptorFactory.addModuleDescriptor("requiresRestart", RequiresRestartModuleDescriptor.class);

        final DynamicSinglePluginLoader dynamicSinglePluginLoader = new DynamicSinglePluginLoader("test.atlassian.plugin.classloaded2", "test-requiresRestartWithUpgrade-plugin.xml");
        manager = makeClassLoadingPluginManager(dynamicSinglePluginLoader);

        assertThat(manager.getPlugins(), hasSize(2));
        assertNotNull(manager.getPlugin("test.atlassian.plugin.classloaded2"));
        assertThat(manager.getEnabledModuleDescriptorsByClass(RequiresRestartModuleDescriptor.class), hasSize(0));


        dynamicSinglePluginLoader.canLoad.set(true);

        manager.scanForNewPlugins();
        assertThat(manager.getPlugins(), hasSize(2));
        assertNotNull(manager.getPlugin("test.atlassian.plugin.classloaded2"));
        assertEquals(PluginRestartState.UPGRADE, manager.getPluginRestartState("test.atlassian.plugin.classloaded2"));
        assertThat(manager.getEnabledModuleDescriptorsByClass(RequiresRestartModuleDescriptor.class), hasSize(0));
    }

    @Test
    public void testInstallPluginThatRequiresRestart() throws PluginParseException, IOException, InterruptedException
    {
        createFillAndCleanTempPluginDirectory();
        moduleDescriptorFactory.addModuleDescriptor("requiresRestart", RequiresRestartModuleDescriptor.class);
        final DefaultPluginManager manager = makeClassLoadingPluginManager();
        assertThat(manager.getPlugins(), hasSize(2));

        new PluginJarBuilder().addFormattedResource("atlassian-plugin.xml",
                "<atlassian-plugin name='Test 2' i18n-name-key='test.name' key='test.restartrequired' pluginsVersion='1'>", "    <plugin-info>", "        <version>1.0</version>",
                "    </plugin-info>", "    <requiresRestart key='foo' />", "</atlassian-plugin>").build(pluginsTestDir);
        manager.scanForNewPlugins();

        assertThat(manager.getPlugins(), hasSize(3));
        Plugin plugin = manager.getPlugin("test.restartrequired");
        assertNotNull(plugin);
        assertEquals("Test 2", plugin.getName());
        assertEquals("test.name", plugin.getI18nNameKey());
        assertEquals(1, plugin.getPluginsVersion());
        assertEquals("1.0", plugin.getPluginInformation().getVersion());
        assertFalse(manager.isPluginEnabled("test.restartrequired"));
        assertThat(manager.getEnabledModuleDescriptorsByClass(RequiresRestartModuleDescriptor.class), hasSize(0));
        assertEquals(PluginRestartState.INSTALL, manager.getPluginRestartState("test.restartrequired"));

        manager.shutdown();
        manager.init();

        assertThat(manager.getPlugins(), hasSize(3));
        assertNotNull(plugin);
        assertTrue(manager.isPluginEnabled("test.restartrequired"));
        assertThat(manager.getEnabledModuleDescriptorsByClass(RequiresRestartModuleDescriptor.class), hasSize(1));
        assertEquals(PluginRestartState.NONE, manager.getPluginRestartState("test.restartrequired"));
    }

    @Test
    public void testInstallPluginThatRequiresRestartThenRevert() throws PluginParseException, IOException, InterruptedException
    {
        createFillAndCleanTempPluginDirectory();
        moduleDescriptorFactory.addModuleDescriptor("requiresRestart", RequiresRestartModuleDescriptor.class);
        final DefaultPluginManager manager = makeClassLoadingPluginManager();
        manager.setPluginInstaller(new FilePluginInstaller(pluginsTestDir));
        assertThat(manager.getPlugins(), hasSize(2));

        File pluginJar = new PluginJarBuilder().addFormattedResource("atlassian-plugin.xml",
                "<atlassian-plugin name='Test 2' i18n-name-key='test.name' key='test.restartrequired' pluginsVersion='1'>", "    <plugin-info>", "        <version>1.0</version>",
                "    </plugin-info>", "    <requiresRestart key='foo' />", "</atlassian-plugin>").build();
        manager.installPlugin(new JarPluginArtifact(pluginJar));

        assertThat(manager.getPlugins(), hasSize(3));
        Plugin plugin = manager.getPlugin("test.restartrequired");
        assertNotNull(plugin);
        assertEquals("Test 2", plugin.getName());
        assertEquals("test.name", plugin.getI18nNameKey());
        assertEquals(1, plugin.getPluginsVersion());
        assertEquals("1.0", plugin.getPluginInformation().getVersion());
        assertFalse(manager.isPluginEnabled("test.restartrequired"));
        assertThat(manager.getEnabledModuleDescriptorsByClass(RequiresRestartModuleDescriptor.class), hasSize(0));
        assertEquals(PluginRestartState.INSTALL, manager.getPluginRestartState("test.restartrequired"));

        manager.revertRestartRequiredChange("test.restartrequired");
        assertEquals(PluginRestartState.NONE, manager.getPluginRestartState("test.restartrequired"));

        manager.shutdown();
        manager.init();

        assertThat(manager.getPlugins(), hasSize(2));
        assertFalse(manager.isPluginEnabled("test.restartrequired"));
        assertEquals(PluginRestartState.NONE, manager.getPluginRestartState("test.restartrequired"));
    }

    @Test
    public void testUpgradePluginThatRequiresRestart() throws PluginParseException, IOException, InterruptedException
    {
        createFillAndCleanTempPluginDirectory();
        moduleDescriptorFactory.addModuleDescriptor("requiresRestart", RequiresRestartModuleDescriptor.class);

        final File origFile = new PluginJarBuilder().addFormattedResource("atlassian-plugin.xml",
                "<atlassian-plugin name='Test 2' key='test.restartrequired' pluginsVersion='1'>", "    <plugin-info>", "        <version>1.0</version>",
                "    </plugin-info>", "    <requiresRestart key='foo' />", "</atlassian-plugin>").build(pluginsTestDir);

        final DefaultPluginManager manager = makeClassLoadingPluginManager();

        assertThat(manager.getPlugins(), hasSize(3));
        assertNotNull(manager.getPlugin("test.restartrequired"));
        assertTrue(manager.isPluginEnabled("test.restartrequired"));
        assertThat(manager.getEnabledModuleDescriptorsByClass(RequiresRestartModuleDescriptor.class), hasSize(1));
        assertEquals(PluginRestartState.NONE, manager.getPluginRestartState("test.restartrequired"));

        // Some filesystems only record last modified in seconds
        Thread.sleep(1000);
        final File updateFile = new PluginJarBuilder().addFormattedResource("atlassian-plugin.xml",
                "<atlassian-plugin name='Test 2' key='test.restartrequired' pluginsVersion='1'>", "    <plugin-info>", "        <version>2.0</version>",
                "    </plugin-info>", "    <requiresRestart key='foo' />", "    <requiresRestart key='bar' />", "</atlassian-plugin>").build();

        origFile.delete();
        FileUtils.moveFile(updateFile, origFile);

        manager.scanForNewPlugins();
        assertThat(manager.getPlugins(), hasSize(3));
        assertNotNull(manager.getPlugin("test.restartrequired"));
        assertTrue(manager.isPluginEnabled("test.restartrequired"));
        assertEquals(PluginRestartState.UPGRADE, manager.getPluginRestartState("test.restartrequired"));
        assertThat(manager.getEnabledModuleDescriptorsByClass(RequiresRestartModuleDescriptor.class), hasSize(1));

        manager.shutdown();
        manager.init();

        assertThat(manager.getPlugins(), hasSize(3));
        assertNotNull(manager.getPlugin("test.restartrequired"));
        assertTrue(manager.isPluginEnabled("test.restartrequired"));
        assertThat(manager.getEnabledModuleDescriptorsByClass(RequiresRestartModuleDescriptor.class), hasSize(2));
        assertEquals(PluginRestartState.NONE, manager.getPluginRestartState("test.restartrequired"));
    }

    @Test
    public void testUpgradePluginThatRequiresRestartThenReverted() throws PluginParseException, IOException, InterruptedException
    {
        createFillAndCleanTempPluginDirectory();
        moduleDescriptorFactory.addModuleDescriptor("requiresRestart", RequiresRestartModuleDescriptor.class);

        new PluginJarBuilder().addFormattedResource("atlassian-plugin.xml",
                "<atlassian-plugin name='Test 2' key='test.restartrequired' pluginsVersion='1'>",
                "    <plugin-info>",
                "        <version>1.0</version>",
                "    </plugin-info>",
                "    <requiresRestart key='foo' />",
                "</atlassian-plugin>")
                .build(pluginsTestDir);

        final DefaultPluginManager manager = makeClassLoadingPluginManager();
        manager.setPluginInstaller(new FilePluginInstaller(pluginsTestDir));

        assertThat(manager.getPlugins(), hasSize(3));
        assertNotNull(manager.getPlugin("test.restartrequired"));
        assertTrue(manager.isPluginEnabled("test.restartrequired"));
        assertThat(manager.getEnabledModuleDescriptorsByClass(RequiresRestartModuleDescriptor.class), hasSize(1));
        assertEquals(PluginRestartState.NONE, manager.getPluginRestartState("test.restartrequired"));

        // Some filesystems only record last modified in seconds
        Thread.sleep(1000);
        final File updateFile = new PluginJarBuilder().addFormattedResource("atlassian-plugin.xml",
                "<atlassian-plugin name='Test 2' key='test.restartrequired' pluginsVersion='1'>", "    <plugin-info>", "        <version>2.0</version>",
                "    </plugin-info>", "    <requiresRestart key='foo' />", "    <requiresRestart key='bar' />", "</atlassian-plugin>").build();

        manager.installPlugin(new JarPluginArtifact(updateFile));

        assertThat(manager.getPlugins(), hasSize(3));
        assertNotNull(manager.getPlugin("test.restartrequired"));
        assertTrue(manager.isPluginEnabled("test.restartrequired"));
        assertEquals(PluginRestartState.UPGRADE, manager.getPluginRestartState("test.restartrequired"));
        assertThat(manager.getEnabledModuleDescriptorsByClass(RequiresRestartModuleDescriptor.class), hasSize(1));

        manager.revertRestartRequiredChange("test.restartrequired");
        assertEquals(PluginRestartState.NONE, manager.getPluginRestartState("test.restartrequired"));

        manager.shutdown();
        manager.init();

        assertThat(manager.getPlugins(), hasSize(3));
        assertNotNull(manager.getPlugin("test.restartrequired"));
        assertTrue(manager.isPluginEnabled("test.restartrequired"));
        assertEquals("1.0", manager.getPlugin("test.restartrequired").getPluginInformation().getVersion());
        assertThat(manager.getEnabledModuleDescriptorsByClass(RequiresRestartModuleDescriptor.class), hasSize(1));
        assertEquals(PluginRestartState.NONE, manager.getPluginRestartState("test.restartrequired"));
    }

    @Test
    public void testUpgradePluginThatRequiresRestartThenRevertedRevertsToOriginalPlugin() throws PluginParseException, IOException, InterruptedException
    {
        createFillAndCleanTempPluginDirectory();
        moduleDescriptorFactory.addModuleDescriptor("requiresRestart", RequiresRestartModuleDescriptor.class);

        // Add the plugin to the plugins test directory so it is included when we first start up
        new PluginJarBuilder().addFormattedResource("atlassian-plugin.xml",
                "<atlassian-plugin name='Test 2' key='test.restartrequired' pluginsVersion='1'>", "    <plugin-info>", "        <version>1.0</version>",
                "    </plugin-info>", "    <requiresRestart key='foo' />", "</atlassian-plugin>").build(pluginsTestDir);

        final DefaultPluginManager manager = makeClassLoadingPluginManager();
        manager.setPluginInstaller(new FilePluginInstaller(pluginsTestDir));

        assertThat(manager.getPlugins(), hasSize(3));
        assertNotNull(manager.getPlugin("test.restartrequired"));
        assertTrue(manager.isPluginEnabled("test.restartrequired"));
        assertThat(manager.getEnabledModuleDescriptorsByClass(RequiresRestartModuleDescriptor.class), hasSize(1));
        assertEquals(PluginRestartState.NONE, manager.getPluginRestartState("test.restartrequired"));

        // Some filesystems only record last modified in seconds
        Thread.sleep(1000);
        final File updateFile = new PluginJarBuilder().addFormattedResource("atlassian-plugin.xml",
                "<atlassian-plugin name='Test 2' key='test.restartrequired' pluginsVersion='1'>", "    <plugin-info>", "        <version>2.0</version>",
                "    </plugin-info>", "    <requiresRestart key='foo' />", "    <requiresRestart key='bar' />", "</atlassian-plugin>").build();

        // Install version 2 of the plugin
        manager.installPlugin(new JarPluginArtifact(updateFile));

        assertThat(manager.getPlugins(), hasSize(3));
        assertNotNull(manager.getPlugin("test.restartrequired"));
        assertTrue(manager.isPluginEnabled("test.restartrequired"));
        assertEquals(PluginRestartState.UPGRADE, manager.getPluginRestartState("test.restartrequired"));
        assertThat(manager.getEnabledModuleDescriptorsByClass(RequiresRestartModuleDescriptor.class), hasSize(1));

        Thread.sleep(1000);
        final File updateFile2 = new PluginJarBuilder().addFormattedResource("atlassian-plugin.xml",
                "<atlassian-plugin name='Test 2' key='test.restartrequired' pluginsVersion='1'>", "    <plugin-info>", "        <version>3.0</version>",
                "    </plugin-info>", "    <requiresRestart key='foo' />", "    <requiresRestart key='bar' />", "</atlassian-plugin>").build();

        // Install version 3 of the plugin
        manager.installPlugin(new JarPluginArtifact(updateFile2));

        assertThat(manager.getPlugins(), hasSize(3));
        assertNotNull(manager.getPlugin("test.restartrequired"));
        assertTrue(manager.isPluginEnabled("test.restartrequired"));
        assertEquals(PluginRestartState.UPGRADE, manager.getPluginRestartState("test.restartrequired"));
        assertThat(manager.getEnabledModuleDescriptorsByClass(RequiresRestartModuleDescriptor.class), hasSize(1));

        // Lets revert the whole upgrade so that the original plugin is restored
        manager.revertRestartRequiredChange("test.restartrequired");
        assertEquals(PluginRestartState.NONE, manager.getPluginRestartState("test.restartrequired"));
        assertEquals("1.0", manager.getPlugin("test.restartrequired").getPluginInformation().getVersion());

        manager.shutdown();
        manager.init();

        assertThat(manager.getPlugins(), hasSize(3));
        assertNotNull(manager.getPlugin("test.restartrequired"));
        assertTrue(manager.isPluginEnabled("test.restartrequired"));
        assertEquals("1.0", manager.getPlugin("test.restartrequired").getPluginInformation().getVersion());
        assertThat(manager.getEnabledModuleDescriptorsByClass(RequiresRestartModuleDescriptor.class), hasSize(1));
        assertEquals(PluginRestartState.NONE, manager.getPluginRestartState("test.restartrequired"));
    }

    @Test
    public void testUpgradePluginThatRequiresRestartMultipleTimeStaysUpgraded() throws PluginParseException, IOException, InterruptedException
    {
        createFillAndCleanTempPluginDirectory();
        moduleDescriptorFactory.addModuleDescriptor("requiresRestart", RequiresRestartModuleDescriptor.class);

        final File origFile = new PluginJarBuilder().addFormattedResource("atlassian-plugin.xml",
                "<atlassian-plugin name='Test 2' key='test.restartrequired' pluginsVersion='1'>", "    <plugin-info>", "        <version>1.0</version>",
                "    </plugin-info>", "    <requiresRestart key='foo' />", "</atlassian-plugin>").build(pluginsTestDir);

        final DefaultPluginManager manager = makeClassLoadingPluginManager();
        manager.setPluginInstaller(new FilePluginInstaller(pluginsTestDir));

        assertThat(manager.getPlugins(), hasSize(3));
        assertNotNull(manager.getPlugin("test.restartrequired"));
        assertTrue(manager.isPluginEnabled("test.restartrequired"));
        assertThat(manager.getEnabledModuleDescriptorsByClass(RequiresRestartModuleDescriptor.class), hasSize(1));
        assertEquals(PluginRestartState.NONE, manager.getPluginRestartState("test.restartrequired"));

        // Some filesystems only record last modified in seconds
        Thread.sleep(1000);
        final File updateFile = new PluginJarBuilder().addFormattedResource("atlassian-plugin.xml",
                "<atlassian-plugin name='Test 2' key='test.restartrequired' pluginsVersion='1'>", "    <plugin-info>", "        <version>2.0</version>",
                "    </plugin-info>", "    <requiresRestart key='foo' />", "</atlassian-plugin>").build();

        manager.installPlugin(new JarPluginArtifact(updateFile));

        assertThat(manager.getPlugins(), hasSize(3));
        assertNotNull(manager.getPlugin("test.restartrequired"));
        assertTrue(manager.isPluginEnabled("test.restartrequired"));
        assertEquals(PluginRestartState.UPGRADE, manager.getPluginRestartState("test.restartrequired"));
        assertThat(manager.getEnabledModuleDescriptorsByClass(RequiresRestartModuleDescriptor.class), hasSize(1));

        Thread.sleep(1000);
        final File updateFile2 = new PluginJarBuilder().addFormattedResource("atlassian-plugin.xml",
                "<atlassian-plugin name='Test 2' key='test.restartrequired' pluginsVersion='1'>", "    <plugin-info>", "        <version>3.0</version>",
                "    </plugin-info>", "    <requiresRestart key='foo' />", "</atlassian-plugin>").build();

        manager.installPlugin(new JarPluginArtifact(updateFile2));

        assertThat(manager.getPlugins(), hasSize(3));
        assertNotNull(manager.getPlugin("test.restartrequired"));
        assertTrue(manager.isPluginEnabled("test.restartrequired"));
        assertEquals(PluginRestartState.UPGRADE, manager.getPluginRestartState("test.restartrequired"));
        assertThat(manager.getEnabledModuleDescriptorsByClass(RequiresRestartModuleDescriptor.class), hasSize(1));

        manager.shutdown();
        manager.init();

        assertThat(manager.getPlugins(), hasSize(3));
        assertNotNull(manager.getPlugin("test.restartrequired"));
        assertTrue(manager.isPluginEnabled("test.restartrequired"));
        assertThat(manager.getEnabledModuleDescriptorsByClass(RequiresRestartModuleDescriptor.class), hasSize(1));
        assertEquals(PluginRestartState.NONE, manager.getPluginRestartState("test.restartrequired"));
    }

    @Test
    public void testUpgradePluginThatPreviouslyRequiredRestart() throws PluginParseException, IOException, InterruptedException
    {
        createFillAndCleanTempPluginDirectory();
        moduleDescriptorFactory.addModuleDescriptor("requiresRestart", RequiresRestartModuleDescriptor.class);

        final File origFile = new PluginJarBuilder().addFormattedResource("atlassian-plugin.xml",
                "<atlassian-plugin name='Test 2' key='test.restartrequired' pluginsVersion='1'>", "    <plugin-info>", "        <version>1.0</version>",
                "    </plugin-info>", "    <requiresRestart key='foo' />", "</atlassian-plugin>").build(pluginsTestDir);

        final DefaultPluginManager manager = makeClassLoadingPluginManager();

        assertThat(manager.getPlugins(), hasSize(3));
        assertNotNull(manager.getPlugin("test.restartrequired"));
        assertTrue(manager.isPluginEnabled("test.restartrequired"));
        assertThat(manager.getEnabledModuleDescriptorsByClass(RequiresRestartModuleDescriptor.class), hasSize(1));
        assertEquals(PluginRestartState.NONE, manager.getPluginRestartState("test.restartrequired"));

        // Some filesystems only record last modified in seconds
        Thread.sleep(1000);
        final File updateFile = new PluginJarBuilder().addFormattedResource("atlassian-plugin.xml",
                "<atlassian-plugin name='Test 2' key='test.restartrequired' pluginsVersion='1'>", "    <plugin-info>", "        <version>2.0</version>",
                "    </plugin-info>", "</atlassian-plugin>").build(pluginsTestDir);

        origFile.delete();
        FileUtils.moveFile(updateFile, origFile);

        manager.scanForNewPlugins();
        assertThat(manager.getPlugins(), hasSize(3));
        assertNotNull(manager.getPlugin("test.restartrequired"));
        assertTrue(manager.isPluginEnabled("test.restartrequired"));
        assertEquals(PluginRestartState.UPGRADE, manager.getPluginRestartState("test.restartrequired"));
        assertThat(manager.getEnabledModuleDescriptorsByClass(RequiresRestartModuleDescriptor.class), hasSize(1));

        manager.shutdown();
        manager.init();

        assertThat(manager.getEnabledModuleDescriptorsByClass(RequiresRestartModuleDescriptor.class), hasSize(0));
        assertEquals(PluginRestartState.NONE, manager.getPluginRestartState("test.restartrequired"));
    }

    @Test
    public void testInstallPluginThatPreviouslyRequiredRestart() throws PluginParseException, IOException, InterruptedException
    {
        createFillAndCleanTempPluginDirectory();
        moduleDescriptorFactory.addModuleDescriptor("requiresRestart", RequiresRestartModuleDescriptor.class);

        final DefaultPluginManager manager = makeClassLoadingPluginManager();

        assertThat(manager.getPlugins(), hasSize(2));
        assertNull(manager.getPlugin("test.restartrequired"));

        final File origFile = new PluginJarBuilder().addFormattedResource("atlassian-plugin.xml",
                "<atlassian-plugin name='Test 2' key='test.restartrequired' pluginsVersion='1'>", "    <plugin-info>", "        <version>1.0</version>",
                "    </plugin-info>", "    <requiresRestart key='foo' />", "</atlassian-plugin>").build(pluginsTestDir);

        manager.scanForNewPlugins();
        assertThat(manager.getPlugins(), hasSize(3));
        assertNotNull(manager.getPlugin("test.restartrequired"));
        assertFalse(manager.isPluginEnabled("test.restartrequired"));
        assertEquals(PluginRestartState.INSTALL, manager.getPluginRestartState("test.restartrequired"));

        // Some filesystems only record last modified in seconds
        Thread.sleep(1000);
        final File updateFile = new PluginJarBuilder().addFormattedResource("atlassian-plugin.xml",
                "<atlassian-plugin name='Test 2' key='test.restartrequired' pluginsVersion='1'>", "    <plugin-info>", "        <version>2.0</version>",
                "    </plugin-info>", "</atlassian-plugin>").build(pluginsTestDir);

        origFile.delete();
        FileUtils.moveFile(updateFile, origFile);

        manager.scanForNewPlugins();
        assertThat(manager.getPlugins(), hasSize(3));
        assertNotNull(manager.getPlugin("test.restartrequired"));
        assertTrue(manager.isPluginEnabled("test.restartrequired"));
        assertEquals(PluginRestartState.NONE, manager.getPluginRestartState("test.restartrequired"));
        assertThat(manager.getEnabledModuleDescriptorsByClass(RequiresRestartModuleDescriptor.class), hasSize(0));

        manager.shutdown();
        manager.init();

        assertThat(manager.getEnabledModuleDescriptorsByClass(RequiresRestartModuleDescriptor.class), hasSize(0));
        assertEquals(PluginRestartState.NONE, manager.getPluginRestartState("test.restartrequired"));
    }

    @Test
    public void testInstallPluginMoreThanOnceStaysAsInstall() throws PluginParseException, IOException, InterruptedException
    {
        createFillAndCleanTempPluginDirectory();
        moduleDescriptorFactory.addModuleDescriptor("requiresRestart", RequiresRestartModuleDescriptor.class);

        final DefaultPluginManager manager = makeClassLoadingPluginManager();

        assertThat(manager.getPlugins(), hasSize(2));
        assertNull(manager.getPlugin("test.restartrequired"));
        assertThat(manager.getEnabledModuleDescriptorsByClass(RequiresRestartModuleDescriptor.class), hasSize(0));

        final File origFile = new PluginJarBuilder().addFormattedResource("atlassian-plugin.xml",
                "<atlassian-plugin name='Test 2' key='test.restartrequired' pluginsVersion='1'>", "    <plugin-info>", "        <version>1.0</version>",
                "    </plugin-info>", "    <requiresRestart key='foo' />", "</atlassian-plugin>").build(pluginsTestDir);

        manager.scanForNewPlugins();
        assertThat(manager.getPlugins(), hasSize(3));
        assertNotNull(manager.getPlugin("test.restartrequired"));
        assertFalse(manager.isPluginEnabled("test.restartrequired"));
        assertEquals(PluginRestartState.INSTALL, manager.getPluginRestartState("test.restartrequired"));

        // Some filesystems only record last modified in seconds
        Thread.sleep(1000);
        final File updateFile = new PluginJarBuilder().addFormattedResource("atlassian-plugin.xml",
                "<atlassian-plugin name='Test 2' key='test.restartrequired' pluginsVersion='1'>", "    <plugin-info>", "        <version>2.0</version>",
                "    </plugin-info>", "    <requiresRestart key='foo' />", "</atlassian-plugin>").build(pluginsTestDir);

        origFile.delete();
        FileUtils.moveFile(updateFile, origFile);

        manager.scanForNewPlugins();
        assertThat(manager.getPlugins(), hasSize(3));
        assertNotNull(manager.getPlugin("test.restartrequired"));
        assertFalse(manager.isPluginEnabled("test.restartrequired"));
        assertEquals(PluginRestartState.INSTALL, manager.getPluginRestartState("test.restartrequired"));

        manager.shutdown();
        manager.init();

        assertThat(manager.getEnabledModuleDescriptorsByClass(RequiresRestartModuleDescriptor.class), hasSize(1));
        assertEquals(PluginRestartState.NONE, manager.getPluginRestartState("test.restartrequired"));
    }

    @Test
    public void testRemovePluginThatRequiresRestart() throws PluginParseException, IOException
    {
        createFillAndCleanTempPluginDirectory();
        moduleDescriptorFactory.addModuleDescriptor("requiresRestart", RequiresRestartModuleDescriptor.class);

        final File pluginFile = new PluginJarBuilder().addFormattedResource("atlassian-plugin.xml",
                "<atlassian-plugin name='Test 2' key='test.restartrequired' pluginsVersion='1'>", "    <plugin-info>", "        <version>1.0</version>",
                "    </plugin-info>", "    <requiresRestart key='foo' />", "</atlassian-plugin>").build(pluginsTestDir);

        final DefaultPluginManager manager = makeClassLoadingPluginManager();

        assertThat(manager.getPlugins(), hasSize(3));
        assertNotNull(manager.getPlugin("test.restartrequired"));
        assertTrue(manager.isPluginEnabled("test.restartrequired"));
        assertThat(manager.getEnabledModuleDescriptorsByClass(RequiresRestartModuleDescriptor.class), hasSize(1));
        assertEquals(PluginRestartState.NONE, manager.getPluginRestartState("test.restartrequired"));

        manager.uninstall(manager.getPlugin("test.restartrequired"));

        assertThat(manager.getPlugins(), hasSize(3));
        assertNotNull(manager.getPlugin("test.restartrequired"));
        assertTrue(manager.isPluginEnabled("test.restartrequired"));
        assertEquals(PluginRestartState.REMOVE, manager.getPluginRestartState("test.restartrequired"));
        assertThat(manager.getEnabledModuleDescriptorsByClass(RequiresRestartModuleDescriptor.class), hasSize(1));

        manager.shutdown();
        manager.init();

        assertFalse(pluginFile.exists());
        assertThat(manager.getEnabledModuleDescriptorsByClass(RequiresRestartModuleDescriptor.class), hasSize(0));
        assertThat(manager.getPlugins(), hasSize(2));
    }

    @Test
    public void testRemovePluginThatRequiresRestartThenReverted() throws PluginParseException, IOException
    {
        createFillAndCleanTempPluginDirectory();
        moduleDescriptorFactory.addModuleDescriptor("requiresRestart", RequiresRestartModuleDescriptor.class);

        final File pluginFile = new PluginJarBuilder().addFormattedResource("atlassian-plugin.xml",
                "<atlassian-plugin name='Test 2' key='test.restartrequired' pluginsVersion='1'>", "    <plugin-info>", "        <version>1.0</version>",
                "    </plugin-info>", "    <requiresRestart key='foo' />", "</atlassian-plugin>").build(pluginsTestDir);

        final DefaultPluginManager manager = makeClassLoadingPluginManager();

        assertThat(manager.getPlugins(), hasSize(3));
        assertNotNull(manager.getPlugin("test.restartrequired"));
        assertTrue(manager.isPluginEnabled("test.restartrequired"));
        assertThat(manager.getEnabledModuleDescriptorsByClass(RequiresRestartModuleDescriptor.class), hasSize(1));
        assertEquals(PluginRestartState.NONE, manager.getPluginRestartState("test.restartrequired"));

        manager.uninstall(manager.getPlugin("test.restartrequired"));

        assertThat(manager.getPlugins(), hasSize(3));
        assertNotNull(manager.getPlugin("test.restartrequired"));
        assertTrue(manager.isPluginEnabled("test.restartrequired"));
        assertEquals(PluginRestartState.REMOVE, manager.getPluginRestartState("test.restartrequired"));
        assertThat(manager.getEnabledModuleDescriptorsByClass(RequiresRestartModuleDescriptor.class), hasSize(1));

        manager.revertRestartRequiredChange("test.restartrequired");
        assertEquals(PluginRestartState.NONE, manager.getPluginRestartState("test.restartrequired"));

        manager.shutdown();
        manager.init();

        assertThat(manager.getPlugins(), hasSize(3));
        assertNotNull(manager.getPlugin("test.restartrequired"));
        assertTrue(manager.isPluginEnabled("test.restartrequired"));
        assertThat(manager.getEnabledModuleDescriptorsByClass(RequiresRestartModuleDescriptor.class), hasSize(1));
        assertEquals(PluginRestartState.NONE, manager.getPluginRestartState("test.restartrequired"));
    }

    @Test
    public void testRemovePluginThatRequiresRestartViaSubclass() throws PluginParseException, IOException
    {
        createFillAndCleanTempPluginDirectory();
        moduleDescriptorFactory.addModuleDescriptor("requiresRestartSubclass", RequiresRestartSubclassModuleDescriptor.class);

        final File pluginFile = new PluginJarBuilder().addFormattedResource("atlassian-plugin.xml",
                "<atlassian-plugin name='Test 2' key='test.restartrequired' pluginsVersion='1'>", "    <plugin-info>", "        <version>1.0</version>",
                "    </plugin-info>", "    <requiresRestartSubclass key='foo' />", "</atlassian-plugin>").build(pluginsTestDir);

        final DefaultPluginManager manager = makeClassLoadingPluginManager();

        assertThat(manager.getPlugins(), hasSize(3));
        assertNotNull(manager.getPlugin("test.restartrequired"));
        assertTrue(manager.isPluginEnabled("test.restartrequired"));
        assertThat(manager.getEnabledModuleDescriptorsByClass(RequiresRestartSubclassModuleDescriptor.class), hasSize(1));
        assertEquals(PluginRestartState.NONE, manager.getPluginRestartState("test.restartrequired"));

        manager.uninstall(manager.getPlugin("test.restartrequired"));

        assertThat(manager.getPlugins(), hasSize(3));
        assertNotNull(manager.getPlugin("test.restartrequired"));
        assertTrue(manager.isPluginEnabled("test.restartrequired"));
        assertEquals(PluginRestartState.REMOVE, manager.getPluginRestartState("test.restartrequired"));
        assertThat(manager.getEnabledModuleDescriptorsByClass(RequiresRestartSubclassModuleDescriptor.class), hasSize(1));

        manager.shutdown();
        manager.init();

        assertFalse(pluginFile.exists());
        assertThat(manager.getEnabledModuleDescriptorsByClass(RequiresRestartSubclassModuleDescriptor.class), hasSize(0));
        assertThat(manager.getPlugins(), hasSize(2));
    }

    @Test
    public void testDisableEnableOfPluginThatRequiresRestart() throws PluginParseException, IOException
    {
        createFillAndCleanTempPluginDirectory();
        moduleDescriptorFactory.addModuleDescriptor("requiresRestart", RequiresRestartModuleDescriptor.class);

        new PluginJarBuilder().addFormattedResource("atlassian-plugin.xml",
                "<atlassian-plugin name='Test 2' key='test.restartrequired' pluginsVersion='1'>", "    <plugin-info>", "        <version>1.0</version>",
                "    </plugin-info>", "    <requiresRestart key='foo' />", "</atlassian-plugin>").build(pluginsTestDir);

        final DefaultPluginManager manager = makeClassLoadingPluginManager();

        assertThat(manager.getPlugins(), hasSize(3));
        assertNotNull(manager.getPlugin("test.restartrequired"));
        assertTrue(manager.isPluginEnabled("test.restartrequired"));
        assertThat(manager.getEnabledModuleDescriptorsByClass(RequiresRestartModuleDescriptor.class), hasSize(1));
        assertEquals(PluginRestartState.NONE, manager.getPluginRestartState("test.restartrequired"));

        manager.disablePlugin("test.restartrequired");
        assertFalse(manager.isPluginEnabled("test.restartrequired"));
        manager.enablePlugins("test.restartrequired");

        assertThat(manager.getPlugins(), hasSize(3));
        assertNotNull(manager.getPlugin("test.restartrequired"));
        assertTrue(manager.isPluginEnabled("test.restartrequired"));
        assertEquals(PluginRestartState.NONE, manager.getPluginRestartState("test.restartrequired"));
        assertThat(manager.getEnabledModuleDescriptorsByClass(RequiresRestartModuleDescriptor.class), hasSize(1));
    }

    @Test
    public void testCannotRemovePluginFromStaticLoader() throws PluginParseException, IOException
    {
        createFillAndCleanTempPluginDirectory();
        moduleDescriptorFactory.addModuleDescriptor("requiresRestart", RequiresRestartModuleDescriptor.class);

        directoryPluginLoader = new DirectoryPluginLoader(
                pluginsTestDir,
                ImmutableList.<PluginFactory>of(new LegacyDynamicPluginFactory(PluginAccessor.Descriptor.FILENAME), new XmlDynamicPluginFactory(new MockApplication().setKey("key"))),
                pluginEventManager);

        manager = newDefaultPluginManager(directoryPluginLoader, new SinglePluginLoader("test-requiresRestart-plugin.xml"));
        manager.init();

        assertThat(getPluginAccessor().getPlugins(), hasSize(3));
        assertNotNull(getPluginAccessor().getPlugin("test.atlassian.plugin"));
        assertTrue(getPluginAccessor().isPluginEnabled("test.atlassian.plugin"));
        assertThat(getPluginAccessor().getEnabledModuleDescriptorsByClass(RequiresRestartModuleDescriptor.class), hasSize(1));
        assertEquals(PluginRestartState.NONE, getPluginAccessor().getPluginRestartState("test.atlassian.plugin"));

        try
        {
            manager.uninstall(getPluginAccessor().getPlugin("test.atlassian.plugin"));
            fail();
        }
        catch (final PluginException ex)
        {
            // test passed
        }

        assertThat(getPluginAccessor().getPlugins(), hasSize(3));
        assertNotNull(getPluginAccessor().getPlugin("test.atlassian.plugin"));
        assertTrue(getPluginAccessor().isPluginEnabled("test.atlassian.plugin"));
        assertEquals(PluginRestartState.NONE, getPluginAccessor().getPluginRestartState("test.atlassian.plugin"));
        assertThat(getPluginAccessor().getEnabledModuleDescriptorsByClass(RequiresRestartModuleDescriptor.class), hasSize(1));
    }

    private DefaultPluginManager makeClassLoadingPluginManager(PluginLoader... pluginLoaders) throws PluginParseException
    {
        moduleDescriptorFactory.addModuleDescriptor("animal", MockAnimalModuleDescriptor.class);

        directoryPluginLoader = new DirectoryPluginLoader(pluginsTestDir, ImmutableList.<PluginFactory>of(
                new LegacyDynamicPluginFactory(PluginAccessor.Descriptor.FILENAME),
                new XmlDynamicPluginFactory(new MockApplication().setKey("key"))), pluginEventManager);

        final DefaultPluginManager manager = newDefaultPluginManager(Iterables.toArray(ImmutableList.<PluginLoader>builder().add(directoryPluginLoader).addAll(copyOf(pluginLoaders)).build(), PluginLoader.class));

        manager.init();
        return manager;
    }

    @Test
    public void testRemovingPlugins() throws PluginException, IOException
    {
        createFillAndCleanTempPluginDirectory();

        final DefaultPluginManager manager = makeClassLoadingPluginManager();
        assertThat(manager.getPlugins(), hasSize(2));
        final MockAnimalModuleDescriptor moduleDescriptor = (MockAnimalModuleDescriptor) manager.getPluginModule("test.atlassian.plugin.classloaded:paddington");
        assertFalse(moduleDescriptor.disabled);
        final PassListener disabledListener = new PassListener(PluginDisabledEvent.class);
        pluginEventManager.register(disabledListener);
        final Plugin plugin = manager.getPlugin("test.atlassian.plugin.classloaded");
        manager.uninstall(plugin);
        assertTrue("Module must have had disable() called before being removed", moduleDescriptor.disabled);

        // uninstalling a plugin should remove it's state completely from the state store - PLUG-13
        assertTrue(pluginStateStore.load().getPluginStateMap(plugin).isEmpty());

        assertThat(manager.getPlugins(), hasSize(1));
        // plugin is no longer available though the plugin manager
        assertNull(manager.getPlugin("test.atlassian.plugin.classloaded"));
        assertEquals(1, pluginsTestDir.listFiles().length);
        disabledListener.assertCalled();
    }

    @Test
    public void testPluginModuleAvailableAfterInstallation()
    {
        PluginLoader pluginLoader = mock(PluginLoader.class);
        when(pluginLoader.supportsRemoval()).thenReturn(true);
        Plugin plugin = mock(Plugin.class);
        when(plugin.getKey()).thenReturn("dynPlugin");
        when(plugin.isEnabledByDefault()).thenReturn(true);
        when(plugin.isDeleteable()).thenReturn(true);
        when(plugin.isUninstallable()).thenReturn(true);
        when(plugin.getPluginState()).thenReturn(PluginState.ENABLED);
        when(plugin.compareTo(any(Plugin.class))).thenReturn(-1);
        when (pluginLoader.loadAllPlugins(any(ModuleDescriptorFactory.class))).thenReturn(asList(plugin));

        manager = newDefaultPluginManager(pluginLoader);
        manager.init();

        PluginModuleEnabledListener listener = new PluginModuleEnabledListener();
        pluginEventManager.register(listener);
        Collection<ModuleDescriptor<?>> mods = new ArrayList<ModuleDescriptor<?>>();
        MockModuleDescriptor<String> moduleDescriptor = new MockModuleDescriptor<String>(plugin, "foo", "foo");
        mods.add(moduleDescriptor);
        when(plugin.getModuleDescriptors()).thenReturn(mods);
        when(plugin.getModuleDescriptor("foo")).thenReturn((ModuleDescriptor)moduleDescriptor);
        pluginEventManager.broadcast(new PluginModuleAvailableEvent(moduleDescriptor));

        assertTrue(getPluginAccessor().isPluginModuleEnabled("dynPlugin:foo"));
        assertTrue(listener.called);
    }

    @Test
    public void testPluginModuleAvailableAfterInstallationButConfiguredToBeDisabled()
    {
        PluginLoader pluginLoader = mock(PluginLoader.class);
        when(pluginLoader.supportsRemoval()).thenReturn(true);
        Plugin plugin = mock(Plugin.class);
        when(plugin.getKey()).thenReturn("dynPlugin");
        when(plugin.isEnabledByDefault()).thenReturn(true);
        when(plugin.isDeleteable()).thenReturn(true);
        when(plugin.isUninstallable()).thenReturn(true);
        when(plugin.getPluginState()).thenReturn(PluginState.ENABLED);
        when(plugin.compareTo(any(Plugin.class))).thenReturn(-1);
        when (pluginLoader.loadAllPlugins(any(ModuleDescriptorFactory.class))).thenReturn(asList(plugin));

        manager = newDefaultPluginManager(pluginLoader);
        manager.init();

        MockModuleDescriptor<String> moduleDescriptor = new MockModuleDescriptor<String>(plugin, "foo", "foo");

        manager.disablePluginModuleState(moduleDescriptor, manager.getStore());

        PluginModuleEnabledListener listener = new PluginModuleEnabledListener();
        pluginEventManager.register(listener);
        Collection<ModuleDescriptor<?>> mods = new ArrayList<ModuleDescriptor<?>>();
        mods.add(moduleDescriptor);

        when(plugin.getModuleDescriptors()).thenReturn(mods);
        when(plugin.getModuleDescriptor("foo")).thenReturn((ModuleDescriptor)moduleDescriptor);
        pluginEventManager.broadcast(new PluginModuleAvailableEvent(moduleDescriptor));

        assertFalse(getPluginAccessor().isPluginModuleEnabled("dynPlugin:foo"));
        assertFalse(listener.called);
    }

    @Test
    public void testPluginModuleUnavailableAfterInstallation()
    {
        PluginLoader pluginLoader = mock(PluginLoader.class);
        when(pluginLoader.supportsRemoval()).thenReturn(true);
        Plugin plugin = mock(Plugin.class);
        when(plugin.getKey()).thenReturn("dynPlugin");
        when(plugin.isEnabledByDefault()).thenReturn(true);
        when(plugin.isDeleteable()).thenReturn(true);
        when(plugin.isUninstallable()).thenReturn(true);
        when(plugin.getPluginState()).thenReturn(PluginState.ENABLED);
        when(plugin.compareTo(any(Plugin.class))).thenReturn(-1);
        when (pluginLoader.loadAllPlugins(any(ModuleDescriptorFactory.class))).thenReturn(asList(plugin));

        manager = newDefaultPluginManager(pluginLoader);
        manager.init();

        PluginModuleDisabledListener listener = new PluginModuleDisabledListener();
        pluginEventManager.register(listener);
        Collection<ModuleDescriptor<?>> mods = new ArrayList<ModuleDescriptor<?>>();
        MockModuleDescriptor<String> moduleDescriptor = new MockModuleDescriptor<String>(plugin, "foo", "foo");
        mods.add(moduleDescriptor);
        when(plugin.getModuleDescriptors()).thenReturn(mods);
        when(plugin.getModuleDescriptor("foo")).thenReturn((ModuleDescriptor)moduleDescriptor);
        pluginEventManager.broadcast(new PluginModuleAvailableEvent(moduleDescriptor));
        assertTrue(getPluginAccessor().isPluginModuleEnabled("dynPlugin:foo"));
        assertFalse(listener.called);
        pluginEventManager.broadcast(new PluginModuleUnavailableEvent(moduleDescriptor));
        assertTrue(listener.called);
    }

    @Test
    public void testPluginContainerUnavailable()
    {
        PluginLoader pluginLoader = mock(PluginLoader.class);
        when(pluginLoader.supportsRemoval()).thenReturn(true);
        Plugin plugin = mock(Plugin.class);
        when(plugin.getKey()).thenReturn("dynPlugin");
        when(plugin.isEnabledByDefault()).thenReturn(true);
        when(plugin.isDeleteable()).thenReturn(true);
        when(plugin.isUninstallable()).thenReturn(true);
        when(plugin.getPluginState()).thenReturn(PluginState.ENABLED);
        when(plugin.compareTo(any(Plugin.class))).thenReturn(-1);
        Collection<ModuleDescriptor<?>> mods = new ArrayList<ModuleDescriptor<?>>();
        MockModuleDescriptor<String> moduleDescriptor = new MockModuleDescriptor<String>(plugin, "foo", "foo");
        mods.add(moduleDescriptor);
        when(plugin.getModuleDescriptors()).thenReturn(mods);
        when(plugin.getModuleDescriptor("foo")).thenReturn((ModuleDescriptor)moduleDescriptor);
        when (pluginLoader.loadAllPlugins(any(ModuleDescriptorFactory.class))).thenReturn(asList(plugin));

        manager = newDefaultPluginManager(pluginLoader);
        manager.init();

        PluginDisabledListener listener = new PluginDisabledListener();
        PluginModuleDisabledListener moduleDisabledListener = new PluginModuleDisabledListener();
        pluginEventManager.register(listener);
        pluginEventManager.register(moduleDisabledListener);
        when(plugin.getPluginState()).thenReturn(PluginState.DISABLED);
        pluginEventManager.broadcast(new PluginContainerUnavailableEvent("dynPlugin"));
        //Fix in behaviour, if the plugin is already disabled and you try to disable it there should be no event called
        assertFalse(getPluginAccessor().isPluginEnabled("dynPlugin"));
        assertFalse(listener.called);
        assertFalse(moduleDisabledListener.called);
    }

    @Test
    public void testUninstallPluginWithDependencies() throws PluginException, IOException
    {
        PluginLoader pluginLoader = mock(PluginLoader.class);
        when(pluginLoader.supportsRemoval()).thenReturn(true);
        Plugin child = mock(Plugin.class);
        when(child.getKey()).thenReturn("child");
        when(child.isEnabledByDefault()).thenReturn(true);
        when(child.getPluginState()).thenReturn(PluginState.ENABLED);
        when(child.getRequiredPlugins()).thenReturn(singleton("parent"));
        when(child.compareTo(any(Plugin.class))).thenReturn(-1);
        Plugin parent = mock(Plugin.class);
        when(parent.getKey()).thenReturn("parent");
        when(parent.isEnabledByDefault()).thenReturn(true);
        when(parent.isDeleteable()).thenReturn(true);
        when(parent.isUninstallable()).thenReturn(true);
        when(parent.getPluginState()).thenReturn(PluginState.ENABLED);
        when(parent.compareTo(any(Plugin.class))).thenReturn(-1);
        when (pluginLoader.loadAllPlugins(any(ModuleDescriptorFactory.class))).thenReturn(asList(child, parent));

        manager = newDefaultPluginManager(pluginLoader);
        manager.init();

        manager.uninstall(parent);
        verify(parent).enable();
        verify(parent).disable();
        verify(pluginLoader).removePlugin(parent);

        verify(child).enable();
        verify(child).disable();
    }

    @Test
    public void testUninstallPluginWithMultiLevelDependencies() throws PluginException, IOException
    {
        PluginLoader pluginLoader = mock(PluginLoader.class);
        when(pluginLoader.supportsRemoval()).thenReturn(true);

        Plugin child = mock(Plugin.class);
        when(child.getKey()).thenReturn("child");
        when(child.isEnabledByDefault()).thenReturn(true);
        when(child.getPluginState()).thenReturn(PluginState.ENABLED);
        when(child.getRequiredPlugins()).thenReturn(singleton("parent"));
        when(child.compareTo(any(Plugin.class))).thenReturn(-1);

        Plugin parent = mock(Plugin.class);
        when(parent.getKey()).thenReturn("parent");
        when(parent.isEnabledByDefault()).thenReturn(true);
        when(parent.getPluginState()).thenReturn(PluginState.ENABLED);
        when(parent.getRequiredPlugins()).thenReturn(singleton("grandparent"));
        when(parent.compareTo(any(Plugin.class))).thenReturn(-1);

        Plugin grandparent = mock(Plugin.class);
        when(grandparent.getKey()).thenReturn("grandparent");
        when(grandparent.isEnabledByDefault()).thenReturn(true);
        when(grandparent.isDeleteable()).thenReturn(true);
        when(grandparent.isUninstallable()).thenReturn(true);
        when(grandparent.getPluginState()).thenReturn(PluginState.ENABLED);
        when(grandparent.compareTo(any(Plugin.class))).thenReturn(-1);
        when(pluginLoader.loadAllPlugins(any(ModuleDescriptorFactory.class))).thenReturn(asList(child, parent, grandparent));
        manager = newDefaultPluginManager(pluginLoader);
        manager.init();

        manager.uninstall(grandparent);
        verify(grandparent).enable();
        verify(grandparent).disable();
        verify(pluginLoader).removePlugin(grandparent);

        verify(parent).enable();
        verify(parent).disable();
        verify(child).enable();
        verify(child).disable();
    }

    @Test
    public void testCircularDependencyWouldNotCauseInfiniteLoop() throws PluginException, IOException
    {
        PluginLoader pluginLoader = mock(PluginLoader.class);
        when(pluginLoader.supportsRemoval()).thenReturn(true);

        Plugin p1 = mock(Plugin.class);
        when(p1.getKey()).thenReturn("p1");
        when(p1.isEnabledByDefault()).thenReturn(true);
        when(p1.getPluginState()).thenReturn(PluginState.ENABLED);
        when(p1.getRequiredPlugins()).thenReturn(ImmutableSet.of("p2", "parent"));
        when(p1.compareTo(any(Plugin.class))).thenReturn(-1);

        // Create a circular dependency between p1 and p2. This should not happen, but test anyway.
        Plugin p2 = mock(Plugin.class);
        when(p2.getKey()).thenReturn("p2");
        when(p2.isEnabledByDefault()).thenReturn(true);
        when(p2.getPluginState()).thenReturn(PluginState.ENABLED);
        when(p2.getRequiredPlugins()).thenReturn(singleton("p1"));
        when(p2.compareTo(any(Plugin.class))).thenReturn(-1);

        Plugin parent = mock(Plugin.class);
        when(parent.getKey()).thenReturn("parent");
        when(parent.isEnabledByDefault()).thenReturn(true);
        when(parent.isDeleteable()).thenReturn(true);
        when(parent.isUninstallable()).thenReturn(true);
        when(parent.getPluginState()).thenReturn(PluginState.ENABLED);
        when(parent.compareTo(any(Plugin.class))).thenReturn(-1);
        when (pluginLoader.loadAllPlugins(any(ModuleDescriptorFactory.class))).thenReturn(asList(p1, p2, parent));
        manager = newDefaultPluginManager(pluginLoader);
        manager.init();

        manager.uninstall(parent);
        verify(parent, times(1)).enable();
        verify(parent, times(1)).disable();
        verify(pluginLoader).removePlugin(parent);

        verify(p1, times(1)).enable();
        verify(p1, times(1)).disable();
        verify(p2, times(1)).enable();
        verify(p2, times(1)).disable();
    }

    @Test
    public void testThreeCycleDependencyWouldNotCauseInfiniteLoop() throws PluginException, IOException
    {
        PluginLoader pluginLoader = mock(PluginLoader.class);
        when(pluginLoader.supportsRemoval()).thenReturn(true);

        Plugin p1 = mock(Plugin.class);
        when(p1.getKey()).thenReturn("p1");
        when(p1.isEnabledByDefault()).thenReturn(true);
        when(p1.getPluginState()).thenReturn(PluginState.ENABLED);
        when(p1.getRequiredPlugins()).thenReturn(ImmutableSet.of("p2", "parent"));
        when(p1.compareTo(any(Plugin.class))).thenReturn(-1);

        // Create a circular dependency between p1, p2 and p3. This should not happen, but test anyway.
        Plugin p2 = mock(Plugin.class);
        when(p2.getKey()).thenReturn("p2");
        when(p2.isEnabledByDefault()).thenReturn(true);
        when(p2.getPluginState()).thenReturn(PluginState.ENABLED);
        when(p2.getRequiredPlugins()).thenReturn(singleton("p3"));
        when(p2.compareTo(any(Plugin.class))).thenReturn(-1);

        Plugin p3 = mock(Plugin.class);
        when(p3.getKey()).thenReturn("p3");
        when(p3.isEnabledByDefault()).thenReturn(true);
        when(p3.getPluginState()).thenReturn(PluginState.ENABLED);
        when(p3.getRequiredPlugins()).thenReturn(singleton("p1"));
        when(p3.compareTo(any(Plugin.class))).thenReturn(-1);

        Plugin parent = mock(Plugin.class);
        when(parent.getKey()).thenReturn("parent");
        when(parent.isEnabledByDefault()).thenReturn(true);
        when(parent.isDeleteable()).thenReturn(true);
        when(parent.isUninstallable()).thenReturn(true);
        when(parent.getPluginState()).thenReturn(PluginState.ENABLED);
        when(parent.compareTo(any(Plugin.class))).thenReturn(-1);
        when (pluginLoader.loadAllPlugins(any(ModuleDescriptorFactory.class))).thenReturn(asList(p1, p2, p3, parent));
        manager = newDefaultPluginManager(pluginLoader);
        manager.init();

        manager.uninstall(parent);
        verify(parent, times(1)).enable();
        verify(parent, times(1)).disable();
        verify(pluginLoader).removePlugin(parent);

        verify(p1, times(1)).enable();
        verify(p1, times(1)).disable();
        verify(p2, times(1)).enable();
        verify(p2, times(1)).disable();
        verify(p3, times(1)).enable();
        verify(p3, times(1)).disable();
    }

    @Test
    public void testNonRemovablePlugins() throws PluginParseException
    {
        moduleDescriptorFactory.addModuleDescriptor("animal", MockAnimalModuleDescriptor.class);
        moduleDescriptorFactory.addModuleDescriptor("mineral", MockMineralModuleDescriptor.class);

        manager = newDefaultPluginManager(new SinglePluginLoader("test-atlassian-plugin.xml"));
        manager.init();

        final Plugin plugin = getPluginAccessor().getPlugin("test.atlassian.plugin");
        assertFalse(plugin.isUninstallable());
        assertNotNull(plugin.getResourceAsStream("test-atlassian-plugin.xml"));

        try
        {
            manager.uninstall(plugin);
            fail("Where was the exception?");
        }
        catch (final PluginException p)
        {
        }
    }

    @Test
    public void testNonDeletablePlugins() throws PluginException, IOException
    {
        createFillAndCleanTempPluginDirectory();

        final DefaultPluginManager manager = makeClassLoadingPluginManager();
        assertThat(manager.getPlugins(), hasSize(2));

        // Set plugin file can't be deleted.
        final Plugin pluginToRemove = new AbstractDelegatingPlugin(manager.getPlugin("test.atlassian.plugin.classloaded"))
        {
            public boolean isDeleteable()
            {
                return false;
            }
        };

        // Disable plugin module before uninstall
        final MockAnimalModuleDescriptor moduleDescriptor = (MockAnimalModuleDescriptor) manager.getPluginModule("test.atlassian.plugin.classloaded:paddington");
        assertFalse(moduleDescriptor.disabled);

        manager.uninstall(pluginToRemove);

        assertTrue("Module must have had disable() called before being removed", moduleDescriptor.disabled);
        assertThat(manager.getPlugins(), hasSize(1));
        assertNull(manager.getPlugin("test.atlassian.plugin.classloaded"));
        assertEquals(2, pluginsTestDir.listFiles().length);
    }

    // These methods test the plugin compareTo() function, which compares plugins based on their version numbers.
    @Test
    public void testComparePluginNewer()
    {

        final Plugin p1 = createPluginWithVersion("1.1");
        final Plugin p2 = createPluginWithVersion("1.0");
        assertTrue(p1.compareTo(p2) == 1);

        p1.getPluginInformation().setVersion("1.10");
        p2.getPluginInformation().setVersion("1.2");
        assertTrue(p1.compareTo(p2) == 1);

        p1.getPluginInformation().setVersion("1.2");
        p2.getPluginInformation().setVersion("1.01");
        assertTrue(p1.compareTo(p2) == 1);

        p1.getPluginInformation().setVersion("1.0.1");
        p2.getPluginInformation().setVersion("1.0");
        assertTrue(p1.compareTo(p2) == 1);

        p1.getPluginInformation().setVersion("1.2");
        p2.getPluginInformation().setVersion("1.1.1");
        assertTrue(p1.compareTo(p2) == 1);
    }

    @Test
    public void testComparePluginOlder()
    {
        final Plugin p1 = createPluginWithVersion("1.0");
        final Plugin p2 = createPluginWithVersion("1.1");
        assertTrue(p1.compareTo(p2) == -1);

        p1.getPluginInformation().setVersion("1.2");
        p2.getPluginInformation().setVersion("1.10");
        assertTrue(p1.compareTo(p2) == -1);

        p1.getPluginInformation().setVersion("1.01");
        p2.getPluginInformation().setVersion("1.2");
        assertTrue(p1.compareTo(p2) == -1);

        p1.getPluginInformation().setVersion("1.0");
        p2.getPluginInformation().setVersion("1.0.1");
        assertTrue(p1.compareTo(p2) == -1);

        p1.getPluginInformation().setVersion("1.1.1");
        p2.getPluginInformation().setVersion("1.2");
        assertTrue(p1.compareTo(p2) == -1);
    }

    @Test
    public void testComparePluginEqual()
    {
        final Plugin p1 = createPluginWithVersion("1.0");
        final Plugin p2 = createPluginWithVersion("1.0");
        assertTrue(p1.compareTo(p2) == 0);

        p1.getPluginInformation().setVersion("1.1.0.0");
        p2.getPluginInformation().setVersion("1.1");
        assertTrue(p1.compareTo(p2) == 0);

        p1.getPluginInformation().setVersion(" 1 . 1 ");
        p2.getPluginInformation().setVersion("1.1");
        assertTrue(p1.compareTo(p2) == 0);
    }

    // If we can't understand the version of a plugin, then take the new one.
    @Test
    public void testComparePluginNoVersion()
    {
        final Plugin p1 = createPluginWithVersion("1.0");
        final Plugin p2 = createPluginWithVersion("#$%");
        assertEquals(1, p1.compareTo(p2));

        p1.getPluginInformation().setVersion("#$%");
        p2.getPluginInformation().setVersion("1.0");
        assertEquals(-1, p1.compareTo(p2));
    }

    @Test
    public void testComparePluginBadPlugin()
    {
        final Plugin p1 = createPluginWithVersion("1.0");
        final Plugin p2 = createPluginWithVersion("1.0");

        // Compare against something with a different key
        p2.setKey("bad.key");
        assertTrue(p1.compareTo(p2) != 0);
    }

    @Test
    public void testInvalidationOfDynamicResourceCache() throws IOException, PluginException
    {
        createFillAndCleanTempPluginDirectory();

        final DefaultPluginManager manager = makeClassLoadingPluginManager();

        checkResources(manager, true, true);
        manager.disablePlugin("test.atlassian.plugin.classloaded");
        checkResources(manager, false, false);
        manager.enablePlugin("test.atlassian.plugin.classloaded");
        checkResources(manager, true, true);
        manager.uninstall(manager.getPlugin("test.atlassian.plugin.classloaded"));
        checkResources(manager, false, false);
        //restore paddington to test plugins dir
        FileUtils.copyDirectory(pluginsDirectory, pluginsTestDir);
        manager.scanForNewPlugins();
        checkResources(manager, true, true);
        // Resources from disabled modules are still available
        //manager.disablePluginModule("test.atlassian.plugin.classloaded:paddington");
        //checkResources(manager, true, false);
    }

    @Test
    public void testValidatePlugin() throws PluginParseException
    {
        final DynamicPluginLoader mockLoader = mock(DynamicPluginLoader.class);
        when(mockLoader.isDynamicPluginLoader()).thenReturn(true);

        manager = new DefaultPluginManager(pluginStateStore, ImmutableList.<PluginLoader>of(mockLoader), moduleDescriptorFactory, new DefaultPluginEventManager());

        final PluginArtifact mockPluginJar = mock(PluginArtifact.class);
        final PluginArtifact pluginArtifact = mockPluginJar;
        when(mockLoader.canLoad(pluginArtifact)).thenReturn("foo");

        final String key = manager.validatePlugin(pluginArtifact);
        assertEquals("foo", key);
        verify(mockLoader).canLoad(pluginArtifact);
    }

    @Test
    public void testValidatePluginWithNoDynamicLoaders() throws PluginParseException
    {
        final PluginLoader loader = mock(PluginLoader.class);
        final DefaultPluginManager manager = new DefaultPluginManager(pluginStateStore, ImmutableList.<PluginLoader>of(loader), moduleDescriptorFactory, new DefaultPluginEventManager());

        final PluginArtifact pluginArtifact = mock(PluginArtifact.class);
        try
        {
            manager.validatePlugin(pluginArtifact);
            fail("Should have thrown exception");
        }
        catch (final IllegalStateException ex)
        {
            // test passed
        }
    }

    @Test
    public void testInvalidationOfDynamicClassCache() throws IOException, PluginException
    {
        createFillAndCleanTempPluginDirectory();

        final DefaultPluginManager manager = makeClassLoadingPluginManager();

        checkClasses(manager, true);
        manager.disablePlugin("test.atlassian.plugin.classloaded");
        checkClasses(manager, false);
        manager.enablePlugin("test.atlassian.plugin.classloaded");
        checkClasses(manager, true);
        manager.uninstall(manager.getPlugin("test.atlassian.plugin.classloaded"));
        checkClasses(manager, false);
        //restore paddington to test plugins dir
        FileUtils.copyDirectory(pluginsDirectory, pluginsTestDir);
        manager.scanForNewPlugins();
        checkClasses(manager, true);
    }

    @Test
    public void testInstallPlugin() throws Exception
    {
        final PluginPersistentStateStore mockPluginStateStore = mock(PluginPersistentStateStore.class);
        final ModuleDescriptorFactory moduleDescriptorFactory = mock(ModuleDescriptorFactory.class);
        final DynamicPluginLoader mockPluginLoader = mock(DynamicPluginLoader.class);
        when(mockPluginLoader.isDynamicPluginLoader()).thenReturn(true);

        final DescriptorParserFactory mockDescriptorParserFactory = mock(DescriptorParserFactory.class);
        final DescriptorParser mockDescriptorParser = mock(DescriptorParser.class);
        final PluginArtifact pluginArtifact = mock(PluginArtifact.class);
        final PluginInstaller mockRepository = mock(PluginInstaller.class);
        final Plugin plugin = mock(Plugin.class);


        final DefaultPluginManager pluginManager = new DefaultPluginManager(mockPluginStateStore,
                Collections.<PluginLoader>singletonList(mockPluginLoader), moduleDescriptorFactory, pluginEventManager);

        when(mockPluginStateStore.load()).thenReturn(new DefaultPluginPersistentState());
        when(mockPluginStateStore.load()).thenReturn(new DefaultPluginPersistentState());
        when(mockPluginStateStore.load()).thenReturn(new DefaultPluginPersistentState());
        when(mockDescriptorParser.getKey()).thenReturn("test");
        when(mockPluginLoader.loadAllPlugins(moduleDescriptorFactory)).thenReturn((Iterable) Collections.emptyList());
        when(mockPluginLoader.supportsAddition()).thenReturn(true);
        when(mockPluginLoader.loadFoundPlugins(moduleDescriptorFactory)).thenReturn(Collections.singletonList(plugin));
        when(mockPluginLoader.canLoad(pluginArtifact)).thenReturn("test");
        when(plugin.getKey()).thenReturn("test");
        when(plugin.getModuleDescriptors()).thenReturn((Collection) new ArrayList<Object>());
        when(plugin.getModuleDescriptors()).thenReturn((Collection) new ArrayList<Object>());
        when(plugin.isEnabledByDefault()).thenReturn(true);

        when(plugin.isEnabledByDefault()).thenReturn(true);
        when(plugin.isEnabled()).thenReturn(true);
        when(plugin.getPluginState()).thenReturn(PluginState.ENABLED);
        when(plugin.hasAllPermissions()).thenReturn(true);
        when(plugin.getActivePermissions()).thenReturn(ImmutableSet.of(Permissions.ALL_PERMISSIONS));

        pluginManager.setPluginInstaller(mockRepository);
        pluginManager.init();
        final PassListener enabledListener = new PassListener(PluginEnabledEvent.class);
        pluginEventManager.register(enabledListener);
        pluginManager.installPlugin(pluginArtifact);

        assertEquals(plugin, pluginManager.getPlugin("test"));
        assertTrue(pluginManager.isPluginEnabled("test"));

//        plugin.verify();
//        mockRepository.verify();
//        pluginArtifact.verify();
//        mockDescriptorParser.verify();
//        mockDescriptorParserFactory.verify();
//        mockPluginLoader.verify();
//        mockPluginStateStore.verify();
        enabledListener.assertCalled();
    }

    @Test
    public void testInstallPluginsWithOne()
    {
        DynamicPluginLoader loader = mock(DynamicPluginLoader.class);
        when(loader.isDynamicPluginLoader()).thenReturn(true);

        ModuleDescriptorFactory descriptorFactory = mock(ModuleDescriptorFactory.class);
        PluginEventManager eventManager = mock(PluginEventManager.class);
        PluginInstaller installer = mock(PluginInstaller.class);
        DefaultPluginManager pm = new DefaultPluginManager(new MemoryPluginPersistentStateStore(), Collections.<PluginLoader>singletonList(loader), descriptorFactory, eventManager);
        pm.setPluginInstaller(installer);
        PluginArtifact artifact = mock(PluginArtifact.class);
        Plugin plugin = mock(Plugin.class);
        when(loader.canLoad(artifact)).thenReturn("foo");
        when(loader.loadFoundPlugins(descriptorFactory)).thenReturn(Arrays.asList(plugin));

        pm.installPlugins(artifact);

        verify(loader).canLoad(artifact);
        verify(installer).installPlugin("foo", artifact);
    }

    @Test
    public void testInstallPluginsWithTwo()
    {
        DynamicPluginLoader loader = mock(DynamicPluginLoader.class);
        when(loader.isDynamicPluginLoader()).thenReturn(true);

        ModuleDescriptorFactory descriptorFactory = mock(ModuleDescriptorFactory.class);
        PluginEventManager eventManager = mock(PluginEventManager.class);
        PluginInstaller installer = mock(PluginInstaller.class);
        DefaultPluginManager pm = new DefaultPluginManager(new MemoryPluginPersistentStateStore(), Collections.<PluginLoader>singletonList(loader), descriptorFactory, eventManager);
        pm.setPluginInstaller(installer);
        PluginArtifact artifactA = mock(PluginArtifact.class);
        Plugin pluginA = mock(Plugin.class);
        when(loader.canLoad(artifactA)).thenReturn("a");
        PluginArtifact artifactB = mock(PluginArtifact.class);
        Plugin pluginB = mock(Plugin.class);
        when(loader.canLoad(artifactB)).thenReturn("b");

        when(loader.loadFoundPlugins(descriptorFactory)).thenReturn(Arrays.asList(pluginA, pluginB));

        pm.installPlugins(artifactA, artifactB);

        verify(loader).canLoad(artifactA);
        verify(loader).canLoad(artifactB);
        verify(installer).installPlugin("a", artifactA);
        verify(installer).installPlugin("b", artifactB);
    }

    @Test
    public void testInstallPluginsWithTwoButOneFailsValidation()
    {
        DynamicPluginLoader loader = mock(DynamicPluginLoader.class);
        when(loader.isDynamicPluginLoader()).thenReturn(true);

        ModuleDescriptorFactory descriptorFactory = mock(ModuleDescriptorFactory.class);
        PluginEventManager eventManager = mock(PluginEventManager.class);
        PluginInstaller installer = mock(PluginInstaller.class);
        DefaultPluginManager pm = new DefaultPluginManager(new MemoryPluginPersistentStateStore(), Collections.<PluginLoader>singletonList(loader), descriptorFactory, eventManager);
        pm.setPluginInstaller(installer);
        PluginArtifact artifactA = mock(PluginArtifact.class);
        Plugin pluginA = mock(Plugin.class);
        when(loader.canLoad(artifactA)).thenReturn("a");
        PluginArtifact artifactB = mock(PluginArtifact.class);
        Plugin pluginB = mock(Plugin.class);
        when(loader.canLoad(artifactB)).thenReturn(null);

        when(loader.loadFoundPlugins(descriptorFactory)).thenReturn(Arrays.asList(pluginA, pluginB));

        try
        {
            pm.installPlugins(artifactA, artifactB);
            fail("Should have not installed plugins");
        }
        catch (PluginParseException ex)
        {
            // this is good
        }

        verify(loader).canLoad(artifactA);
        verify(loader).canLoad(artifactB);
        verify(installer, never()).installPlugin("a", artifactA);
        verify(installer, never()).installPlugin("b", artifactB);
    }

    @Test
    public void testInstallPluginsWithTwoButOneFailsValidationWithException()
    {
        DynamicPluginLoader loader = mock(DynamicPluginLoader.class);
        when(loader.isDynamicPluginLoader()).thenReturn(true);

        ModuleDescriptorFactory descriptorFactory = mock(ModuleDescriptorFactory.class);
        PluginEventManager eventManager = mock(PluginEventManager.class);
        PluginInstaller installer = mock(PluginInstaller.class);
        DefaultPluginManager pm = new DefaultPluginManager(new MemoryPluginPersistentStateStore(), Collections.<PluginLoader>singletonList(loader), descriptorFactory, eventManager);
        pm.setPluginInstaller(installer);
        PluginArtifact artifactA = mock(PluginArtifact.class);
        Plugin pluginA = mock(Plugin.class);
        when(loader.canLoad(artifactA)).thenReturn("a");
        PluginArtifact artifactB = mock(PluginArtifact.class);
        Plugin pluginB = mock(Plugin.class);
        doThrow(new PluginParseException()).when(loader).canLoad(artifactB);

        when(loader.loadFoundPlugins(descriptorFactory)).thenReturn(Arrays.asList(pluginA, pluginB));

        try
        {
            pm.installPlugins(artifactA, artifactB);
            fail("Should have not installed plugins");
        }
        catch (PluginParseException ex)
        {
            // this is good
        }

        verify(loader).canLoad(artifactA);
        verify(loader).canLoad(artifactB);
        verify(installer, never()).installPlugin("a", artifactA);
        verify(installer, never()).installPlugin("b", artifactB);
    }


    private <T> void checkResources(final PluginAccessor manager, final boolean canGetGlobal, final boolean canGetModule) throws IOException
    {
        InputStream is = manager.getDynamicResourceAsStream("icon.gif");
        assertEquals(canGetGlobal, is != null);
        IOUtils.closeQuietly(is);
        is = manager.getDynamicResourceAsStream("bear/paddington.vm");
        assertEquals(canGetModule, is != null);
        IOUtils.closeQuietly(is);
    }

    private <T> void checkClasses(final PluginAccessor manager, final boolean canGet)
    {
        try
        {
            manager.getDynamicPluginClass("com.atlassian.plugin.mock.MockPaddington");
            if (!canGet)
            {
                fail("Class in plugin was successfully loaded");
            }
        }
        catch (final ClassNotFoundException e)
        {
            if (canGet)
            {
                fail(e.getMessage());
            }
        }
    }

    @Test
    public void testAddPluginsThatThrowExceptionOnEnabled() throws Exception
    {
        final Plugin plugin = new CannotEnablePlugin();

        manager = newDefaultPluginManager();
        manager.addPlugins(null, Arrays.asList(plugin));

        assertFalse(plugin.getPluginState() == PluginState.ENABLED);
    }

    @Test
    public void testUninstallPluginClearsState() throws IOException
    {
        createFillAndCleanTempPluginDirectory();

        final DefaultPluginManager manager = makeClassLoadingPluginManager();

        checkClasses(manager, true);
        final Plugin plugin = manager.getPlugin("test.atlassian.plugin.classloaded");

        final ModuleDescriptor<?> module = plugin.getModuleDescriptor("paddington");
        assertTrue(manager.isPluginModuleEnabled(module.getCompleteKey()));
        manager.disablePluginModule(module.getCompleteKey());
        assertFalse(manager.isPluginModuleEnabled(module.getCompleteKey()));
        manager.uninstall(plugin);
        assertFalse(manager.isPluginModuleEnabled(module.getCompleteKey()));
        assertTrue(pluginStateStore.load().getPluginStateMap(plugin).isEmpty());
    }

    @Test
    public void testCannotInitTwice() throws PluginParseException
    {
        manager = newDefaultPluginManager();
        manager.init();
        try
        {
            manager.init();
            fail("IllegalStateException expected");
        }
        catch (final IllegalStateException expected)
        {
        }
    }

    @Test
    public void testCannotShutdownTwice() throws PluginParseException
    {
        manager = newDefaultPluginManager();
        manager.init();
        manager.shutdown();
        try
        {
            manager.shutdown();
            fail("IllegalStateException expected");
        }
        catch (final IllegalStateException expected)
        {
        }
    }

    @Test
    public void testGetPluginWithNullKey()
    {
        manager = newDefaultPluginManager();
        manager.init();
        try
        {
            getPluginAccessor().getPlugin(null);
            fail();
        }
        catch (IllegalArgumentException ex)
        {
            // test passed
        }
    }

    @Test
    public void testShutdownHandlesException()
    {
        final ThingsAreWrongListener listener = new ThingsAreWrongListener();
        pluginEventManager.register(listener);

        manager = newDefaultPluginManager();
        manager.init();
        try
        {
            //this should not throw an exception
            manager.shutdown();
        }
        catch (Exception e)
        {
            fail("Should not have thrown an exception!");
        }
        assertTrue(listener.isCalled());
    }

    private void writeToFile(String file, String line) throws IOException, URISyntaxException
    {
        final String resourceName = ClasspathFilePluginMetadata.class.getPackage().getName().replace(".", "/") + "/" + file;
        URL resource = getClass().getClassLoader().getResource(resourceName);

        FileOutputStream fout = new FileOutputStream(new File(resource.toURI()), false);
        fout.write(line.getBytes(), 0, line.length());
        fout.close();

    }

    @Test
    public void testExceptionOnRequiredPluginNotEnabling() throws Exception
    {
        try
        {
            writeToFile("application-required-modules.txt", "foo.required:bar");
            writeToFile("application-required-plugins.txt", "foo.required");

            final PluginLoader mockPluginLoader = mock(PluginLoader.class);
            final ModuleDescriptor<Object> badModuleDescriptor = new AbstractModuleDescriptor<Object>(ModuleFactory.LEGACY_MODULE_FACTORY)
            {
                @Override
                public String getKey()
                {
                    return "bar";
                }

                @Override
                public String getCompleteKey()
                {
                    return "foo.required:bar";
                }

                @Override
                public void enabled()
                {
                    throw new IllegalArgumentException("Cannot enable");
                }

                @Override
                public Object getModule()
                {
                    return null;
                }
            };

            final AbstractModuleDescriptor goodModuleDescriptor = mock(AbstractModuleDescriptor.class);
            when(goodModuleDescriptor.getKey()).thenReturn("baz");
            when(goodModuleDescriptor.getCompleteKey()).thenReturn("foo.required:baz");

            Plugin plugin = new StaticPlugin()
            {
                @Override
                public Collection<ModuleDescriptor<?>> getModuleDescriptors()
                {
                    return Arrays.<ModuleDescriptor<?>>asList(goodModuleDescriptor, badModuleDescriptor);
                }

                @Override
                public ModuleDescriptor<Object> getModuleDescriptor(final String key)
                {
                    return badModuleDescriptor;
                }
            };
            plugin.setKey("foo.required");
            plugin.setEnabledByDefault(true);
            plugin.setPluginInformation(new PluginInformation());

            when(mockPluginLoader.loadAllPlugins(any(ModuleDescriptorFactory.class))).thenReturn(Collections.singletonList(plugin));

            manager = newDefaultPluginManager(mockPluginLoader);
            try
            {
                manager.init();
            }
            catch(PluginException e)
            {
                // expected
                assertEquals("Unable to validate required plugins or modules", e.getMessage());
                return;
            }
            fail("A PluginException is expected when trying to initialize the plugins system with required plugins that do not load.");
        }
        finally
        {
            // remove references from required files
            writeToFile("application-required-modules.txt", "");
            writeToFile("application-required-plugins.txt", "");
        }
    }

    public static class ThingsAreWrongListener
    {
        private volatile boolean called = false;

        @PluginEventListener
        public void onFrameworkShutdown(final PluginFrameworkShutdownEvent event)
        {
            called = true;
            throw new NullPointerException("AAAH!");
        }

        public boolean isCalled()
        {
            return called;
        }
    }

    public Plugin createPluginWithVersion(final String version)
    {
        final Plugin p = new StaticPlugin();
        p.setKey("test.default.plugin");
        final PluginInformation pInfo = p.getPluginInformation();
        pInfo.setVersion(version);
        return p;
    }

    /**
     * Dummy plugin loader that reports that removal is supported and returns plugins that report that they can
     * be uninstalled.
     */
    private static class SinglePluginLoaderWithRemoval extends SinglePluginLoader
    {
        public SinglePluginLoaderWithRemoval(final String resource)
        {
            super(resource);
        }

        public boolean supportsRemoval()
        {

            return true;
        }

        public void removePlugin(final Plugin plugin) throws PluginException
        {
            plugins = Collections.emptyList();
        }

        protected StaticPlugin getNewPlugin()
        {
            return new StaticPlugin()
            {
                public boolean isUninstallable()
                {
                    return true;
                }
            };
        }
    }

    class NothingModuleDescriptor extends MockUnusedModuleDescriptor
    {
    }

    @RequiresRestart
    public static class RequiresRestartModuleDescriptor extends MockUnusedModuleDescriptor
    {
    }

    // A subclass of a module descriptor that @RequiresRestart; should inherit the annotation
    public static class RequiresRestartSubclassModuleDescriptor extends RequiresRestartModuleDescriptor
    {
    }

    private class MultiplePluginLoader implements PluginLoader
    {
        private final String[] descriptorPaths;

        public MultiplePluginLoader(final String... descriptorPaths)
        {
            this.descriptorPaths = descriptorPaths;
        }

        public Iterable<Plugin> loadAllPlugins(final ModuleDescriptorFactory moduleDescriptorFactory) throws PluginParseException
        {
            final ImmutableList.Builder<Plugin> result = ImmutableList.builder();
            for (final String path : descriptorPaths)
            {
                final SinglePluginLoader loader = new SinglePluginLoader(path);
                result.addAll(loader.loadAllPlugins(moduleDescriptorFactory));
            }
            return result.build();
        }

        public boolean supportsAddition()
        {
            return false;
        }

        public boolean supportsRemoval()
        {
            return false;
        }

        public Iterable<Plugin> loadFoundPlugins(final ModuleDescriptorFactory moduleDescriptorFactory) throws PluginParseException
        {
            throw new UnsupportedOperationException("This PluginLoader does not support addition.");
        }

        public void removePlugin(final Plugin plugin) throws PluginException
        {
            throw new UnsupportedOperationException("This PluginLoader does not support addition.");
        }

        @Override
        public boolean isDynamicPluginLoader()
        {
            return false;
        }
    }

    private static class DynamicSinglePluginLoader extends SinglePluginLoader implements PluginLoader, DynamicPluginLoader
    {
        private final AtomicBoolean canLoad = new AtomicBoolean(false);

        private final String key;

        public DynamicSinglePluginLoader(final String key, final String resource)
        {
            super(resource);
            this.key = key;
        }

        @Override
        public boolean isDynamicPluginLoader()
        {
            return true;
        }

        public String canLoad(final PluginArtifact pluginArtifact) throws PluginParseException
        {
            return canLoad.get() ? key : null;
        }

        public boolean supportsAddition()
        {
            return true;
        }

        @Override
        public Iterable<Plugin> loadAllPlugins(ModuleDescriptorFactory moduleDescriptorFactory)
        {
            if (canLoad.get())
            {
                return super.loadAllPlugins(moduleDescriptorFactory);
            }
            else
            {
                return ImmutableList.of();
            }
        }

        @Override
        public Iterable<Plugin> loadFoundPlugins(final ModuleDescriptorFactory moduleDescriptorFactory)
        {
            if (canLoad.get())
            {
                return super.loadAllPlugins(moduleDescriptorFactory);
            }
            else
            {
                return ImmutableList.of();
            }
        }
    }

    private static class CannotEnablePlugin extends StaticPlugin
    {
        public CannotEnablePlugin()
        {
            setKey("foo");
        }

        @Override
        protected PluginState enableInternal()
        {
            throw new RuntimeException("boo");
        }

        public void disabled()
        {
        }
    }

    public static class PluginModuleEnabledListener
    {
        public volatile boolean called;
        @PluginEventListener
        public void onEnable(PluginModuleEnabledEvent event)
        {
            called = true;
        }
    }

    public static class PluginModuleDisabledListener
    {
        public volatile boolean called;
        @PluginEventListener
        public void onDisable(PluginModuleDisabledEvent event)
        {
            called = true;
        }
    }

    public static class PluginDisabledListener
    {
        public volatile boolean called;
        @PluginEventListener
        public void onDisable(PluginDisabledEvent event)
        {
            called = true;
        }
    }
}
