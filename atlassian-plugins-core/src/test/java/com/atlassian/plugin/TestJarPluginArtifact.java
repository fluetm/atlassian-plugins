package com.atlassian.plugin;

import com.atlassian.plugin.test.PluginJarBuilder;
import com.google.common.collect.ImmutableMap;
import org.junit.Test;

import java.io.File;
import java.io.IOException;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

public final class TestJarPluginArtifact
{
    @Test
    public void testGetResourceAsStream() throws IOException
    {
        final JarPluginArtifact artifact = new JarPluginArtifact(new PluginJarBuilder()
                .addResource("foo", "bar")
                .build());

        assertNotNull(artifact.getResourceAsStream("foo"));
        assertNull(artifact.getResourceAsStream("bar"));
    }

    @Test
    public void testContainsJavaExecutableCodeWithNoJava() throws Exception
    {
        final JarPluginArtifact artifact = new JarPluginArtifact(new PluginJarBuilder()
                .addResource("foo", "bar")
                .build());

        assertFalse(artifact.containsJavaExecutableCode());
    }

    @Test
    public void testContainsJavaExecutableCodeWithNoJavaAndNoManifest() throws Exception
    {
        final JarPluginArtifact artifact = new JarPluginArtifact(new PluginJarBuilder()
                .addResource("foo", "bar")
                .buildWithNoManifest());

        assertFalse(artifact.containsJavaExecutableCode());
    }

    @Test
    public void testContainsJavaExecutableCodeAsJavaClass() throws Exception
    {
        final JarPluginArtifact artifact = new JarPluginArtifact(new PluginJarBuilder()
                .addFormattedJava("foo.Bar",
                        "package foo;",
                        "public final class Bar {}")
                .build());

        assertTrue(artifact.containsJavaExecutableCode());
    }

    @Test
    public void testContainsJavaExecutableCodeAsJavaLibrary() throws Exception
    {
        File innerJar = new PluginJarBuilder().build();

        final JarPluginArtifact artifact = new JarPluginArtifact(new PluginJarBuilder()
                .addFile("innerJar.jar", innerJar)
                .build());

        assertTrue(artifact.containsJavaExecutableCode());
    }

    @Test
    public void testContainsJavaExecutableCodeAsSpringContextXmlFiles() throws Exception
    {
        final JarPluginArtifact artifact = new JarPluginArtifact(new PluginJarBuilder()
                .addFormattedResource("META-INF/spring/file.xml", "bla")
                .build());

        assertTrue(artifact.containsJavaExecutableCode());
    }

    @Test
    public void testContainsJavaExecutableCodeAsSpringContextManifestEntry() throws Exception
    {
        final JarPluginArtifact artifact = new JarPluginArtifact(new PluginJarBuilder()
                .manifest(ImmutableMap.of("Spring-Context", "bla"))
                .build());

        assertTrue(artifact.containsJavaExecutableCode());
    }

    @Test
    public void testContainsJavaExecutableCodeAsBundleActivator() throws Exception
    {
        final JarPluginArtifact artifact = new JarPluginArtifact(new PluginJarBuilder()
                .manifest(ImmutableMap.of("Bundle-Activator", "bla"))
                .build());

        assertTrue(artifact.containsJavaExecutableCode());
    }
}
