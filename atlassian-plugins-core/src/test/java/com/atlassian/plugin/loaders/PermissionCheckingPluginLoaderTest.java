package com.atlassian.plugin.loaders;

import com.atlassian.plugin.*;
import com.atlassian.plugin.impl.UnloadablePlugin;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Iterables;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public final class PermissionCheckingPluginLoaderTest
{
    private PermissionCheckingPluginLoader permissionCheckingPluginLoader;

    @Mock
    private PluginLoader pluginLoader;

    @Mock
    private ModuleDescriptorFactory moduleDescriptorFactory;

    @Before
    public void setUp()
    {
        permissionCheckingPluginLoader = new PermissionCheckingPluginLoader(pluginLoader);
    }

    @Test
    public void loadAllPluginsWithEmptyCollectionOfPlugins()
    {
        when(pluginLoader.loadAllPlugins(moduleDescriptorFactory)).thenReturn(ImmutableList.<Plugin>of());
        final Iterable<Plugin> plugins = permissionCheckingPluginLoader.loadAllPlugins(moduleDescriptorFactory);
        assertTrue(Iterables.isEmpty(plugins));
    }

    @Test
    public void loadFoundPluginsWithEmptyCollectionOfPlugins()
    {
        when(pluginLoader.loadFoundPlugins(moduleDescriptorFactory)).thenReturn(ImmutableList.<Plugin>of());
        final Iterable<Plugin> plugins = permissionCheckingPluginLoader.loadFoundPlugins(moduleDescriptorFactory);
        assertTrue(Iterables.isEmpty(plugins));
    }

    @Test
    public void loadFoundPluginsWithPluginWithoutExecuteJavaPermissionAndNoJava()
    {
        final PluginArtifactBackedPlugin plugin = newPlugin(ImmutableSet.<String>of(), false, false);

        when(pluginLoader.loadFoundPlugins(moduleDescriptorFactory)).thenReturn(ImmutableList.<Plugin>of(plugin));
        final Iterable<Plugin> plugins = permissionCheckingPluginLoader.loadFoundPlugins(moduleDescriptorFactory);

        assertEquals(1, Iterables.size(plugins));
        assertSame(plugin, Iterables.get(plugins, 0));
    }

    @Test
    public void loadAllPluginsWithPluginWithoutExecuteJavaPermissionAndNoJava()
    {
        final PluginArtifactBackedPlugin plugin = newPlugin(ImmutableSet.<String>of(), false, false);

        when(pluginLoader.loadAllPlugins(moduleDescriptorFactory)).thenReturn(ImmutableList.<Plugin>of(plugin));
        final Iterable<Plugin> plugins = permissionCheckingPluginLoader.loadAllPlugins(moduleDescriptorFactory);

        assertEquals(1, Iterables.size(plugins));
        assertSame(plugin, Iterables.get(plugins, 0));
    }

    @Test
    public void loadFoundPluginsWithPluginWithoutExecuteJavaPermissionAndSomeJava()
    {
        final PluginArtifactBackedPlugin plugin = newPlugin(ImmutableSet.<String>of(), true, false);

        when(pluginLoader.loadFoundPlugins(moduleDescriptorFactory)).thenReturn(ImmutableList.<Plugin>of(plugin));
        final Iterable<Plugin> plugins = permissionCheckingPluginLoader.loadFoundPlugins(moduleDescriptorFactory);

        assertEquals(1, Iterables.size(plugins));
        assertTrue(Iterables.get(plugins, 0) instanceof UnloadablePlugin);
    }

    @Test
    public void loadAllPluginsWithPluginWithoutExecuteJavaPermissionAndSomeJava()
    {
        final PluginArtifactBackedPlugin plugin = newPlugin(ImmutableSet.<String>of(), true, false);

        when(pluginLoader.loadAllPlugins(moduleDescriptorFactory)).thenReturn(ImmutableList.<Plugin>of(plugin));
        final Iterable<Plugin> plugins = permissionCheckingPluginLoader.loadAllPlugins(moduleDescriptorFactory);

        assertEquals(1, Iterables.size(plugins));
        assertTrue(Iterables.get(plugins, 0) instanceof UnloadablePlugin);
    }

    @Test
    public void loadAllPluginsWithUnloadablePluginThenRemoved()
    {
        final PluginArtifactBackedPlugin plugin = newPlugin(ImmutableSet.<String>of(), true, false);

        doThrow(new PluginException("cannot find plugin")).when(pluginLoader).removePlugin(any(Plugin.class));
        when(pluginLoader.loadAllPlugins(moduleDescriptorFactory)).thenReturn(ImmutableList.<Plugin>of(plugin));
        final Iterable<Plugin> plugins = permissionCheckingPluginLoader.loadAllPlugins(moduleDescriptorFactory);

        assertEquals(1, Iterables.size(plugins));
        Plugin loadedPlugin = Iterables.get(plugins, 0);
        assertTrue(loadedPlugin instanceof UnloadablePlugin);
        permissionCheckingPluginLoader.removePlugin(loadedPlugin);
    }

    @Test
    public void loadAllPluginsWithPluginWithNoPermissionsAndSystemModules() throws Exception
    {
        final PluginArtifactBackedPlugin plugin = newPlugin(ImmutableSet.<String>of(), false, false);

        addModuleDescriptor(plugin, true);

        when(pluginLoader.loadAllPlugins(moduleDescriptorFactory)).thenReturn(ImmutableList.<Plugin>of(plugin));
        final Iterable<Plugin> plugins = permissionCheckingPluginLoader.loadAllPlugins(moduleDescriptorFactory);

        assertEquals(1, Iterables.size(plugins));
        assertTrue(Iterables.get(plugins, 0) instanceof UnloadablePlugin);
    }

    @Test
    public void loadAllPluginsWithPluginWithNoPermissionsAndNoSystemModules() throws Exception
    {
        final PluginArtifactBackedPlugin plugin = newPlugin(ImmutableSet.<String>of(), false, false);

        addModuleDescriptor(plugin, false);

        when(pluginLoader.loadAllPlugins(moduleDescriptorFactory)).thenReturn(ImmutableList.<Plugin>of(plugin));
        final Iterable<Plugin> plugins = permissionCheckingPluginLoader.loadAllPlugins(moduleDescriptorFactory);

        assertEquals(1, Iterables.size(plugins));
        assertSame(plugin, Iterables.get(plugins, 0));
    }

    @Test
    public void loadFoundPluginsWithPluginWithJavaPermissionAndSomeJava() throws Exception
    {
        final PluginArtifactBackedPlugin plugin = newPlugin(ImmutableSet.<String>of(Permissions.EXECUTE_JAVA), true, false);

        when(pluginLoader.loadFoundPlugins(moduleDescriptorFactory)).thenReturn(ImmutableList.<Plugin>of(plugin));
        final Iterable<Plugin> plugins = permissionCheckingPluginLoader.loadFoundPlugins(moduleDescriptorFactory);

        assertEquals(1, Iterables.size(plugins));
        assertSame(plugin, Iterables.get(plugins, 0));
    }

    @Test
    public void loadAllPluginsWithPluginWithCreateSystemModulePermissionsAndSystemModules() throws Exception
    {
        final PluginArtifactBackedPlugin plugin = newPlugin(ImmutableSet.<String>of(Permissions.CREATE_SYSTEM_MODULES), false, false);

        addModuleDescriptor(plugin, true);

        when(pluginLoader.loadAllPlugins(moduleDescriptorFactory)).thenReturn(ImmutableList.<Plugin>of(plugin));
        final Iterable<Plugin> plugins = permissionCheckingPluginLoader.loadAllPlugins(moduleDescriptorFactory);

        assertEquals(1, Iterables.size(plugins));
        assertSame(plugin, Iterables.get(plugins, 0));
    }

    @Test
    public void loadAllPluginsWithPluginWithCreateSystemModulePermissionsAndNoSystemModules() throws Exception
    {
        final PluginArtifactBackedPlugin plugin = newPlugin(ImmutableSet.<String>of(Permissions.CREATE_SYSTEM_MODULES), false, false);

        addModuleDescriptor(plugin, false);

        when(pluginLoader.loadAllPlugins(moduleDescriptorFactory)).thenReturn(ImmutableList.<Plugin>of(plugin));
        final Iterable<Plugin> plugins = permissionCheckingPluginLoader.loadAllPlugins(moduleDescriptorFactory);

        assertEquals(1, Iterables.size(plugins));
        assertSame(plugin, Iterables.get(plugins, 0));
    }

    @Test
    public void loadFoundPluginsWithPluginWithJavaPermissionAndNoJava() throws Exception
    {
        final PluginArtifactBackedPlugin plugin = newPlugin(ImmutableSet.<String>of(Permissions.EXECUTE_JAVA), false, false);

        when(pluginLoader.loadFoundPlugins(moduleDescriptorFactory)).thenReturn(ImmutableList.<Plugin>of(plugin));
        final Iterable<Plugin> plugins = permissionCheckingPluginLoader.loadFoundPlugins(moduleDescriptorFactory);

        assertEquals(1, Iterables.size(plugins));
        assertSame(plugin, Iterables.get(plugins, 0));
    }

    @Test
    public void loadAllPluginsWithPluginWithJavaPermissionAndNoJava() throws Exception
    {
        final PluginArtifactBackedPlugin plugin = newPlugin(ImmutableSet.<String>of(Permissions.EXECUTE_JAVA), false, false);

        when(pluginLoader.loadAllPlugins(moduleDescriptorFactory)).thenReturn(ImmutableList.<Plugin>of(plugin));
        final Iterable<Plugin> plugins = permissionCheckingPluginLoader.loadAllPlugins(moduleDescriptorFactory);

        assertEquals(1, Iterables.size(plugins));
        assertSame(plugin, Iterables.get(plugins, 0));
    }

    @Test
    public void loadFoundPluginsWithPluginWithAllPermissionsAndSomeJava() throws Exception
    {
        final PluginArtifactBackedPlugin plugin = newPlugin(ImmutableSet.<String>of(), true, true);

        when(pluginLoader.loadFoundPlugins(moduleDescriptorFactory)).thenReturn(ImmutableList.<Plugin>of(plugin));
        final Iterable<Plugin> plugins = permissionCheckingPluginLoader.loadFoundPlugins(moduleDescriptorFactory);

        assertEquals(1, Iterables.size(plugins));
        assertSame(plugin, Iterables.get(plugins, 0));
    }

    @Test
    public void loadAllPluginsWithPluginWithAllPermissionsAndSomeJava() throws Exception
    {
        final PluginArtifactBackedPlugin plugin = newPlugin(ImmutableSet.<String>of(), true, true);

        when(pluginLoader.loadAllPlugins(moduleDescriptorFactory)).thenReturn(ImmutableList.<Plugin>of(plugin));
        final Iterable<Plugin> plugins = permissionCheckingPluginLoader.loadAllPlugins(moduleDescriptorFactory);

        assertEquals(1, Iterables.size(plugins));
        assertSame(plugin, Iterables.get(plugins, 0));
    }

    @Test
    public void loadAllPluginsWithPluginWithAllPermissionsAndSystemModules() throws Exception
    {
        final PluginArtifactBackedPlugin plugin = newPlugin(ImmutableSet.<String>of(), false, true);
        addModuleDescriptor(plugin, true);

        when(pluginLoader.loadAllPlugins(moduleDescriptorFactory)).thenReturn(ImmutableList.<Plugin>of(plugin));
        final Iterable<Plugin> plugins = permissionCheckingPluginLoader.loadAllPlugins(moduleDescriptorFactory);

        assertEquals(1, Iterables.size(plugins));
        assertSame(plugin, Iterables.get(plugins, 0));
    }

    private void addModuleDescriptor(PluginArtifactBackedPlugin plugin, boolean system)
    {
        ModuleDescriptor descriptor = mock(ModuleDescriptor.class);
        when(descriptor.getKey()).thenReturn("foo");
        when(descriptor.isSystemModule()).thenReturn(system);
        when(plugin.getModuleDescriptors()).thenReturn(ImmutableList.<ModuleDescriptor<?>>of(descriptor));
    }

    private PluginArtifactBackedPlugin newPlugin(Set<String> permissions, boolean hasJava, boolean hasAllPermissions)
    {
        final PluginArtifactBackedPlugin plugin = mockPlugin(PluginArtifactBackedPlugin.class);
        final PluginArtifact pluginArtifact = mock(PluginArtifact.class);
        when(plugin.hasAllPermissions()).thenReturn(hasAllPermissions);
        when(plugin.getActivePermissions()).thenReturn(permissions);
        when(plugin.getPluginArtifact()).thenReturn(pluginArtifact);
        when(pluginArtifact.containsJavaExecutableCode()).thenReturn(hasJava);
        return plugin;
    }

    private <P extends Plugin> P mockPlugin(Class<P> type)
    {
        P mock = mock(type);
        when(mock.getKey()).thenReturn("test-plugin-key");
        when(mock.getName()).thenReturn("Test Plugin");
        return mock;
    }
}