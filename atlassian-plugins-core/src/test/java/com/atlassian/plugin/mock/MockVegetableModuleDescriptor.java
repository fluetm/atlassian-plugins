package com.atlassian.plugin.mock;

import com.atlassian.plugin.descriptors.AbstractModuleDescriptor;
import com.atlassian.plugin.descriptors.CannotDisable;
import com.atlassian.plugin.module.ModuleFactory;

@CannotDisable
public class MockVegetableModuleDescriptor extends AbstractModuleDescriptor<MockThing>
{
    public MockVegetableModuleDescriptor()
    {
        super(ModuleFactory.LEGACY_MODULE_FACTORY);
    }

    @Override
    public MockThing getModule()
    {
        return new MockVegetable();
    }
}
