/*
 * Created by IntelliJ IDEA.
 * User: Mike
 * Date: Jul 29, 2004
 * Time: 3:19:33 PM
 */
package com.atlassian.plugin;

import com.google.common.annotations.VisibleForTesting;
import org.apache.commons.lang.StringUtils;

import static org.apache.commons.lang.StringUtils.isEmpty;

public class ModuleCompleteKey
{
    @VisibleForTesting
    protected static final String SEPARATOR = ":";

    private final String pluginKey;
    private final String moduleKey;

    public ModuleCompleteKey(String completeKey)
    {
        this(StringUtils.substringBefore(completeKey, SEPARATOR), StringUtils.substringAfter(completeKey, SEPARATOR));
    }

    public ModuleCompleteKey(String pluginKey, String moduleKey)
    {
        this.pluginKey = StringUtils.trimToEmpty(pluginKey);

        if (!isValidKey(this.pluginKey))
        {
            throw new IllegalArgumentException("Invalid plugin key specified: " + this.pluginKey);
        }

        this.moduleKey = StringUtils.trimToEmpty(moduleKey);

        if (isEmpty(this.moduleKey)) // just validate that we have a non-empty module key
        {
            throw new IllegalArgumentException("Invalid module key specified: " + this.moduleKey);
        }
    }

    private boolean isValidKey(String key)
    {
        return StringUtils.isNotBlank(key) && !key.contains(SEPARATOR);
    }

    public String getModuleKey()
    {
        return moduleKey;
    }

    public String getPluginKey()
    {
        return pluginKey;
    }

    public String getCompleteKey()
    {
        return pluginKey + SEPARATOR + moduleKey;
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o)
        {
            return true;
        }
        if (o == null || getClass() != o.getClass())
        {
            return false;
        }

        ModuleCompleteKey that = (ModuleCompleteKey) o;

        if (!moduleKey.equals(that.moduleKey))
        {
            return false;
        }
        if (!pluginKey.equals(that.pluginKey))
        {
            return false;
        }

        return true;
    }

    @Override
    public int hashCode()
    {
        int result = pluginKey.hashCode();
        result = 31 * result + moduleKey.hashCode();
        return result;
    }

    @Override
    public String toString()
    {
        return getCompleteKey();
    }
}