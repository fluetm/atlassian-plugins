package com.atlassian.plugin.event.events;

import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.PluginController;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Signals a warm restart of the plugin framework is about to begin
 *
 * @since 2.3.0
 */
public class PluginFrameworkWarmRestartingEvent
{
    private final PluginController pluginController;
    private final PluginAccessor pluginAccessor;

    public PluginFrameworkWarmRestartingEvent(PluginController pluginController, PluginAccessor pluginAccessor)
    {
        this.pluginController = checkNotNull(pluginController);
        this.pluginAccessor = checkNotNull(pluginAccessor);
    }

    public PluginController getPluginController()
    {
        return pluginController;
    }

    public PluginAccessor getPluginAccessor()
    {
        return pluginAccessor;
    }
}
