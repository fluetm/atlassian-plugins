package com.atlassian.plugin.event.events;

import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.PluginController;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Event that signifies the plugin framework has been started and initialized
 */
public class PluginFrameworkStartedEvent
{
    private final PluginController pluginController;
    private final PluginAccessor pluginAccessor;

    public PluginFrameworkStartedEvent(PluginController pluginController, PluginAccessor pluginAccessor)
    {
        this.pluginController = checkNotNull(pluginController);
        this.pluginAccessor = checkNotNull(pluginAccessor);
    }

    public PluginController getPluginController()
    {
        return pluginController;
    }

    public PluginAccessor getPluginAccessor()
    {
        return pluginAccessor;
    }
}
