package com.atlassian.plugin.module;

import com.atlassian.plugin.ModuleDescriptor;
import com.atlassian.plugin.PluginParseException;
import com.atlassian.plugin.hostcontainer.HostContainer;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Legacy module factory that uses the {@link ContainerManagedPlugin} to create beans
 *
 * @since 2.5.0
 */
public class HostContainerLegacyAdaptor extends LegacyModuleFactory
{

    private final HostContainer hostContainer;

    public HostContainerLegacyAdaptor(HostContainer hostContainer)
    {
        this.hostContainer = checkNotNull(hostContainer);
    }

    public <T> T createModule(String name, ModuleDescriptor<T> moduleDescriptor) throws PluginParseException {

        // Give the plugin a go first
        if (moduleDescriptor.getPlugin() instanceof ContainerManagedPlugin)
        {
            return ((ContainerManagedPlugin) moduleDescriptor.getPlugin()).getContainerAccessor().createBean(moduleDescriptor.getModuleClass());
        }
        else
        {
            return hostContainer.create(moduleDescriptor.getModuleClass());
        }
    }

}
