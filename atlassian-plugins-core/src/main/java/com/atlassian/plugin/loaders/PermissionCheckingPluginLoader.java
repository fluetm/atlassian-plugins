package com.atlassian.plugin.loaders;

import com.atlassian.plugin.*;
import com.atlassian.plugin.impl.UnloadablePlugin;
import com.google.common.base.Function;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static com.google.common.collect.ImmutableList.copyOf;
import static com.google.common.collect.Iterables.transform;

public final class PermissionCheckingPluginLoader extends ForwardingPluginLoader
{
    private static final Logger logger = LoggerFactory.getLogger(PermissionCheckingPluginLoader.class);

    public PermissionCheckingPluginLoader(PluginLoader delegate)
    {
        super(delegate);
    }

    @Override
    public Iterable<Plugin> loadAllPlugins(ModuleDescriptorFactory moduleDescriptorFactory) throws PluginParseException
    {
        return copyOf(transform(delegate().loadAllPlugins(moduleDescriptorFactory), new CheckPluginPermissionFunction()));
    }

    @Override
    public Iterable<Plugin> loadFoundPlugins(ModuleDescriptorFactory moduleDescriptorFactory) throws PluginParseException
    {
        return copyOf(transform(delegate().loadFoundPlugins(moduleDescriptorFactory), new CheckPluginPermissionFunction()));
    }

    @Override
    public void removePlugin(Plugin plugin) throws PluginException
    {
        if (!(plugin instanceof UnloadablePlugin))
        {
            super.removePlugin(plugin);
        }
        else
        {
            logger.debug("Detected an unloadable plugin '{}', so skipping removal", plugin.getKey());
        }
    }

    private static final class CheckPluginPermissionFunction implements Function<Plugin, Plugin>
    {
        @Override
        public Plugin apply(Plugin p)
        {
            // we don't need to check if all permissions are allowed
            if (p.hasAllPermissions())
            {
                return p;
            }

            if (p instanceof PluginArtifactBackedPlugin)
            {
                return checkPlugin((PluginArtifactBackedPlugin) p);
            }
            return p;
        }

        private Plugin checkPlugin(PluginArtifactBackedPlugin p)
        {
            final PluginArtifact pluginArtifact = p.getPluginArtifact();

            if (pluginArtifact.containsJavaExecutableCode() && !p.getActivePermissions().contains(Permissions.EXECUTE_JAVA))
            {
                UnloadablePlugin unloadablePlugin = new UnloadablePlugin(
                        "Plugin doesn't require '" + Permissions.EXECUTE_JAVA + "' permission yet references some java executable code. " +
                                "This could be either embedded java classes, embedded java libraries, spring context files or bundle activator.");
                unloadablePlugin.setKey(p.getKey());
                unloadablePlugin.setName(p.getName());

                logger.warn("Plugin '{}' only requires permission {} which doesn't include '{}', yet has some java code " +
                        "(classes, libs, spring context, etc), making it un-loadable.",
                        new Object[]{p.getKey(), p.getActivePermissions(), Permissions.EXECUTE_JAVA});

                return unloadablePlugin;
            }
            else if (hasSystemModules(p) && !p.getActivePermissions().contains(Permissions.CREATE_SYSTEM_MODULES))
            {
                UnloadablePlugin unloadablePlugin = new UnloadablePlugin(
                        "Plugin doesn't require '" + Permissions.CREATE_SYSTEM_MODULES + "' permission yet declared " +
                                "modules use the 'system' attribute. ");
                unloadablePlugin.setKey(p.getKey());
                unloadablePlugin.setName(p.getName());

                logger.warn("Plugin '{}' only requires permission {} which doesn't include '{}', yet has system modules " +
                        ", making it un-loadable.",
                        new Object[]{p.getKey(), p.getActivePermissions(), Permissions.CREATE_SYSTEM_MODULES});

                return unloadablePlugin;
            }
            else
            {
                return p;
            }
        }

        private boolean hasSystemModules(PluginArtifactBackedPlugin plugin)
        {
            for (ModuleDescriptor descriptor : plugin.getModuleDescriptors())
            {
                if (descriptor.isSystemModule())
                {
                    return true;
                }
            }
            return false;
        }
    }
}
