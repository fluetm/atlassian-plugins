package com.atlassian.plugin.loaders;

import com.atlassian.plugin.ModuleDescriptorFactory;
import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginException;
import com.atlassian.plugin.PluginParseException;

/**
 * Handles loading and unloading plugin artifacts from a location
 */
public interface PluginLoader
{
    /**
     * Loads all plugins that can be installed in the plugin system.
     * @param moduleDescriptorFactory the factory for module descriptors
     * @return the list of found plugins, may be empty
     * @throws PluginParseException if any error occurred loading plugins
     */
    Iterable<Plugin> loadAllPlugins(ModuleDescriptorFactory moduleDescriptorFactory) throws PluginParseException;

    /**
     * Load all newly found plugins that can be installed in the plugin system. Only plugins not previously loaded will
     * be added.
     * @param moduleDescriptorFactory the factory for module descriptors
     * @return a list of newly discovered plugins since the last time plugins were loaded
     * @throws PluginParseException if any error occurred loading plugins
     */
    Iterable<Plugin> loadFoundPlugins(ModuleDescriptorFactory moduleDescriptorFactory) throws PluginParseException;

    /**
     * @return true if this PluginLoader tracks whether or not plugins are added to it.
     */
    boolean supportsAddition();

    /**
     * @return true if this PluginLoader tracks whether or not plugins are removed from it.
     */
    boolean supportsRemoval();

    /**
     * Remove a specific plugin
     */
    void removePlugin(Plugin plugin) throws PluginException;

    /**
     * @return {@code true} if this plugin loader can load plugins dynamically
     * @since 3.0
     */
    boolean isDynamicPluginLoader();
}
