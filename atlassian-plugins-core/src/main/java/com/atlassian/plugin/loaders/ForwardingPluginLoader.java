package com.atlassian.plugin.loaders;

import com.atlassian.plugin.ModuleDescriptorFactory;
import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginArtifact;
import com.atlassian.plugin.PluginException;
import com.atlassian.plugin.PluginParseException;
import com.google.common.collect.ForwardingObject;

import static com.google.common.base.Preconditions.checkNotNull;

abstract class ForwardingPluginLoader extends ForwardingObject implements DynamicPluginLoader
{
    private final PluginLoader delegate;

    protected ForwardingPluginLoader(PluginLoader delegate)
    {
        this.delegate = checkNotNull(delegate);
    }

    @Override
    protected final PluginLoader delegate()
    {
        return delegate;
    }

    @Override
    public Iterable<Plugin> loadAllPlugins(ModuleDescriptorFactory moduleDescriptorFactory) throws PluginParseException
    {
        return delegate().loadAllPlugins(moduleDescriptorFactory);
    }

    @Override
    public Iterable<Plugin> loadFoundPlugins(ModuleDescriptorFactory moduleDescriptorFactory) throws PluginParseException
    {
        return delegate().loadFoundPlugins(moduleDescriptorFactory);
    }

    @Override
    public boolean supportsAddition()
    {
        return delegate().supportsAddition();
    }

    @Override
    public boolean supportsRemoval()
    {
        return delegate().supportsRemoval();
    }

    @Override
    public void removePlugin(Plugin plugin) throws PluginException
    {
        delegate().removePlugin(plugin);
    }

    @Override
    public boolean isDynamicPluginLoader()
    {
        return delegate().isDynamicPluginLoader();
    }

    @Override
    public String canLoad(PluginArtifact pluginArtifact) throws PluginParseException
    {
        if (isDynamicPluginLoader())
        {
            return ((DynamicPluginLoader)delegate()).canLoad(pluginArtifact);
        }
        else
        {
            throw new IllegalStateException("Should not call on non-dynamic plugin loader");
        }
    }
}
