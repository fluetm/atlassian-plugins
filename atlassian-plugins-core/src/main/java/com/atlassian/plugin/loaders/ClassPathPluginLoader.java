package com.atlassian.plugin.loaders;

import com.atlassian.plugin.ModuleDescriptorFactory;
import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.PluginException;
import com.atlassian.plugin.PluginParseException;
import com.atlassian.plugin.util.ClassLoaderUtils;
import com.google.common.base.Function;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nullable;
import java.io.IOException;
import java.net.URL;
import java.util.Enumeration;

import static com.google.common.collect.ImmutableList.copyOf;
import static com.google.common.collect.Iterables.concat;
import static com.google.common.collect.Iterators.forEnumeration;
import static com.google.common.collect.Iterators.transform;

/**
 * Loads plugins from the classpath
 */
public class ClassPathPluginLoader implements PluginLoader
{
    private static Logger log = LoggerFactory.getLogger(ClassPathPluginLoader.class);

    private final String fileNameToLoad;
    private Iterable<Plugin> plugins;

    public ClassPathPluginLoader()
    {
        this(PluginAccessor.Descriptor.FILENAME);
    }

    public ClassPathPluginLoader(final String fileNameToLoad)
    {
        this.fileNameToLoad = fileNameToLoad;
    }

    private void loadClassPathPlugins(final ModuleDescriptorFactory moduleDescriptorFactory) throws PluginParseException
    {
        final Enumeration<URL> pluginDescriptorFiles;
        try
        {
            pluginDescriptorFiles = ClassLoaderUtils.getResources(fileNameToLoad, this.getClass());
        }
        catch (final IOException e)
        {
            log.error("Could not load classpath plugins: " + e, e);
            return;
        }

        plugins = concat(copyOf(transform(forEnumeration(pluginDescriptorFiles), new Function<URL, Iterable<Plugin>>()
        {
            @Override
            public Iterable<Plugin> apply(@Nullable URL url)
            {
                return new SinglePluginLoader(url).loadAllPlugins(moduleDescriptorFactory);
            }
        })));
    }

    public Iterable<Plugin> loadAllPlugins(final ModuleDescriptorFactory moduleDescriptorFactory) throws PluginParseException
    {
        if (plugins == null)
        {
            loadClassPathPlugins(moduleDescriptorFactory);
        }
        return copyOf(plugins);
    }

    public boolean supportsRemoval()
    {
        return false;
    }

    public boolean supportsAddition()
    {
        return false;
    }

    @Override
    public boolean isDynamicPluginLoader()
    {
        return false;
    }

    public Iterable<Plugin> loadFoundPlugins(final ModuleDescriptorFactory moduleDescriptorFactory)
    {
        throw new UnsupportedOperationException("This PluginLoader does not support addition.");
    }

    public void removePlugin(final Plugin plugin) throws PluginException
    {
        throw new PluginException("This PluginLoader does not support removal.");
    }
}
