package com.atlassian.plugin.factories;

import com.atlassian.plugin.Application;
import com.atlassian.plugin.ModuleDescriptorFactory;
import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginArtifact;
import com.atlassian.plugin.PluginParseException;
import com.atlassian.plugin.impl.XmlDynamicPlugin;
import com.atlassian.plugin.parsers.DescriptorParser;
import com.atlassian.plugin.parsers.XmlDescriptorParserFactory;
import com.google.common.base.Predicate;
import com.google.common.base.Predicates;
import com.google.common.collect.Sets;
import org.apache.commons.io.IOUtils;
import org.dom4j.DocumentException;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Set;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Deploys plugins that consist of an XML descriptor file.
 *
 * @since 2.1.0
 */
public final class XmlDynamicPluginFactory extends AbstractPluginFactory
{
    /**
     * @param application The application key to use to choose modules
     * @since 3.0
     */
    public XmlDynamicPluginFactory(final Application application)
    {
        this(Sets.newHashSet(application));
    }

    /**
     * @param applications The application key to use to choose modules
     * @since 3.0
     */
    public XmlDynamicPluginFactory(final Set<Application> applications)
    {
        super(new XmlDescriptorParserFactory(), applications);
    }

    @Override
    protected InputStream getDescriptorInputStream(PluginArtifact pluginArtifact)
    {
        return pluginArtifact.getInputStream();
    }

    @Override
    protected Predicate<Integer> isValidPluginsVersion()
    {
        return Predicates.alwaysTrue();
    }

    @Override
    public String canCreate(PluginArtifact pluginArtifact) throws PluginParseException
    {
        try
        {
            return super.canCreate(pluginArtifact);
        }
        catch (PluginParseException e)
        {
            if (e.getCause() instanceof DocumentException)
            {
                log.debug("There was an error parsing the plugin descriptor for '{}'", pluginArtifact);
                log.debug("This is most probably because we parsed a jar, and the plugin is not an XML dynamic plugin. See the exception below for confirmation:", e);
                return null;
            }
            throw e;
        }
    }

    /**
     * Deploys the plugin artifact
     *
     * @param pluginArtifact the plugin artifact to deploy
     * @param moduleDescriptorFactory The factory for plugin modules
     * @return The instantiated and populated plugin
     * @throws PluginParseException If the descriptor cannot be parsed
     * @since 2.2.0
     */
    public Plugin create(final PluginArtifact pluginArtifact, final ModuleDescriptorFactory moduleDescriptorFactory) throws PluginParseException
    {
        checkNotNull(pluginArtifact, "The plugin artifact must not be null");
        checkNotNull(moduleDescriptorFactory, "The module descriptor factory must not be null");

        InputStream pluginDescriptor = null;
        try
        {
            pluginDescriptor = new FileInputStream(pluginArtifact.toFile());
            // The plugin we get back may not be the same (in the case of an UnloadablePlugin), so add what gets returned, rather than the original
            final DescriptorParser parser = descriptorParserFactory.getInstance(pluginDescriptor, applications);
            return parser.configurePlugin(moduleDescriptorFactory, new XmlDynamicPlugin(pluginArtifact));
        }
        catch (final RuntimeException e)
        {
            throw new PluginParseException(e);
        }
        catch (final IOException e)
        {
            throw new PluginParseException(e);
        }
        finally
        {
            IOUtils.closeQuietly(pluginDescriptor);
        }
    }
}