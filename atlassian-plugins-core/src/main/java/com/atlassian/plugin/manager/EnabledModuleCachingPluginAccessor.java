package com.atlassian.plugin.manager;

import com.atlassian.plugin.ModuleDescriptor;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.event.PluginEventListener;
import com.atlassian.plugin.event.PluginEventManager;
import com.atlassian.plugin.event.events.PluginDisabledEvent;
import com.atlassian.plugin.tracker.PluginModuleTracker;
import com.atlassian.plugin.cache.ConcurrentCacheFactory;
import com.google.common.base.Function;
import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;

import java.util.List;

import static com.atlassian.plugin.tracker.DefaultPluginModuleTracker.create;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.collect.ImmutableList.copyOf;

/**
 * A caching decorator which caches {@link #getEnabledModuleDescriptorsByClass(Class)} on {@link com.atlassian.plugin.PluginAccessor} interface.
 *
 * @since 2.7.0
 */
public final class EnabledModuleCachingPluginAccessor extends ForwardingPluginAccessor implements PluginAccessor
{
    private final PluginEventManager pluginEventManager;
    private final Cache<Class<ModuleDescriptor<Object>>, PluginModuleTracker<Object, ModuleDescriptor<Object>>> cache;

    public EnabledModuleCachingPluginAccessor(final PluginAccessor delegate, final PluginEventManager pluginEventManager)
    {
        super(delegate);
        this.pluginEventManager = checkNotNull(pluginEventManager);
        this.pluginEventManager.register(this);
        this.cache = new DefaultPluginModuleTrackerCacheFactory().createCache();
    }

    public EnabledModuleCachingPluginAccessor(final PluginAccessor delegate, final PluginEventManager pluginEventManager, final ConcurrentCacheFactory<Class<ModuleDescriptor<Object>>, PluginModuleTracker<Object, ModuleDescriptor<Object>>> pluginModuleTrackerCacheFactory)
    {
        super(delegate);
        this.pluginEventManager = checkNotNull(pluginEventManager);
        this.pluginEventManager.register(this);
        this.cache = pluginModuleTrackerCacheFactory.createCache();
    }


    @Override
    public <D extends ModuleDescriptor<?>> List<D> getEnabledModuleDescriptorsByClass(final Class<D> descriptorClazz)
    {
        return copyOf(descriptors(descriptorClazz));
    }

    /**
     * Clears the enabled module cache when any plugin is disabled.  The cache already has weak keys and values, but this
     * ensures old modules are never returned from disabled plugins.
     *
     * @param event The plugin disabled event
     */
    @PluginEventListener
    public void onPluginDisable(PluginDisabledEvent event)
    {
        cache.invalidateAll();
    }

    /**
     * Cache implementation.
     */
    @SuppressWarnings("unchecked")
    <D> Iterable<D> descriptors(final Class<D> moduleDescriptorClass)
    {
        PluginModuleTracker<Object, ModuleDescriptor<Object>> tracker = cache.getUnchecked((Class<ModuleDescriptor<Object>>) moduleDescriptorClass);
        return (Iterable<D>) tracker.getModuleDescriptors();
    }

    private class DefaultPluginModuleTrackerCacheFactory implements ConcurrentCacheFactory<Class<ModuleDescriptor<Object>>, PluginModuleTracker<Object, ModuleDescriptor<Object>>>
    {
        @Override
        public Cache<Class<ModuleDescriptor<Object>>, PluginModuleTracker<Object, ModuleDescriptor<Object>>> createCache()
        {
            return CacheBuilder.newBuilder().weakKeys().weakValues().build(CacheLoader.from(new PluginModuleTrackerFactory()));
        }
    }

    private class PluginModuleTrackerFactory implements Function<Class<ModuleDescriptor<Object>>, PluginModuleTracker<Object, ModuleDescriptor<Object>>>
    {
        public PluginModuleTracker<Object, ModuleDescriptor<Object>> apply(final Class<ModuleDescriptor<Object>> moduleDescriptorClass)
        {
            return create(delegate, pluginEventManager, moduleDescriptorClass);
        }
    }
}
