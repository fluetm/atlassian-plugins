package com.atlassian.plugin.util.collect;

import com.atlassian.plugin.Plugin;
import com.google.common.collect.Iterables;

public final class Transforms
{
    private Transforms()
    {
    }

    public static Iterable<String> toPluginKeys(Iterable<Plugin> plugins)
    {
        return Iterables.transform(plugins, new ToPluginKeysFunction());
    }

    private static final class ToPluginKeysFunction implements com.google.common.base.Function<Plugin, String>
    {
        @Override
        public String apply(Plugin p)
        {
            return p.getKey();
        }
    }
}
