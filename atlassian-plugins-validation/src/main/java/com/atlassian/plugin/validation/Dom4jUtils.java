package com.atlassian.plugin.validation;

import com.google.common.io.Closeables;
import com.google.common.io.InputSupplier;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.io.SAXReader;

import java.io.IOException;
import java.io.Reader;


/**
 * @since 3.0.0
 */
abstract class Dom4jUtils
{
    private Dom4jUtils()
    {
    }

    public static Document readDocument(InputSupplier<? extends Reader> input)
    {
        Reader reader = null;
        try
        {
            reader = input.getInput();
            return readDocument(reader);
        }
        catch (DocumentException e)
        {
            throw new RuntimeException(e);
        }
        catch (IOException e)
        {
            throw new RuntimeException(e);
        }
        finally
        {
            Closeables.closeQuietly(reader);
        }
    }

    private static Document readDocument(Reader reader) throws DocumentException
    {
        return getSaxReader().read(reader);
    }

    private static SAXReader getSaxReader()
    {
        final SAXReader reader = new SAXReader();
        reader.setMergeAdjacentText(true);
        return reader;
    }
}
