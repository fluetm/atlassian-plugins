package com.atlassian.plugin.validation;

import com.google.common.io.CharStreams;
import com.google.common.io.InputSupplier;
import org.dom4j.Document;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.Charset;

final class ResourcesLoader
{
    static Document getTestDocument(String name)
    {
        return Dom4jUtils.readDocument(getInput(name));
    }

    static InputSupplier<InputStreamReader> getInput(final String name)
    {
        return CharStreams.newReaderSupplier(new InputSupplier<InputStream>()
        {
            @Override
            public InputStream getInput() throws IOException
            {
                return ResourcesLoader.class.getResourceAsStream(name);
            }
        }, Charset.forName("UTF-8"));
    }
}
