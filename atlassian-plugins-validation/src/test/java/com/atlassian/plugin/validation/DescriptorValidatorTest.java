package com.atlassian.plugin.validation;

import com.atlassian.fugue.Either;
import com.atlassian.plugin.Application;
import com.atlassian.plugin.InstallationMode;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Iterables;
import org.junit.Test;

import java.util.Set;

import static com.atlassian.plugin.validation.DescriptorValidator.ValidationError;
import static com.atlassian.plugin.validation.DescriptorValidator.ValidationSuccess;
import static com.atlassian.plugin.validation.ResourcesLoader.getInput;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public final class DescriptorValidatorTest
{
    @Test
    public void testGetRequiredPermissionsIsEmptyForEmptyDescriptor()
    {
        final DescriptorValidator descriptorValidator = new DescriptorValidator(getInput("/empty-descriptor.xml"), getInput("/schema.xsd"), getApplications());

        final Set<String> requiredPermissions = descriptorValidator.getRequiredPermissions(InstallationMode.LOCAL);
        assertTrue(requiredPermissions.isEmpty());
    }

    @Test
    public void testGetRequiredPermissionsIsEmptyForDescriptorWithModuleRequiringPermission()
    {
        final DescriptorValidator descriptorValidator = new DescriptorValidator(getInput("/descriptor-with-module-requiring-exec-java.xml"), getInput("/schema.xsd"), getApplications());

        final Set<String> requiredPermissions = descriptorValidator.getRequiredPermissions(InstallationMode.LOCAL);
        assertEquals(1, requiredPermissions.size());
        assertEquals("execute_java", Iterables.getFirst(requiredPermissions, null));
    }

    @Test
    public void testHasNecessaryPermissionsForEmptyDescriptor()
    {
        final DescriptorValidator descriptorValidator = new DescriptorValidator(getInput("/empty-descriptor.xml"), getInput("/schema.xsd"), getApplications());

        final Either<ValidationError, ValidationSuccess> validation = descriptorValidator.validate(InstallationMode.LOCAL);
        assertTrue(validation.isRight());
        assertFalse(validation.right().get().isRemotable());
    }

    @Test
    public void testHasNecessaryPermissionsForRemotableDescriptor()
    {
        final DescriptorValidator descriptorValidator = new DescriptorValidator(getInput("/remotable-descriptor.xml"), getInput("/schema.xsd"), getApplications());

        final Either<ValidationError, ValidationSuccess> validation = descriptorValidator.validate(InstallationMode.LOCAL);
        assertTrue(validation.isRight());
        assertTrue(validation.right().get().isRemotable());
    }

    @Test
    public void testHasNecessaryPermissionsForDescriptorWithModuleRequiringPermission()
    {
        final DescriptorValidator descriptorValidator = new DescriptorValidator(getInput("/descriptor-with-module-requiring-exec-java.xml"), getInput("/schema.xsd"), getApplications());

        final Either<ValidationError, ValidationSuccess> validation = descriptorValidator.validate(InstallationMode.LOCAL);
        assertFalse(validation.isRight());
        final ValidationError validationError = validation.left().get();
        assertEquals(1, validationError.getNotAskedPermissions().size());
        assertEquals("execute_java", Iterables.getFirst(validationError.getNotAskedPermissions(), null));
    }

    @Test
    public void testHasNecessaryPermissionsForDescriptorWithModuleRequiringPermissionButAtlassianPlugins2()
    {
        final DescriptorValidator descriptorValidator = new DescriptorValidator(getInput("/descriptor-with-module-requiring-exec-java-but-version-2.xml"), getInput("/schema.xsd"), getApplications());

        final Either<ValidationError, ValidationSuccess> validation = descriptorValidator.validate(InstallationMode.LOCAL);
        assertTrue(validation.isRight());
    }

    @Test
    public void testHasNecessaryPermissionsForDescriptorWithModuleRequiringPermissionAndDescriptorAskingForIt()
    {
        final DescriptorValidator descriptorValidator = new DescriptorValidator(getInput("/descriptor-asking-exec-java.xml"), getInput("/schema.xsd"), getApplications());

        final Either<ValidationError, ValidationSuccess> validation = descriptorValidator.validate(InstallationMode.LOCAL);
        assertTrue(validation.isRight());
    }

    @Test
    public void testHasNecessaryPermissionsForDescriptorWithModuleRequiringPermissionAndDescriptorAskingForItInLocalMode()
    {
        final DescriptorValidator descriptorValidator = new DescriptorValidator(getInput("/descriptor-asking-exec-java-when-installed-locally.xml"), getInput("/schema.xsd"), getApplications());

        final Either<ValidationError, ValidationSuccess> validation = descriptorValidator.validate(InstallationMode.LOCAL);
        assertTrue(validation.isRight());
    }

    @Test
    public void testHasNecessaryPermissionsForDescriptorWithModuleRequiringPermissionAndDescriptorAskingForItInRemoteMode()
    {
        final DescriptorValidator descriptorValidator = new DescriptorValidator(getInput("/descriptor-asking-exec-java-when-installed-locally.xml"), getInput("/schema.xsd"), getApplications());

        final Either<ValidationError, ValidationSuccess> validation = descriptorValidator.validate(InstallationMode.REMOTE);
        assertTrue(validation.isRight());
    }

    @Test
    public void testHasNecessaryPermissionsForDescriptorWithModuleRequiringPermissionAndDescriptorAskingForItInLocalModeButRequireInRemoteTestingLocal()
    {
        final DescriptorValidator descriptorValidator = new DescriptorValidator(getInput("/descriptor-asking-exec-java-locally-but-require-remotely.xml"), getInput("/schema.xsd"), getApplications());

        final Either<ValidationError, ValidationSuccess> validation = descriptorValidator.validate(InstallationMode.LOCAL);
        assertTrue(validation.isRight());
    }

    @Test
    public void testHasNecessaryPermissionsForDescriptorWithModuleRequiringPermissionAndDescriptorAskingForItInLocalModeButRequireInRemoteTestingRemote()
    {
        final DescriptorValidator descriptorValidator = new DescriptorValidator(getInput("/descriptor-asking-exec-java-locally-but-require-remotely.xml"), getInput("/schema.xsd"), getApplications());

        final Either<ValidationError, ValidationSuccess> validation = descriptorValidator.validate(InstallationMode.REMOTE);
        assertTrue(validation.isLeft());
        final ValidationError validationError = validation.left().get();
        assertEquals(1, validationError.getNotAskedPermissions().size());
        assertEquals("execute_java", Iterables.getFirst(validationError.getNotAskedPermissions(), null));
    }

    @Test
    public void testHasNecessaryPermissionsForDescriptorRequiringNonValidPermission()
    {
        final DescriptorValidator descriptorValidator = new DescriptorValidator(getInput("/descriptor-requiring-non-valid-permission.xml"), getInput("/schema.xsd"), getApplications());

        final Either<ValidationError, ValidationSuccess> validation = descriptorValidator.validate(InstallationMode.LOCAL);
        assertFalse(validation.isRight());
        final ValidationError validationError = validation.left().get();
        assertEquals(1, validationError.getNonValidPermissions().size());
        assertEquals("funky_permission", Iterables.getFirst(validationError.getNonValidPermissions(), null));
    }

    @Test
    public void testHasNecessaryPermissionsForDescriptorRequiringNonValidPermissionForOtherInstallationMode()
    {
        final DescriptorValidator descriptorValidator = new DescriptorValidator(getInput("/descriptor-requiring-non-valid-permission-local.xml"), getInput("/schema.xsd"), getApplications());

        final Either<ValidationError, ValidationSuccess> validation = descriptorValidator.validate(InstallationMode.REMOTE);
        assertTrue(validation.isRight());
    }

    @Test
    public void testHasNecessaryPermissionsForDescriptorRequiringNonValidPermissionForSameInstallationMode()
    {
        final DescriptorValidator descriptorValidator = new DescriptorValidator(getInput("/descriptor-requiring-non-valid-permission-local.xml"), getInput("/schema.xsd"), getApplications());

        final Either<ValidationError, ValidationSuccess> validation = descriptorValidator.validate(InstallationMode.LOCAL);
        assertFalse(validation.isRight());
        final ValidationError validationError = validation.left().get();
        assertEquals(1, validationError.getNonValidPermissions().size());
        assertEquals("funky_permission", Iterables.getFirst(validationError.getNonValidPermissions(), null));
    }

    private ImmutableSet<Application> getApplications()
    {
        return ImmutableSet.<Application>of(new Application()
        {
            @Override
            public String getKey()
            {
                return "test-app";
            }

            @Override
            public String getVersion()
            {
                return "1.0";
            }

            @Override
            public String getBuildNumber()
            {
                return "1";
            }
        });
    }
}
